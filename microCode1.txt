!Provera starta!
madr00 br (if !START then madr00);
madr01 PCout1, ldMAR, MOST1_3, incPC;
madr02 RD;
madr03 br (if OBI then madr03);
madr04 MDRout1, ldIR;
madr05 br (if !gropr then madr08);
madr06 stPRCOD;
madr07 br madr5B;
madr08 br (if !mem_transfer then madr14);
!Adresiranja!
madr09 bradr;
!LD!
madr0A GPRSRCout2, IRIMM18out1, add, ALUout3, ldMAR;
madr0B br madr12;
!IN!
madr0C GPRSRCout2,  MOST2_1, MOST1_3, ldMAR, stIO; 
madr0D br madr12;
!ST!
madr0E GPRDSTout1, IRIMM18out2, add, ALUout3, ldMAR;
madr0F br madr14;
!OUT!
madr10 GPRDSTout1, MOST1_3, ldMAR, stIO;
madr11 br madr14;
!Citanje operanda!
madr12 RD;
madr13 br (if OBI then madr13); 
!Izvrsavanje instrukcije!
madr14 bropr;
!LD!!IN!
madr15 MDRout1, MOST1_3, wrGPR;
madr16 br madr5B;
!ST!!OUT!
madr17 GPRSRCout2, MOST2_1, ldMDR;
madr18 WR;
madr19 br (if OBI then madr19);
madr1A br madr5B;
!PUSH-PSW!
madr1B decSP;
madr1C PSWout1, ldMDR, SPout3, ldMAR;
madr1D br madr20;
!PUSH!
madr1E decSP;
madr1F GPRDSTout1, ldMDR, SPout3, ldMAR;
madr20 WR;
madr21 br (if OBI then madr21);
madr22 br madr5B;
!POP-PSW!!POP!
madr23 SPout3, ldMAR, incSP;
madr24 RD;
madr25 br (if OBI then madr25);
madr26 br (if !POP_PSW then madr29);
madr27 MDRout1, ldPSW;
madr28 br madr5B;
madr29 MDRout1, MOST1_3, wrGPR, ldN, ldZ;
madr2A br madr5B;
!ADD!
madr2B GPRDSTout1, GPRSRCout2, add, ALUout3, ldAW;
madr2C AWout1, IRIMM18out2, add, ALUout3, wrGPR, ldC, ldV, ldN, ldZ;
madr2D br madr5B;
!SUB!
madr2E GPRDSTout1, GPRSRCout2, sub, ALUout3, ldAW;
madr2F AWout1, IRIMM18out2, sub, ALUout3, wrGPR, ldC, ldV, ldN, ldZ;
madr30 br madr5B;
!CMP!
madr31 GPRDSTout1, GPRSRCout2, sub, ALUout3, ldAW;
madr32 AWout1, IRIMM18out2, sub, ALUout3, ldC, ldV, ldN, ldZ;
madr33 br madr5B;
!AND!
madr34 GPRDSTout1, GPRSRCout2, and, ALUout3, wrGPR, ldN, ldZ;
madr35 br madr5B;
!OR!
madr36 GPRDSTout1, GPRSRCout2, or, ALUout3, wrGPR, ldN, ldZ;
madr37 br madr5B;
!NOT!
madr38 GPRSRCout2, MOST2_1, not, ALUout3, wrGPR, ldN, ldZ;
madr39 br madr5B;
!TST!
madr3A GPRDSTout1, GPRSRCout2, and, ALUout3, ldN, ldZ;
madr3B br madr5B;
!LSR!
madr3C GPRSRCout2, IRSHout1, shr, BSout3, wrGPR, ldN, ldZ, ldC; 
madr3D br madr5B;
!LSL!
madr3E GPRSRCout2, IRSHout1, shl, BSout3, wrGPR, ldN, ldZ, ldC;
madr3F br madr5B;
!JE, JNE, JGE, JG, JLE, JL, JP, JNP, JO, JNO!
madr40 br (if !brpom then madr5B);
madr41 br (if !brnch_regindpom then madr4A);
madr42 br madr48;
!CALL push pc on stack, jump on address!
madr43 decSP;
madr44 PCout1, SPout3, ldMAR, ldMDR;	
madr45 WR;
madr46 br (if OBI then madr46);
madr47 br (if !call_regindpom then madr4A);
madr48 GPRDSTout1, IRBRNCHout2, add, ALUout3, ldPC;
madr49 br madr5B;
madr4A PCout1, IRBRNCHout2, add, ALUout3, ldMAR;
madr4B RD;
madr4C br (if OBI then madr4C);
madr4D MDRout1, MOST1_3, ldPC;
madr4E br madr5B;
!INT!
madr4F stPRINS;
madr50 br madr5B;
!JMP!
madr51 PCout1, IRJMPout2, add, ALUout3, ldPC;
madr52 br madr5B;
!IRET!//psw from stack
madr53 SPout3, ldMAR, incSP;
madr54 RD;
madr55 br (if OBI then madr55);
madr56 MDRout1, ldPSW;
!RET!
madr57 SPout3, ldMAR, incSP;
madr58 RD;
madr59 br (if OBI then madr59);
madr5A MDRout1, MOST1_3, ldPC;
!Obrada prekida!
madr5B br (if !prekid then madr00);
!Cuvanje konteksta procesora!
madr5C decSP;
madr5D PCout1, SPout3, ldMAR, ldMDR;
madr5E WR;
madr5F br (if OBI then madr5F);
madr60 decSP;
madr61 PSWout1, ldMDR, SPout3, ldMAR;
madr62 WR;
madr63 br (if OBI then madr63);
!Utvrdjivanje broja ulaza i adrese prekidne rutine!
madr64 brintr;
!PRINS!
madr65 IRINTout3, ldMAR, clPRINS;
madr66 br madr70;
!PRCOD!
madr67 UINTout3, ldMAR, clPRCOD;
madr68 br madr70;
!PRADR!
madr69 UINTout3, ldMAR, clPRADR;
madr6A br madr70;
!PRINM!
madr6B UINTout3, ldMAR, clPRINM;
madr6C br madr70;
!PRINTR!
madr6D UEXTout3, ldMAR, clINTR, ldL;
madr6E br madr70;
!PSWT!
madr6F UINTout3, ldMAR;
!Dohvatanje adrese prekidne rutine!
madr70 RD;
madr71 br (if OBI then madr71);
madr72 MDRout1, MOST1_3, ldPC, clPSWI, clPSWT;
madr73 br madr00;


!HALT!
madr74 clSTART;
madr75 br madr64;