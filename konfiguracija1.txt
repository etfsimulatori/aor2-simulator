// Sirina podatka. Dozvoljene vrednost 8 i 16
"dataSize", "8"

// Broj registara opste namene u procesoru. Dozvoljene vrednost 2, 4, 8, 16, 32 i 64
"numberOfRegisters", "32"

// Pozicija unutar bajta koja odredjuje pocetni biti unutar reci na magistrali koji specificira kom registru opste namene se pristupa
"GPRARStartPosition", "0"

// Broj bita potrebnih za adresiranje unutar registarskog fajla od numberOfRegisters registara
"GPRNumberOfBits", "5"

// Kasnjenje memorije. Dozvoljena vrednost > 0 i < 256
"memdelay", "5"

// Sirina adresibilne jedinice. Iste je sirine DBUS, MDR i IR0-IR3. Dozvoljena vrednost 8 i 16
"addressableUnit", "8"

// konfiguracija za fetch1 jedinicu se sastoji od veceg broja redova u kojima su opisani trostaticki baferi koji izbacuju sadrzaj na interne magistrale.
// U jednom redu se nalazi konfiguracija za jedno trostaticko kolo.
// Konfiguracija za trostaticko kolo se zadaje po formatu: Naziv jedinice ("FETCH1"), Tip ("TRI"), Naziv logickog kola ("IRAD", dozvoljene su samo vrednosti: IRAD, IRJA, IRPOM, IRBR ), Broj ulaznih pinova signala ("16"), Naziv signala povezanog na ulaz E trostatickog kola ("Oper1.IRDAout3"), Naziv signala povezanog na ulaze 0..n-1 trostatickog bafera ("IR23", dozvoljene vrednosti su IRX i IRXX), Naziv izlaza trostatickog kola ("IRAD")
// primer konfiguracija fetch1 jedinice
"FETCH1", "TRI", "IRAD", "16", "Oper1.IRDAout3", "IR0", "IR1", "IR2", "IR3", "IR4", "IR5", "IR6", "IR7", "IR8", "IR9", "IR10", "IR11", "IR12", "IR13", "IR14", "IR15", "IRAD"
"FETCH1", "TRI", "IRJA", "16", "Oper1.IRJAout2", "IR8", "IR9", "IR10", "IR11", "IR12", "IR13", "IR14", "IR15", "IR16", "IR17", "IR18", "IR19", "IR20", "IR21", "IR22", "IR23", "IRJA"
"FETCH1", "TRI", "IRPOM", "16", "Oper1.IRPOMout3", "IR16", "IR17", "IR18", "IR19", "IR20", "IR21", "IR22", "IR23","IR23", "IR23", "IR23", "IR23", "IR23", "IR23", "IR23", "IR23", "IRPOM"
"FETCH1", "TRI", "IRBR", "8", "Oper1.IRBRout3", "IR24", "IR25", "IR26", "IR27", "IR28", "IR29", "IR30", "IR31", "IRBR"

// konfiguracija za fetch2 jedinicu se sastoji od veceg broja redova u kojima su opisani dekoderi koji se koriste u datoj jedinici.
// U jednom redu se nalazi konfiguracija za jedan dekoder.
// konfiguracija za dekoder se zadaje po formatu: Naziv jedinice ("FETCH2"), Tip ("DC"), Naziv dekodera ("DC1"), Broj kontrolnih signala ("2"), Naziv signala povezanog na ulaz E ("START"), Naziv signala povezanog na ulaz 0 dekodeta ("IR30"), Naziv signala povezanog na ulaz 1 dekodeta ("IR31"), Naziv izlaza 0 dekodeta ("Go"), ...
// Broj kontrolnih signala moze da ima vrednost 1, 2, 3 ili 4. Nazivi izlaznih pinova dekodera moraju da imaju jedinstveno ime. 
// Ime nekog pina moze da se sastoji od je jednog ili vise slova, znakova, ili znaka _
// voditi racuna da ostale jedinice procesora koriste ove pinove i da im pristupaju preko imena.
// primer konfiguracija fetch2 jedinice
"FETCH2", "DC", "DC1", "1", "START", "IR31", "G0", "G1"
"FETCH2", "DC", "DC2", "2", "G0", "IR29", "IR30", "G0_PG0", "G0_PG1", "G0_PG2", "G0_PG3"
"FETCH2", "DC", "DC3", "3", "G0_PG0", "IR26", "IR27", "IR28", "ADD", "SUB", "G0_PG0_2", "G0_PG0_3", "CMP", "AND", "OR", "NOT"
"FETCH2", "DC", "DC4", "3", "G0_PG1", "IR26", "IR27", "IR28", "TST", "LD", "ST", "JE", "JNE", "JGE", "JG", "JLE"
"FETCH2", "DC", "DC5", "3", "G0_PG2", "IR26", "IR27", "IR28", "JL", "JP", "JNP", "JO", "JNO", "CALL", "G0_PG2_6", "PUSH"
"FETCH2", "DC", "DC6", "3", "G0_PG3", "IR26", "IR27", "IR28", "POP", "HALT", "IN", "OUT", "LSR", "LSL", "INT", "G0_PG3_7"
"FETCH2", "DC", "DC7", "2", "G0_PG2_6", "IR24", "IR25", "RET", "IRET", "JMP", "G0_PG2_6_3"


// U jednom redu se nalazi konfiguracija za jedno logicko kolo.
// Konfiguracija za logicko kolo se zadaje po formatu: Naziv jedinice ("FETCH3"), Tip ("OR"), Naziv logickog kola ("ORgradr"), Broj ulaznih pinova signala ("2"), Naziv signala povezanog na ulaz 0 logickog kola ("Fetch2.STB"), Naziv signala povezanog na ulaz 1 logickog kola ("Fetch2.STW"), Naziv izlaza logickog kola ("ORgradr_0")
// Broj kontrolnih signala je veci od 0. 
// Ime nekog pina moze da se sastoji od je jednog ili vise slova, znakova, ili znaka _
// voditi racuna da ostale jedinice procesora koriste ove pinove i da im pristupaju preko imena.
// Dozvoljeno je postaviti parametar sa brojem ulaznih pinova za svako OR logicko kolo (AND nije dozvoljeno menjati), kao i imenovati ulazne signale povezane na svaki pin
// primer konfiguracija fetch3 jedinice
"FETCH3", "OR", "ORregdir", "9", "Fetch2.ADD", "Fetch2.SUB", "Fetch2.CMP", "Fetch2.AND", "Fetch2.OR", "Fetch2.NOT", "Fetch2.TST", "Fetch2.LSL", "Fetch2.LSR", "regdir"
"FETCH3", "OR", "ORmemTransfer", "4", "Fetch2.LD", "Fetch2.IN", "Fetch2.ST", "Fetch2.OUT", "mem_transfer"
"FETCH3", "AND", "ANDpopPSW", "2", "Fetch2.POP", "Fetch1.IR25", "pop_psw"
"FETCH3", "OR", "ORbranch", "10", "Fetch2.JE", "Fetch2.JNE", "Fetch2.JGE", "Fetch2.JG", "Fetch2.JLE", "Fetch2.JL", "Fetch2.JP", "Fetch2.JNP", "Fetch2.JO", "Fetch2.JNO", "branch"
"FETCH3", "OR", "ORgropr", "5", "Fetch2.G1", "Fetch2.G0_PG0_2", "Fetch2.G0_PG0_3", "Fetch2.G0_PG3_7", "Fetch2.G0_PG2_6_3", "gropr"
"FETCH3", "OR", "ORsdst", "4", "Fetch3.branch", "Fetch2.CALL", "Fetch2.POP", "Fetch2.PUSH", "sdst"
//"FETCH3", "AND", "ANDsdst", "2", "Fetch3.or_sdst", "Fetch1.IR25", "sdst"
"FETCH3", "AND", "ANDbrnchPC", "2", "Fetch3.branch", "Fetch1.IR25", "brnch_regindpom"
"FETCH3", "AND", "ANDcallPC", "2", "Fetch2.CALL", "Fetch1.IR25", "call_regindpom"
"FETCH3", "AND", "ANDpush_psw", "2", "Fetch2.PUSH", "Fetch1.IR25", "PUSH_PSW"

// U jednom redu se nalazi konfiguracija za jedno logicko kolo. Konfiguracija je data po istom foramtu kao i za prethodne stepene.
// Dozvoljeno je postaviti parametar sa brojem ulaznih pinova za jedno OR logicko kolo, kao i imenovati ulazne signale povezane na svaki pin tok logickog OR kola
// Nije dozvoljeno dodavati nove komponente, preimenovati postojece komponente, niti menjati naziv izlaznim pinovaima.
// primer konfiguracija exec3 jedinice
"EXEC3", "OR", "NZOR", "11", "Fetch2.ADD", "Fetch2.SUB", "Fetch2.CMP", "Fetch2.AND", "Fetch2.OR", "Fetch2.NOT", "Fetch2.TST", "Fetch2.LSR", "Fetch2.LSL", "Fetch2.LD", "Fetch2.POP", "NZOR"

// U jednom redu se nalazi konfiguracija za jedan uslov visestrukog uslovnog skoka kod koda operacije 
// Konfiguracija za logicko kolo se zadaje po formatu: Naziv jedinice ("KMOPR1"), Naziv logickog uslova za visestruki uslovni skok ("Fetch2.NOP"), Adresa u mikroprogramskoj memoriji ("62")
// Broj ulaza ije ogranicen
// primer konfiguracija kmopr1 jedinice
"KMOPR1", "Fetch2.LD", "21"
"KMOPR1", "Fetch2.IN", "21"
"KMOPR1", "Fetch2.ST", "23"
"KMOPR1", "Fetch2.OUT", "23"
"KMOPR1", "Fetch3.PUSH_PSW", "27"
"KMOPR1", "Fetch2.PUSH", "30"
"KMOPR1", "Fetch2.POP", "35"
"KMOPR1", "Fetch2.ADD", "43"
"KMOPR1", "Fetch2.SUB", "46"
"KMOPR1", "Fetch2.CMP", "49"
"KMOPR1", "Fetch2.AND", "52"
"KMOPR1", "Fetch2.OR", "54"
"KMOPR1", "Fetch2.NOT", "56"
"KMOPR1", "Fetch2.TST", "58"
"KMOPR1", "Fetch2.LSR", "60"
"KMOPR1", "Fetch2.LSL", "62"
"KMOPR1", "Fetch2.JE", "64"
"KMOPR1", "Fetch2.JNE", "64"
"KMOPR1", "Fetch2.JGE", "64"
"KMOPR1", "Fetch2.JG", "64"
"KMOPR1", "Fetch2.JLE", "64"
"KMOPR1", "Fetch2.JL", "64"
"KMOPR1", "Fetch2.JP", "64"
"KMOPR1", "Fetch2.JNP", "64"
"KMOPR1", "Fetch2.JO", "64"
"KMOPR1", "Fetch2.JNO", "64"
"KMOPR1", "Fetch2.CALL", "67"
"KMOPR1", "Fetch2.INT", "79"
"KMOPR1", "Fetch2.JMP", "81"
"KMOPR1", "Fetch2.IRET", "83"
"KMOPR1", "Fetch2.RET", "87"
"KMOPR1", "Fetch2.HALT", "116"

// U jednom redu se nalazi konfiguracija za jedan uslov visestrukog uslovnog skoka kod nacina adresiranja
// Konfiguracija za logicko kolo se zadaje po formatu: Naziv jedinice ("KMADR1"), Naziv logickog uslova za visestruki uslovni skok ("Fetch2.regdir"), Adresa u mikroprogramskoj memoriji ("26")
// Broj ulaza ije ogranicen
// primer konfiguracija kmadr1 jedinice
"KMADR1", "Fetch2.LD", "10"
"KMADR1", "Fetch2.IN", "12"
"KMADR1", "Fetch2.ST", "14"
"KMADR1", "Fetch2.OUT", "16"

//KMINTR1
"KMINTR1", "Intr1.PRINS", "101"
"KMINTR1", "Intr1.PRCOD", "103"
"KMINTR1", "Intr1.PRADR", "105"
"KMINTR1", "Intr1.PRINM", "107"
"KMINTR1", "Intr2.printr", "109"
"KMINTR1", "Exec2.PSWT", "111"


// U jednom redu se nalazi konfiguracija za jedan izlaz dekodera u jedinici "Signali upravljacke jedinice"
// Konfiguracija jednog izlaza se zadaje po formatu: Naziv jedinice ("CONTRODC"), Izlaz dekodera koji se pozmatra ("2"), Uslov koji se posmatra ("Exec2.START"), Koplementarna vrednost ("#")
// Dozvoljene vrednost za: izlaz dekodera koji se posmatra [0-31]
// neki izlaza treba da imaju sledece nazive: next (preporuka da bude 0), br, bradr, bropr
//	uslov koji se posmatra - pinovi definisani u ostalim jedinicama
//	komplementarna vrednost - "" ako se ne komplementira i "#" ako se komplementira
// ukoliko su polje za uslov koji se posmatra i komplementarna vrednost onda trece polje predstavlja naziv signala
// primer konfiguracija CONTRODC jedinice

"CONTRODC", "0", "", "", "next"
"CONTRODC", "1", "", "", "br"
"CONTRODC", "2", "", "", "bradr"
"CONTRODC", "3", "", "", "bropr"
"CONTRODC", "4", "", "", "brintr"
"CONTRODC", "5", "Exec2.START", "#"
"CONTRODC", "6", "Bus1.OBI", ""
"CONTRODC", "7", "Fetch3.gropr", "#"
"CONTRODC", "8", "Fetch3.mem_transfer", "#"
"CONTRODC", "9", "Fetch3.pop_psw", "#"
"CONTRODC", "10", "Exec4.brpom", "#"
"CONTRODC", "11", "Fetch3.brnch_regindpom", "#"
"CONTRODC", "12", "Fetch3.call_regindpom", "#"
"CONTRODC", "13", "Intr1.prekid", "#"