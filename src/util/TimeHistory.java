package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import logic.Pin;

public class TimeHistory {
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TRI = 2;
	public static final String TRISTATE = "Z";

	// public static final int[] MASKS = { 8, 4, 2, 1 };
	// public static final int[] UNMASKS = { 7, 11, 13, 14 };

	private List<String> history;
	private List<Map<String, String>> multiHistrory;

	public Map<String, Integer> signals;
	private List<Pin> pins;

	private List<TimeHistoryObserver> observers;

	public TimeHistory(List<String> names, TimeHistoryObserver observer) {
		history = new ArrayList<String>();
		multiHistrory = new ArrayList<Map<String, String>>();

		this.signals = new HashMap<String, Integer>();
		this.pins = new ArrayList<Pin>();
		int i = 0;
		for (String name : names) {
			this.signals.put(name, i++);
			Pin pin = NameConnector.getPin(name);
			pin.setName(name);
			this.pins.add(pin);
		}
		observers = new LinkedList<TimeHistoryObserver>();
		this.addObserver(observer);
	}

	public void addClkState() {
		String newState = String.format("%0" + (this.pins.size() + 1) + "X",
				new Object[] { Integer.valueOf(0) });
		Map<String, String> newMultiState = new HashMap<String, String>();

		for (int i = 0; i < this.pins.size(); i++) {
			Pin pin = this.pins.get(i);
			if (pin.getNumOfLines() == 1) {
				if (pin.isHighZ()) {
					newState = addSignalToClkState(TRI, newState, i);
				} else if (pin.getBoolVal()) {
					newState = addSignalToClkState(ONE, newState, i);
				} else {
					newState = addSignalToClkState(ZERO, newState, i);
				}
			} else {
				if (pin.isHighZ()) {
					newMultiState.put(pin.getName(), TRISTATE);
				} else {
					newMultiState.put(pin.getName(), "" + pin.getIntVal());
				}
			}
		}
		this.history.add(newState);
		this.multiHistrory.add(newMultiState);

		update();
	}

	public String addSignalToClkState(int signalState, String clkState, int i) {
		try {
			String newValue = Integer.toHexString(signalState);
			return clkState.substring(0, i) + newValue
					+ clkState.substring(i + 1);
		} catch (Exception e) {
			return clkState;
		}
	}

	public int getHistorySize() {
		return this.history.size();
	}

	public List<String> getHistory() {
		return this.history;
	}

	public void setHistory(ArrayList<String> history) {
		this.history = history;
	}

	public List<Map<String, String>> getMultiHistrory() {
		return multiHistrory;
	}

	public void setMultiHistrory(List<Map<String, String>> multiHistrory) {
		this.multiHistrory = multiHistrory;
	}

	public void clearHistory(int beginIndex) {
		int hsize = getHistorySize();
		for (int i = beginIndex; i < hsize; i++) {
			this.history.remove(beginIndex);
			this.multiHistrory.remove(beginIndex);
		}
		update();
	}

	public boolean isEmpty() {
		return signals == null || signals.size() == 0;
	}

	public int getSignalFromHistory(int clk, String name) {
		if (this.signals.containsKey(name)) {
			int index = this.signals.get(name);
			int val = Integer.parseInt(
					"" + ((String) this.history.get(clk)).charAt(index), 16);
			// return (val & MASKS[(index % 4)]) >>> (index % 4);
			return val;
		}
		return 0;
	}

	public String getMultiSignalFromHistory(int i, String name) {
		try {
			Map<String, String> tmp = multiHistrory.get(i);
			return tmp.get(name);
		} catch (Exception e) {
			return "NNNN";
		}
	}

	public static interface TimeHistoryObserver {
		public void update();
	}

	public void addObserver(TimeHistoryObserver observer) {
		observers.add(observer);
	}

	public void clearObservers() {
		observers.clear();
	}

	public void update() {
		for (TimeHistoryObserver observer : observers) {
			observer.update();
		}
	}
}
