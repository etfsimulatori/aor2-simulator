package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import logic.LogicalComponent;
import logic.Pin;
import shemes.Schema;

public class NameConnector {

	static Map<String, Pin> pins;
	static {
		pins = new HashMap<String, Pin>();
		pins.put("0", new Pin(false, "0"));
		pins.put("1", new Pin(true, "1"));
	}

	public static void addPins(String component, Map<String, Pin> componentPins) {
		for (String key : componentPins.keySet()) {
			key = key.toLowerCase();
			Pin pin = pins.get(key);
			if (pin == null) {
				pin = componentPins.get(key);
				pins.put(key, pin);
				pins.put(component + "." + key, pin);
			}
		}
	}

	public static void addPins(String component, Pin[] outputPins) {
		component = component.toLowerCase();
		for (Pin outputPin : outputPins) {
			String key = outputPin.getName();
			key = key.toLowerCase();
			pins.put(key, outputPin);
			pins.put(component + "." + key, outputPin);
		}
	}

	public static void addPin(String component, Pin outputPin) {
		addPin(component, outputPin.getName(), outputPin);
	}

	public static void addPin(String component, String pinName, Pin outputPin) {
		pins.put(pinName.toLowerCase(), outputPin);
		pins.put(component.toLowerCase() + "." + pinName.toLowerCase(),
				outputPin);
	}

	public static Pin getPin(String name) {
		name = name.toLowerCase();
		Pin result = pins.get(name);
		if (result == null) {
			result = new Pin(false, name);
			Log.log("Pin["
					+ name
					+ "] nije definisan u konfiguracionom fajlu, a neko ga referencira");
		}
		return result;
	}

	public static Map<String, Pin> listPins() {
		Map<String, Pin> result = new HashMap<String, Pin>();
		for (Map.Entry<String, Pin> entry : pins.entrySet()) {
			if (entry.getKey() != null && !entry.getKey().contains(".")) {
				result.put(entry.getKey(), entry.getValue());
			}
		}
		return result;
	}

	public static List<String> listPinNames() {
		Set<String> temp = new HashSet<String>();
		for (Map.Entry<String, Pin> entry : pins.entrySet()) {
			if (entry.getKey() != null && !entry.getKey().contains(".")) {
				temp.add(entry.getKey());
			}
		}
		List<String> result = new ArrayList<String>(temp);
		Collections.sort(result);

		return result;
	}

	static Map<String, LogicalComponent> components;

	public static void addComponents(String componentName,
			List<LogicalComponent> componentList) {
		if (components == null) {
			components = new HashMap<String, LogicalComponent>();
		}
		int i = 0;
		for (LogicalComponent subComponent : componentList) {
			String subComponentName = subComponent.getName();
			LogicalComponent comp = components.get(subComponentName);
			if (comp == null) {
				if (subComponentName == null || "".equals(subComponentName)) {
					subComponentName = componentName + "." + i;
					components.put(subComponentName, subComponent);
				} else {
					components.put(subComponentName, subComponent);
					components.put(componentName + "." + subComponentName,
							subComponent);

				}
			}
		}
	}

	public static void addComponent(String componentParent,
			String componentName, LogicalComponent component) {
		if (components == null) {
			components = new HashMap<String, LogicalComponent>();
		}
		components.put(componentName, component);
		components.put(componentParent + "." + componentName, component);
	}

	public static LogicalComponent getComponent(String name) {
		return components.get(name);
	}

	public static Map<String, LogicalComponent> listComponents() {
		Map<String, LogicalComponent> result = new HashMap<String, LogicalComponent>(
				components);
		return result;
	}

	static Map<String, Schema> schemas;

	public static void addSchema(String schemaName, Schema schema) {
		if (schemas == null) {
			schemas = new HashMap<String, Schema>();
		}
		schemas.put(schemaName, schema);
	}

	public static Schema getSchema(String schemaName) {
		Schema result = schemas.get(schemaName);
		return result;

	}

	public static Map<String, Schema> listSchemas() {
		Map<String, Schema> result = new HashMap<String, Schema>(schemas);
		return result;
	}

}
