package util;

public class IndexExtractor {
	public static String extractShortName(String longName) {
		String[] data = longName.split(",");
		String prefix = "";
		int startIndex = -1;
		int lastIndex = -1;
		int currenttIndex = 0;
		int cnt = 0;
		String result = "";

		for (int i = 0; i < data.length; i++) {
			if (isValidFormat(data[i])) {

				String currentPrefix = extractPrefix(data[i]);
				currenttIndex = Integer.parseInt(data[i]
						.substring(currentPrefix.length()));
				if (i == 0) {
					startIndex = currenttIndex;
					lastIndex = currenttIndex;
					cnt = 0;
					prefix = currentPrefix;
				}
				if (prefix.equals(currentPrefix)) {
					if (lastIndex == currenttIndex - 1) {
						lastIndex = currenttIndex;
						if (cnt > 1) {
							result = prefix + lastIndex + "#" + cnt + ","
									+ result;
							cnt = 1;
							startIndex = currenttIndex;
						}
					} else {
						if (lastIndex == currenttIndex) {
							if (startIndex != lastIndex) {
								result = prefix + lastIndex + ".." + startIndex
										+ "," + result;
								startIndex = currenttIndex;
								cnt = 0;
							}
							cnt++;
						} else {
							if (cnt == 0) {
								result = prefix + lastIndex + "," + result;
							} else if (cnt == 1) {
								if (startIndex == lastIndex) {
									result = prefix + lastIndex + "," + result;
								} else {
									result = prefix + lastIndex + ".."
											+ startIndex + "," + result;
								}
							} else {
								result = prefix + lastIndex + "#" + cnt + ","
										+ result;
							}

							startIndex = currenttIndex;
							lastIndex = currenttIndex;
							cnt = 1;
							prefix = currentPrefix;
						}
					}
				}
			}
		}
		if (cnt == 0) {
			result = prefix + currenttIndex + "," + result;
		} else if (cnt == 1) {
			if (startIndex == currenttIndex) {
				result = prefix + currenttIndex + "," + result;
			} else {
				result = prefix + currenttIndex + ".." + startIndex + ","
						+ result;
			}
		} else {
			result = prefix + currenttIndex + "#" + cnt + "," + result;
		}
		if (result.endsWith(",")) {
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}

	private static String extractPrefix(String string) {
		return "IR";
	}

	private static boolean isValidFormat(String data) {
		return data.startsWith("IR");
	}
}
