package util;

import javax.swing.JTextArea;

import main.MessageDialog;

public class Log {
	public static JTextArea logArea;
	public static StringBuffer buffer = new StringBuffer();

	public static void log(String data) {
		try {
			if (logArea != null) {
				logArea.append(data.trim() + "\n");
			} else {
				System.out.println(data);
				buffer.append(data.trim()).append("\n");

				String[] lines = data.split("\\n");
				if (lines.length > 0 && lines[0].contains(":")) {
					String msg = lines[0].substring(lines[0].indexOf(":") + 1);
					MessageDialog.sendMessage(msg.trim(), "Error");
				}
			}
		} catch (Exception e) {

		}
	}

	public static void log(Exception data) {
		try {
			String s = data.toString();
			if (logArea != null) {
				logArea.append(s.trim() + "\n");
			} else {
				System.out.println(s.trim());
				buffer.append(s.trim()).append("\n");

				String[] lines = s.split("\\n");
				if (lines.length > 0 && lines[0].contains(":")) {
					String msg = lines[0].substring(lines[0].indexOf(":") + 1);
					MessageDialog.sendMessage(msg.trim(), "Error");
				}
			}
		} catch (Exception e) {

		}
	}
}
