package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MicrocodeConverter {

	private static int NOJUMP = 0;
	private static final String NOJUMPS = "next";
	private static int UNCOND = 1;
	private static final String UNCONDS = "br";
	private static int BRADR = 22;
	private static final String BRADRS = "bradr";
	private static int BROPR = 23;
	private static final String BROPRS = "bropr";
	private static int BRINTR = 24;
	private static final String BRINTRS = "brintr";

	public static String[][] codes = { { "ldmar", "1" }, { "incmar", "2" }, { "ldmdr", "3" }, { "mdrout1", "4" },
			{ "most1_3", "5" }, { "most2_3", "6" }, { "most1_2", "7" }, { "most2_1", "8" }, { "most3_2", "9" },
			{ "rd", "10" }, { "wr", "11" }, { "stio", "12" }, { "ldir", "13" }, { "irimm18out1", "14" },
			{ "irimm18out2", "15" }, { "irbrnchout2", "16" }, { "irjmpout2", "17" }, { "irintout3", "18" },
			{ "irshout1", "19" }, { "wrgpr", "20" }, { "incgpr", "21" }, { "decgpr", "22" }, { "gprdstout1", "23" },
			{ "gprsrcout2", "24" }, { "pcout1", "25" }, { "spout3", "26" }, { "ldpc", "27" }, { "incpc", "28" },
			{ "decpc", "29" }, { "ldsp", "30" }, { "incsp", "31" }, { "decsp", "32" }, { "add", "33" }, { "sub", "34" },
			{ "inc", "35" }, { "dec", "36" }, { "and", "37" }, { "or", "38" }, { "xor", "39" }, { "not", "40" },
			{ "shr", "41" }, { "shl", "42" }, { "ldaw", "43" }, { "aluout3", "44" }, { "bsout3", "45" },
			{ "awout1", "46" }, { "stpswi", "47" }, { "clpswi", "48" }, { "stpswt", "49" }, { "clpswt", "50" },
			{ "ldn", "51" }, { "ldz", "52" }, { "ldc", "53" }, { "ldv", "54" }, { "ldl", "55" }, { "clstart", "56" },
			{ "ldpsw", "57" }, { "pswout1", "58" }, { "stprins", "59" }, { "clprins", "60" }, { "stprcod", "61" },
			{ "clprcod", "62" }, { "stpradr", "63" }, { "clpradr", "64" }, { "clprinm", "65" }, { "clintr", "66" },
			{ "uintout3", "67" }, { "uextout3", "68" }, {"decspout3", "69"} };

	public void parseIn(String inFile, String outFile) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(inFile));
			List<String> lines = new LinkedList<String>();
			String s;
			while ((s = in.readLine()) != null) {
				lines.add(s);
			}
			in.close();
			List<MicrocodeData> microcode = toMicrocode(lines);

			PrintWriter out = new PrintWriter(new FileWriter(outFile));
			for (MicrocodeData code : microcode) {
				if (code != null && code.tupe == MicrocodeData.OK) {
					out.println(code.microcode);
				} else {
					out.println(MicrocodeData.EMPTYCODE);
				}
			}
			out.close();

		} catch (Exception e) {
			Log.log(e);
		}
	}

	public void parseOut(String inFile, String outFile) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(inFile));
			List<String> lines = new LinkedList<String>();
			String s;
			while ((s = in.readLine()) != null) {
				lines.add(s);
			}
			in.close();
			List<MicrocodeData> microcode = toMicrocodeFromBin(lines);
			List<String> data = fromMicrocode(microcode);

			PrintWriter out = new PrintWriter(new FileWriter(outFile));
			for (String code : data) {
				if (code != null) {
					out.println(code);
				} else {
					out.println("!-----!");
				}
			}
			out.close();

		} catch (Exception e) {
			Log.log(e);
		}
	}

	private List<MicrocodeData> toMicrocodeFromBin(List<String> lines) {
		List<MicrocodeData> result = new LinkedList<MicrocodeData>();

		int i = 0;
		for (String line : lines) {
			if (line != null && !line.equals("")) {
				line = line.trim();
				int com = line.indexOf(' ');
				if (com > 0) {
					line = line.substring(0, com);
				}
				com = line.indexOf(';');
				if (com > 0) {
					line = line.substring(0, com);
				}
				com = line.indexOf('\t');
				if (com > 0) {
					line = line.substring(0, com);
				}
				MicrocodeData code = new MicrocodeData();
				code.line = i++;
				code.microcode = line;
				code.tupe = MicrocodeData.OK;
				result.add(code);
			}
		}

		return result;
	}

	public List<MicrocodeData> toMicrocode(List<String> operations) {
		List<MicrocodeData> result = new ArrayList<MicrocodeData>();
		for (int i = 0; i < 256; i++) {
			result.add(null);
		}
		int i = 0;
		for (String operation : operations) {
			MicrocodeData data = toMicrocode(operation);
			switch (data.tupe) {
			case MicrocodeData.OK:
				result.set(data.line, data);
				break;
			case MicrocodeData.NODATA:
				break;
			case MicrocodeData.ERR:
				throw new RuntimeException("invalid data in line " + i + ": " + operation);
			}
			i++;
		}
		return result;
	}

	public MicrocodeData toMicrocode(String line) {
		MicrocodeData result = new MicrocodeData();
		char[] code = result.microcode.toCharArray();
		try {
			line = prepareLine(line);
			String microcodeAdr = extractAdderess(line);

			String[] args = extractOperands(line);

			if (args.length > 0) {
				if (isControlBranch(args)) {
					putHexAtPosition(code, 0);
					String arg = args[0];

					int branchCode = extractBranchCode(arg);

					for (int i = 8; i >= 1; i--) {
						if (branchCode % 2 == 1) {
							putHexAtPosition(code, i);
						}
						branchCode = branchCode / 2;
						if (branchCode == 0)
							break;
					}

					if (isConditionsBranch(arg) || isUnonditionsBranch(arg)) {
						int branchAdr = extractBranchAdr(arg);

						for (int i = 16; i >= 9; i--) {
							if (branchAdr % 2 == 1) {
								putHexAtPosition(code, i);
							}
							branchAdr = branchAdr / 2;
							if (branchAdr == 0)
								break;
						}

					}

					result.tupe = MicrocodeData.OK;
					result.line = Integer.parseInt(microcodeAdr.substring(4, microcodeAdr.length()), 16);
				} else if (isOperationCodes(args)) {
					for (String arg : args) {
						int position = cetPosition(arg);
						putHexAtPosition(code, position);
						result.tupe = MicrocodeData.OK;
						result.line = Integer.parseInt(microcodeAdr.substring(4, microcodeAdr.length()), 16);
					}

				} else {
					throw new RuntimeException("PARSING ERROR!");
				}
			}
		} catch (Exception e) {
			Log.log(e);
			result.tupe = MicrocodeData.ERR;
		}
		result.microcode = new String(code);
		return result;
	}

	private boolean isControlBranch(String[] args) {
		if (args.length != 1)
			return false;

		if (isUnonditionsBranch(args[0]) || isConditionsBranch(args[0]) || isAddressBranch(args[0])
				|| isOperationBranch(args[0]) || isInterruptBranch(args[0]))
			return true;
		else
			return false;
	}

	private boolean isOperationCodes(String[] args) {
		for (String arg : args) {
			if (!isCode(arg))
				return false;
		}

		return true;
	}

	private void putHexAtPosition(char[] code, int position) {
		int nibl = position / 4;
		int val = position % 4;
		char c = code[nibl];
		int d = Integer.parseInt("" + c, 16);
		d = d | (1 << (3 - val));
		c = Integer.toHexString(d).charAt(0);
		code[nibl] = c;
	}

	private String[] extractOperands(String line) {

		if (line == null || line.equals("")) {
			return new String[0];
		}
		line = line.toLowerCase();
		int index = line.indexOf(' ');
		if (index >= 0) {
			line = line.substring(index);
			line = line.trim();
		}
		String args[] = line.split(",");
		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].trim();
		}
		return args;
	}

	private String extractAdderess(String line) {
		String result = "";
		if (line != null && !line.equals("")) {
			int i = line.indexOf(' ');
			if (i >= 0) {
				result = line.substring(0, i);
				result = result.trim();
			}
		}
		return result;
	}

	protected String prepareLine(String operation) {

		operation = operation.toLowerCase();
		int len = -1;
		while (len != operation.length()) {
			len = operation.length();
			operation = operation.replace("  ", " ");
		}
		operation = operation.replace("if !", "if #");
		int index = operation.indexOf('!');
		if (index >= 0) {
			operation = operation.substring(0, index);
		}
		index = operation.indexOf(';');
		if (index > 0) {
			operation = operation.substring(0, index);
		}
		operation = operation.trim();
		operation = operation.replace('\t', ' ');
		len = -1;
		while (len != operation.length()) {
			len = operation.length();
			operation = operation.replace("  ", " ");
		}
		return operation;
	}

	private int extractBranchAdr(String arg) {
		int result = 0;
		try {
			arg = arg.replace('(', ' ');
			arg = arg.replace(')', ' ');
			String adr = arg.substring(arg.indexOf(" madr") + " madr".length(), arg.length());
			adr = adr.trim();
			String[] adrs = adr.split(" ");
			result = Integer.parseInt(adrs[0], 16);
		} catch (Exception e) {
			throw new RuntimeException(arg);
		}
		return result;
	}

	static Map<String, Integer> branches;

	private int extractBranchCode(String arg) {
		int result = 1;
		try {
			if (isConditionsBranch(arg)) {
				String operation = arg.substring(arg.indexOf("if ") + "if ".length(), arg.length());
				operation = operation.substring(0, operation.indexOf(' '));
				operation = operation.trim();
				if (branches.containsKey(operation)) {
					result = branches.get(operation);
				} else {
					if (operation.contains(".")) {
						String[] data = operation.split("\\.");
						operation = data[data.length - 1];
						operation = operation.trim();
					}
					result = branches.get(operation);
				}
			} else if (isAddressBranch(arg)) {
				result = BRADR;
			} else if (isOperationBranch(arg)) {
				result = BROPR;
			} else if (isInterruptBranch(arg)) {
				result = BRINTR;
			} else if (isUnonditionsBranch(arg)) {
				result = UNCOND;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(arg);
		}
		return result;
	}

	private boolean isConditionsBranch(String arg) {
		return arg.contains(UNCONDS + " ") && (arg.indexOf("if ") >= 0);
	}

	private boolean isUnonditionsBranch(String arg) {
		return arg.contains(UNCONDS + " ") && (arg.indexOf("if ") < 0);
	}

	private boolean isAddressBranch(String arg) {
		return arg.contains(BRADRS);
	}

	private boolean isOperationBranch(String arg) {
		return arg.contains(BROPRS);
	}

	private boolean isInterruptBranch(String arg) {
		return arg.contains(BRINTRS);
	}

	static Map<String, Integer> codesFields;

	private int cetPosition(String arg) {
		Integer i = codesFields.get(arg);
		if (i == null) {
			return 0;
		} else {
			return i;
		}
	}

	static Map<Integer, String> fieldCodes;

	private String cetCode(Integer arg) {
		String result = fieldCodes.get(arg);
		return result;
	}

	static Map<Integer, String> conditionCodes;

	private String getCondition(Integer arg) {
		String result = conditionCodes.get(arg);
		return result;
	}

	static Set<String> operations;

	private boolean isCode(String arg) {
		return operations.contains(arg);
	}

	public List<String> fromMicrocode(List<MicrocodeData> microcode) {
		List<String> result = new LinkedList<String>();

		for (MicrocodeData code : microcode) {
			String operation = fromMicrocode(code);
			result.add(operation);
		}

		return result;
	}

	public String fromMicrocode(MicrocodeData microcode) {
		String result = "";

		String adr = toAddress(microcode.line);
		result += adr + " ";

		int num = 0;
		String str = microcode.microcode.substring(0, 1);
		int brOrOpr = Integer.parseInt(str, 16);
		brOrOpr = brOrOpr >> 3;
		if (brOrOpr == 0) {
			// form operational signals
			char c = microcode.microcode.charAt(0);
			int posi = Integer.parseInt("" + c, 16);
			for (int j = 0; j < 3; j++) {
				if ((posi & 1) == 1) {
					int index = 0 * 4 + 3 - j;
					String opr = cetCode(index);
					if (opr != null && !opr.equals("")) {
						if (num != 0) {
							result += ", ";
						}
						result += opr;
						num++;
					}
				}
				posi = posi >> 1;
			}

			for (int i = 1; i < 84 / 4; i++) {
				c = microcode.microcode.charAt(i);
				posi = Integer.parseInt("" + c, 16);

				for (int j = 0; j < 4; j++) {
					if ((posi & 1) == 1) {
						int index = i * 4 + 3 - j;
						String opr = cetCode(index);
						if (opr != null && !opr.equals("")) {
							if (num != 0) {
								result += ", ";
							}
							result += opr;
							num++;
						}
					}
					posi = posi >> 1;
				}
			}

		} else {
			// form branches
			String branchCodeString = microcode.microcode.substring(0, 3);
			int branchCode = Integer.parseInt(branchCodeString, 16);
			branchCode = branchCode>>3;
			branchCode = branchCode & 0b011111111;
			if (branchCode == UNCOND) {
				String adrString = toAddress1(microcode.microcode.substring(2, 5));
				if (num != 0) {
					result += ", ";
				}
				result += "br " + adrString;

			} else if (branchCode == BRADR) {

				if (num != 0) {
					result += ", ";
				}
				result += BRADRS;
			} else if (branchCode == BROPR) {

				if (num != 0) {
					result += ", ";
				}
				result += BROPRS;
			} else if(branchCode == BRINTR){
				
				if (num != 0) {
					result += ", ";
				}
				result += BRINTRS;
			}else {
				String condition = getCondition(branchCode);
				if (condition != null && !condition.equals("")) {
					condition = condition.replace('#', '!');
					String adrString = toAddress1(microcode.microcode.substring(2, 5));
					if (num != 0) {
						result += ", ";
					}
					result += "br (if " + condition + " then " + adrString + ")";
				}
			}
		}
		result += ";";
		return result;
	}

	public String toAddress1(String adr) {
		if (adr == null || adr.equals("")) {
			throw new RuntimeException("Not a valid address");
		}
		int hexInt = Integer.parseInt(adr, 16);
		hexInt = hexInt >> 3;
		hexInt = hexInt & 0b011111111;
		String result = Integer.toString(hexInt, 16);
		if (result.length() == 1) {
			result = "0" + result;
		}
		result = "madr" + result.toUpperCase();
		return result;
	}
	
	public String toAddress(String adr) {
		if (adr == null || adr.equals("")) {
			throw new RuntimeException("Not a valid address");
		}
		String result = adr;
		if (result.length() == 1) {
			result = "0" + result;
		}
		result = "madr" + result.toUpperCase();
		return result;
	}

	public String toAddress(int adr) {
		return toAddress(Integer.toHexString(adr));
	}

	static {
		codesFields = new HashMap<String, Integer>();
		for (String[] lin : codes) {
			codesFields.put(lin[0], Integer.parseInt(lin[1]));
		}

		fieldCodes = new HashMap<Integer, String>();
		for (String[] lin : codes) {
			fieldCodes.put(Integer.parseInt(lin[1]), lin[0]);
		}

		operations = codesFields.keySet();

		branches = new HashMap<String, Integer>();
		String[][] t = { { "#start", "2" }, { "hack", "3" }, { "#fccpu", "4" }, { "#gropr", "5" }, { "l1", "6" },
				{ "#gradr", "7" }, { "l2_brnch", "8" }, { "l2_arlog", "9" }, { "l3_jump", "10" }, { "l3_arlog", "11" },
				{ "store", "12" }, { "ldw", "13" }, { "regdir", "14" }, { "#brpom", "15" }, { "#prekid", "16" },
				{ "#prins", "17" }, { "#prcod", "18" }, { "#pradr", "19" }, { "#prinm", "20" }, { "#printr", "21" } };
		for (String[] lin : t) {
			branches.put(lin[0], Integer.parseInt(lin[1]));
		}

		conditionCodes = new HashMap<Integer, String>();
		for (String[] lin : codes) {
			conditionCodes.put(Integer.parseInt(lin[1]), lin[0]);
		}

	}

	public static void load(String[][] init) {
		loadBranchCodes(init);
		loadBranches(init);
		MicrocodeData.init();
	}

	public static void loadBranchCodes(String[][] init) {
		for (int i = 0; i < init.length; i++) {
			String[] data = init[i];
			int index = Integer.parseInt(data[0]);
			if (data.length > 3) {
				String name = data[3];
				if (NOJUMPS.equals(name)) {
					NOJUMP = index;
				}
				if (UNCONDS.equals(name)) {
					UNCOND = index;
				}
				if (BRADRS.equals(name)) {
					BRADR = index;
				}
				if (BROPRS.equals(name)) {
					BROPR = index;
				}
				if (BRINTRS.equals(name)) {
					BRINTR = index;
				}
			}
		}

	}

	public static void loadBranches(String[][] init) {
		for (int i = 0; i < init.length; i++) {
			String[] data = init[i];
			int index = Integer.parseInt(data[0]);
			String name = data[1].toLowerCase().trim();
			int poslition = name.indexOf('.');
			if (poslition > 0) {
				name = name.substring(poslition + 1);
			}
			String cmp = data[2].toLowerCase().trim();

			branches.put(cmp + name, index);

			conditionCodes.put(index, cmp + name);
		}
	}

	public static class MicrocodeData {
		public static String EMPTYCODE = "0000000000000000000000000000";
		public static final int OK = 0;
		public static final int NODATA = 1;
		public static final int ERR = 2;

		String microcode;
		int line;
		int tupe;
		String comment;

		public MicrocodeData() {
			microcode = EMPTYCODE;
			line = 0;
			tupe = NODATA;
			comment = null;
		}

		public static void init() {
			char[] code = EMPTYCODE.toCharArray();
			int branchCode = NOJUMP;

			String bCode = "0" + Integer.toHexString(branchCode);
			bCode = bCode.substring(bCode.length() - 2, bCode.length());
			char[] cCode = bCode.toCharArray();
			for (int i = 0; i < 2; i++) {
				code[i + 96 / 4] = cCode[i];
			}

			EMPTYCODE = new String(code);
		}
	}

	public List<String> parseIn(String inFile) {
		List<String> result = new LinkedList<String>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(inFile));
			List<String> lines = new LinkedList<String>();
			String s;
			while ((s = in.readLine()) != null) {
				lines.add(s);
			}
			in.close();
			List<MicrocodeData> microcode = toMicrocode(lines);

			for (MicrocodeData code : microcode) {
				if (code != null && code.tupe == MicrocodeData.OK) {
					result.add(code.microcode);
				} else {
					result.add(MicrocodeData.EMPTYCODE);
				}
			}

		} catch (Exception e) {
			Log.log(e);
		}
		return result;
	}

	public static void main(String args[]) {
		MicrocodeConverter c = new MicrocodeConverter();
		Parameters.init("./Konfiguracija.txt");
		load(Parameters.controlUnitDecoder);

		c.parseIn("./tmp/microProgram-1.txt", "o1.txt");
		c.parseOut("o1.txt", "./o2.txt");
	}

}
