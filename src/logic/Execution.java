package logic;

import java.util.ArrayList;
import java.util.List;

import util.TimeHistory;

public class Execution {

	public static int globalTime = 0;

	public static void nextCLK() {

		for (LogicalComponent seq : allSequentialComponents) {
			seq.execute();
		}

		for (LogicalComponent seq : allSequentialComponents) {
			seq.flush();
		}
		globalTime++;
		history.addClkState();
	}

	public static void init(boolean fullInit) {
		for (LogicalComponent comp : allComponents) {
			comp.init(fullInit);
		}

		for (LogicalComponent comp : allComponents) {
			comp.flush();
		}
		globalTime = 0;
		history.clearHistory(0);
	}

	static List<LogicalComponent> allSequentialComponents = new ArrayList<LogicalComponent>();

	static List<LogicalComponent> allComponents;

	static TimeHistory history;

	public static void setHistory(TimeHistory tdh) {
		history = tdh;
	}

	public static TimeHistory getHistory() {
		return history;
	}

	public static void addSequentialComponent(LogicalComponent logcomp) {
		allSequentialComponents.add(logcomp);
	}

	public static List<LogicalComponent> getSequentialComponent() {
		return allSequentialComponents;
	}

	public static List<LogicalComponent> getComponent() {
		return allComponents;
	}

	public static void setComponent(List<LogicalComponent> comps) {
		allComponents = comps;
	}
}
