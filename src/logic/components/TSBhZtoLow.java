package logic.components;

public class TSBhZtoLow extends TSB {

	public TSBhZtoLow(String name) {
		super(name);
	}
	
	@Override
	public void func() {
		
		if (enable.getBoolVal()) {
			if (in[0].isHighZ()) {
				if (out[0].isBool()) {
					out[0].setBoolVal(false);
				} else {
					out[0].setIntVal(0);
				}
			} else if (out[0].isBool()) {
				out[0].setBoolVal(in[0].getBoolVal());
			} else {
				out[0].setIntVal(in[0].getIntVal());
			}
		} else {
			out[0].setHighZ(true);
			// out[0].clear();
		}
		
		lastIn.copyVal(in[0]);
		lastE.copyVal(enable);
		lastOut.copyVal(out[0]);

	}

}
