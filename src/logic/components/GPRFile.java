package logic.components;

import java.util.ArrayList;

import logic.Execution;
import logic.LogicalComponent;
import logic.Pin;

public class GPRFile extends LogicalComponent {
	private static final int SRC = 0;
	private static final int DST = 1;
	private static final int DOUT = 2;
	
	private int size;
	
	private Pin clk;
	
	private ArrayList<REG> regs;
	private ArrayList<AND> ands;
	
	private IntToBools [] addressBits = new IntToBools[2];
	
	private DC [] decoders = new DC[2];
	
	private MP [] multiplexers = new MP[2];
	

	public GPRFile(int size){
		super(3, 2);
		this.size = size;
		
		int numberOfPins = (int) (Math.log(size) / Math.log(2));
		
		addressBits[SRC] = new IntToBools(numberOfPins, numberOfPins);
		addressBits[DST] = new IntToBools(numberOfPins, numberOfPins);
		
		out[SRC].setIsInt();
		out[SRC].setNumOfLines(32);
		
		out[DST].setIsInt();
		out[DST].setNumOfLines(32);
		
		decoders[SRC] = new DC(numberOfPins);
		decoders[SRC].setE(new Pin(true, ""));
		
		for(int i = 0; i<numberOfPins; i++){
			decoders[SRC].setInPin(i, addressBits[SRC].getOutPin(i));
		}
		
		multiplexers[SRC] = new MP(size);
		multiplexers[SRC].setOutPin(0, this.out[SRC]);
		for(int i = 0; i<numberOfPins; i++){
			multiplexers[SRC].setCtrl(i, addressBits[SRC].getOutPin(i));
		}
		
		decoders[DST] = new DC(numberOfPins);
		decoders[DST].setE(new Pin(true, ""));
	
		for(int i = 0; i<numberOfPins; i++){
			decoders[DST].setInPin(i, addressBits[DST].getOutPin(i));
		}
		
		multiplexers[DST] = new MP(size);
		multiplexers[DST].setOutPin(0, this.out[DST]);
		for(int i = 0; i<numberOfPins; i++){
			multiplexers[DST].setCtrl(i, addressBits[DST].getOutPin(i));
		}
		
		regs = new ArrayList<REG>(size);
		ands = new ArrayList<AND>(size);
		for(int i = 0; i<size; i++){
			AND ldreg = new AND();
			ldreg.setInPin(0, decoders[DST].getOutPin(i));
			ands.add(ldreg);
			
			REG r = new REG(1, "R" + i);
			r.getOutPin(0).setIsInt();
			r.getOutPin(0).setNumOfLines(32);
			r.setPinLd(ldreg.getOutPin(0));
			regs.add(r);
			
			multiplexers[SRC].setInPin(i, r.getOutPin(0));
			multiplexers[DST].setInPin(i, r.getOutPin(0));
			
			//Execution.addSequentialComponent(r);
		}
		
		
		Execution.addSequentialComponent(this);
	}

	@Override
	public void func() {
		if ((clk != null) && (!clk.getBoolVal())) {
			return;
		}
		multiplexers[SRC].func();
		multiplexers[DST].func();
	}
	
	public int getSize() {
		return size;
	}
	
	public void setRead(Pin read) {
		read.addChild(this);
		multiplexers[SRC].setE(read);
		multiplexers[DST].setE(read);
	}
	
	public void setWrite(Pin write){
		for(int i = 0; i<size; i++){
			ands.get(i).setInPin(1, write);
			write.addChild(ands.get(i));
		}
	}
	
	public void setAdressPin(Pin adressSrc, Pin adressDst) {
		in[SRC] = adressSrc;
		in[DST] = adressDst;
		
		addressBits[SRC].setInPin(0,  adressSrc);
		adressSrc.addChild(this);
		adressSrc.addChild(addressBits[SRC]);
		
		addressBits[DST].setInPin(0, adressDst);
		adressDst.addChild(this);
		adressDst.addChild(addressBits[DST]);
	}
	
	public void setInputDataPin(Pin datain) {
		in[DOUT] = datain;
		for (int i = 0; i < size; i++) {
			regs.get(i).setInPin(0, datain);
			datain.addChild(regs.get(i));
		}

	}
	
	public int read(int adress) {
		return regs.get(adress).getVal();
	}

	public REG getREG(int i) {
		return regs.get(i);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}
	
	public void setClk(CLK clk) {
		this.clk = clk.getClk();
		this.period = clk.getPeriod();
		for (REG reg : regs) {
			reg.setClk(clk);
		}
	}
	
	public static void main(String args[]){
		GPRFile file = new GPRFile(16);
		Pin read = new Pin(false, "r");
		
		Pin write = new Pin(false, "w");
		Pin dataIn = new Pin(25, 32, "dataIn");
		dataIn.setIsInt();
		Pin addr1 = new Pin(0, 2, "adr1");
		addr1.setIsInt();
		Pin addr2 = new Pin(1, 2, "adr2");
		addr2.setIsInt();
		
		
		file.setRead(read);
		file.setWrite(write);
		file.setInputDataPin(dataIn);
		file.setAdressPin(addr1, addr2);
		
		write.setBoolVal(true);
		dataIn.setIntVal(234);
		addr1.setIntVal(1);
		addr2.setIntVal(2);
		write.setBoolVal(true);
		
		for(int i = 0; i<16; i++){
			dataIn.setIntVal(i);
			addr2.setIntVal(i);
			Execution.nextCLK();
			System.out.println("UPISAO U REGISTAR R" + i);
		}
		
		write.setBoolVal(false);
		read.setBoolVal(true);
		
		for(int i = 0; i<16; i++){
			addr1.setIntVal(i);
			addr2.setIntVal(i);
			
			System.out.println("R0 adr1: " + file.getOutPin(0).getIntVal() + " R1 adr2: " + file.getOutPin(1).getIntVal());
		}
		
		read.setBoolVal(false);
		
		/*Execution.setComponent(Netlist.extractAllComponent(Execution
				.getSequentialComponent()));
		Execution.init(true);*/
		Execution.nextCLK();
		
	}
	

}
