package logic.components;

import logic.LogicalComponent;

public class INC extends LogicalComponent {
	int size;
	private int mask = 0xff;
	
	public INC(int size){
		super(1, 1);
		out[0].setIsInt();
		out[0].setNumOfLines(size);
		
		mask = 0;
		for (int i = 0; i < size; i++) {
			mask = (mask << 1) | 1;
		}
		this.size = size;
	}

	@Override
	public void func() {
		
		int A = in[0].getIntVal();
		int output = A + 1;
		output = output & mask;
		
		out[0].setIntVal(output);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
