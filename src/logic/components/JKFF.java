package logic.components;

import logic.Pin;

public class JKFF extends FF {
	public JKFF(String name) {
		super(2, name);
	}

	public void func() {
		if ((clk != null) && (!clk.getBoolVal())) {
			return;
		}
		if ((reset != null) && (reset.getBoolVal())) {
			state = false;
			return;
		}
		// JKFF func: J(!Q)+kQ
		state = (in[0].getBoolVal() && out[1].getBoolVal())
				|| ((!in[1].getBoolVal()) && (out[0].getBoolVal()));
	}

	public void setPinK(Pin r) {
		setInPin(1, r);
	}

	public void setPinJ(Pin s) {
		setInPin(0, s);
	}

}
