package logic.components;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import logic.LogicalComponent;
import logic.Pin;
import util.Log;
import util.MicrocodeConverter;
import util.Parameters;

public class MicroMemory extends LogicalComponent {

	private String[] memory;

	public MicroMemory(int size) {
		super(1, 6);

		out[0].setIsInt();
		out[0].setNumOfLines(1);
		out[1].setIsInt();
		out[1].setNumOfLines(8);
		out[2].setIsInt();
		out[2].setNumOfLines(8);
		out[3].setIsInt();
		out[3].setNumOfLines(20);
		out[4].setIsInt();
		out[4].setNumOfLines(20);
		out[5].setIsInt();
		out[5].setNumOfLines(20);
		memory = new String[size];

		initFromMikroprogramFile("./microCode.txt");
	}

	@Override
	public void func() {
		Log.log(memory[in[0].getIntVal()]);
		// TODO
		try {
			int hexInt = Integer.parseInt(memory[in[0].getIntVal()].substring(0, 1), 16) >> 3;
			out[0].setIntVal(hexInt);
			hexInt = Integer.parseInt(memory[in[0].getIntVal()].substring(0, 3), 16) >> 3;
			hexInt = hexInt & 0b011111111;
			out[1].setIntVal(hexInt);
			hexInt = Integer.parseInt(memory[in[0].getIntVal()].substring(2, 5), 16) >> 3;
			hexInt = hexInt & 0b011111111;
			out[2].setIntVal(hexInt);
			hexInt = Integer.parseInt(memory[in[0].getIntVal()].substring(4, 10), 16) >> 3;
			hexInt = hexInt & 0x0FFFFF;
			out[3].setIntVal(hexInt);
			hexInt = Integer.parseInt(memory[in[0].getIntVal()].substring(9, 15), 16) >> 3;
			hexInt = hexInt & 0x0FFFFF;
			out[4].setIntVal(hexInt);
			hexInt = Integer.parseInt(memory[in[0].getIntVal()].substring(14, 20), 16) >> 3;
			hexInt = hexInt & 0x0FFFFF;
			out[5].setIntVal(hexInt);
		} catch (Exception e) {
			System.out.println(in[0].getIntVal());
			e.printStackTrace();
			Log.log(e);
		}

	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

	protected void initFromFile(String fileName) {
		int counter = 0;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = reader.readLine()) != null) {
				memory[counter++] = line;
			}
		} catch (Exception e) {
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
			}
		}
	}

	protected void initFromMikroprogramFile(String fileName) {
		int counter = 0;
		try {

			MicrocodeConverter.load(Parameters.controlUnitDecoder);

			MicrocodeConverter c = new MicrocodeConverter();

			List<String> lines = c.parseIn(fileName);

			for (String line : lines) {
				memory[counter++] = line;
			}
		} catch (Exception e) {
		}
	}
	
	public static void main(String [] args){
		Parameters.init("./Konfiguracija.txt");
		MicroMemory mm = new MicroMemory(1024);
		mm.setInPin(0, new Pin(1, 8, ""));

		mm.func();

		Log.log(Integer.toBinaryString(mm.getOutPin(0).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(1).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(2).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(3).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(4).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(5).getIntVal()));
	}

}
