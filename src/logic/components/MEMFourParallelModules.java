package logic.components;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import logic.Execution;
import logic.LogicalComponent;
import logic.Pin;
import util.Log;

public class MEMFourParallelModules extends LogicalComponent{
	
	private static final int NUM_OF_MODULES = 4;
	private static final int ADDRESS_IN = 0;
	private static final int DATA_IN = 1;
	private static final int DATA_OUT = 0;
	private static final int MP_IN = 0;
	private static final int MP_OUT = 1;
	
	private MEM [] mems;
	private MP [] multiplexers = new MP[2];
	private BoolsToInt [] inputRotations;
	private BoolsToInt [] outputRotations;
	private IntToInt [] inputParts;
	private IntToBools inputConvert;
	private IntToBools [] outputConvert;
	private IntToInt addressBits;
	private IntToBools addressSelector;
	//private INC addrInc;
	//private MP [] addrMultiplexers = new MP[NUM_OF_MODULES-1];
	//private Pin read;
	//private Pin write;
	private int size;

	public MEMFourParallelModules(int size) {
		super(2, 1);
		
		out[0].setIsInt();
		out[0].setNumOfLines(32);
		
		//Initializing memory modules
		int module_size = size/4;
		this.size = module_size*4;
		mems = new MEM[NUM_OF_MODULES];
		for(int i = 0; i<NUM_OF_MODULES; i++){
			mems[i] = new MEM(module_size);
		}
		
		//Address Input Adjustment
		addressBits = new IntToInt(32, 30, true, false, 0);
		addressSelector = new IntToBools(32, 2);
		
		for(int i = 0; i<NUM_OF_MODULES; i++){
			mems[i].setInPin(ADDRESS_IN, addressBits.getOutPin(0));
		}
		
		
		//Data Input Adjustment
		inputConvert = new IntToBools(32, 32);
		
		inputRotations = new BoolsToInt[NUM_OF_MODULES];
		for(int i = 0; i<NUM_OF_MODULES; i++){
			inputRotations[i] = new BoolsToInt(32, 32);
			for(int j = 0; j<32; j++){
				inputRotations[i].setInPin(j, inputConvert.getOutPin((j + i*8)%32));
			}
		}
		
		multiplexers[MP_IN] = new MP(NUM_OF_MODULES);
		for(int i = 0; i<NUM_OF_MODULES; i++){
			multiplexers[MP_IN].setInPin(i, inputRotations[i].getOutPin(0));
		}
		
		for(int i = 0; i<2; i++){
			multiplexers[MP_IN].setCtrl(i, addressSelector.getOutPin(i));
		}
		
		//multiplexers[MP_IN].setE(new Pin(true, "1"));
		multiplexers[MP_IN].getOutPin(0).setIsInt();
		
		inputParts = new IntToInt[NUM_OF_MODULES];
		for(int i = 0; i<NUM_OF_MODULES; i++){
			inputParts[i] = new IntToInt(32, 8, false, false, i*8);
			inputParts[i].setInPin(0, multiplexers[MP_IN].getOutPin(0));
		}
		
		for(int i = 0; i<NUM_OF_MODULES; i++){
			mems[i].setInPin(DATA_IN, inputParts[i].getOutPin(0));
		}
		
		//Output adjustment
		outputConvert = new IntToBools[NUM_OF_MODULES];
		for(int i = 0; i<NUM_OF_MODULES; i++){
			outputConvert[i] = new IntToBools(8, 8);
			outputConvert[i].setInPin(0, mems[i].getOutPin(0));
		}
		
		outputRotations = new BoolsToInt[NUM_OF_MODULES];
		for(int i = 0; i<NUM_OF_MODULES; i++){
			outputRotations[i] = new BoolsToInt(32, 32);
			for(int j = 0; j<32; j++){
				outputRotations[i].setInPin(j, outputConvert[(j/8 + i)%4].getOutPin(j%8));
			}
		}
		
		multiplexers[MP_OUT] = new MP(NUM_OF_MODULES);
		multiplexers[MP_OUT].setOutPin(0, this.out[DATA_OUT]);
		for(int i = 0; i<NUM_OF_MODULES; i++){
			multiplexers[MP_OUT].setInPin(i, outputRotations[i].getOutPin(0));
		}
		
		for(int i = 0; i<2; i++){
			multiplexers[MP_OUT].setCtrl(i, addressSelector.getOutPin(i));
		}
		
		
		Execution.addSequentialComponent(this);
	}

	@Override
	public void func() {
		multiplexers[MP_IN].func();
		for(int i = 0; i<NUM_OF_MODULES; i++){
			mems[i].func();
		}
		//multiplexers[MP_OUT].func();
		for(int i = 0; i<NUM_OF_MODULES; i++){
			outputConvert[i].func();
		}
	}
	
	public void setInputAddressPin(Pin address){
		in[ADDRESS_IN] = address;
		
		addressBits.setInPin(0, address);
		address.addChild(addressBits);
		
		addressSelector.setInPin(0, address);
		address.addChild(addressSelector);
		
		address.addChild(this);
	}

	public void setInputDataPin(Pin datain){
		in[DATA_IN] = datain;
		inputConvert.setInPin(0, datain);
		datain.addChild(this);
		datain.addChild(inputConvert);
	}
	
	public int getSize(){
		return size;
	}
	
	public void setRead(Pin read) {
		//this.read = read;
		read.addChild(this);
		
		for(int i = 0; i<NUM_OF_MODULES; i++){
			mems[i].setRead(read);
			read.addChild(mems[i]);
		}
		
		multiplexers[MP_OUT].setE(read);
	}

	public void setWrite(Pin write) {
		//this.write = write;
		write.addChild(this);
		
		for(int i = 0; i<NUM_OF_MODULES; i++){
			mems[i].setWrite(write);
			write.addChild(mems[i]);
		}
		
		multiplexers[MP_IN].setE(write);
	}
	
	public void write(int adress, int data) {
		mems[adress%NUM_OF_MODULES].write(adress/4, data);
	}

	public int read(int adress) {
		return mems[adress%NUM_OF_MODULES].read(adress/4);
	}
	
	
	public void initFromFile(String fileName) {
		BufferedReader reader = null;
		try {
			int mask = size - 1;
			reader = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = reader.readLine()) != null) {
				try {

					line = removeComments(line);
					String[] linData = line.split(",");
					if (linData.length == 2) {
						String adr = linData[0].trim();
						String data = linData[1].trim();
						int intAdr = Integer.parseInt(adr, 16);
						intAdr = intAdr & mask;
						int intData = Integer.parseInt(data, 16);
						intData = intData & 0xFF;
						write(intAdr, intData);
					}
				} catch (Exception e) {
					 //e.printStackTrace();
				}
			}
		} catch (Exception e) {
			Log.log(e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				Log.log(e);
			}
		}
	}

	private String removeComments(String line) {
		if (line != null) {
			int index = line.indexOf("//");
			if (index > 0) {
				line = line.substring(0, index);
				line = line.trim();
			}
			line = line.toLowerCase().replace("\"", " ").trim();
		}
		return line;
	}
	
	@Override
	public void initArgs(String[] args) {
		// TODO Auto-generated method stub
		
	}
	
	
	public static void main(String [] args){
		MEMFourParallelModules mem = new MEMFourParallelModules(4*1024); //4kb
		
		Pin data = new Pin(0, 32, "data");
		data.setIsInt();
		Pin addr = new Pin(0, 32, "addr");
		addr.setIsInt();
		Pin wr = new Pin(false, "wr");
		Pin rd = new Pin(false, "rd");
		
		mem.setWrite(wr);
		mem.setRead(rd);
		mem.setInputAddressPin(addr);
		mem.setInputDataPin(data);
		
		wr.setBoolVal(true);
		for(int i = 0; i<10; i++){
			addr.setIntVal(i*4);
			data.setIntVal(i + 250);
			mem.func();
		}
		wr.setBoolVal(false);
		rd.setBoolVal(true);
		for(int i = 0; i<10; i++){
			addr.setIntVal(i*4);
			mem.func();
			System.out.println("Mem[" + (i*4) + "] = " + mem.getOutPin(0).getIntVal());
		}
		rd.setBoolVal(false);
		
		/*MEMFourParallelModules mem = new MEMFourParallelModules(4*1024);
		
		mem.initFromFile("memorija.txt");
		for(int i = 0x80; i<0x89; i++){
			System.out.println(mem.read(i));
		}*/
		
	}

}
