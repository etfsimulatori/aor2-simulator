package logic.components;

import logic.LogicalComponent;

public class KMINTR extends LogicalComponent {
	
	private int[] stepsDec;
	
	public KMINTR(int [] stepsDec) {
		super(stepsDec.length, 1);
		out[0].setIsInt();
		this.stepsDec = stepsDec;
	}

	@Override
	public void func() {
		int i = 0;
		for (; i < in.length; i++) {
			if (in[i].getBoolVal()) {
				break;
			}
		}
		if (i < in.length)
			out[0].setIntVal(stepsDec[i]);
	}

	@Override
	public void initArgs(String[] args) {
		// TODO Auto-generated method stub

	}

}
