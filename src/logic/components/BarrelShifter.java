package logic.components;

import logic.LogicalComponent;
import logic.Pin;
import util.Log;

public class BarrelShifter extends LogicalComponent {

	private Pin shr;
	private Pin shl;
	private Pin C;
	
	private int size;
	private int outMask = 0xff;
	private int nMask = 0xff;
	
	public BarrelShifter(int size){
		super(2, 1);
		out[0].setIsInt();
		out[0].setNumOfLines(size);
		C = new Pin(false, "Cbs");
		allPins.add(C);
		
		outMask = 0;
		for (int i = 0; i < size; i++) {
			outMask = (outMask << 1) | 1;
		}
		
		nMask = 0;
		for(int i = 0; i < calcOut(size); i++){
			nMask = (nMask<<1) | 1;
		}
		this.size = size;
	}

	@Override
	public void func() {
		int akc = 0;
		if((shr!=null) && (shr.getBoolVal())){
			akc++;
		}
		if((shl!=null) && (shl.getBoolVal())){
			akc++;
		}
		
		if (akc > 1) {
			Log.log(":Error!!! Multiple operations active...");
			out[0].setIntVal(0);
			C.setBoolVal(false);
			return;
		}
		
		if (akc == 0) {
			out[0].setIntVal(0);
			C.setBoolVal(false);
			return;
		}
		
		int A = in[0].getIntVal();
		int n = in[1].getIntVal() & nMask;
		
		if(in[1].isHighZ()) n = 1 & nMask;
		
		if((shr!=null) && (shr.getBoolVal())){
			if(n == 0){
				C.setBoolVal(false);
			}
			else if(n>0){
				if(((A>>>(n-1))&1) == 1)
					C.setBoolVal(true);
				else
					C.setBoolVal(false);
			}
			
			int output = A>>>n;
			output = output & outMask;
			out[0].setIntVal(output);
			return; 
		}
		
		if((shl!=null) && (shl.getBoolVal())){
			if(n == 0){
				C.setBoolVal(false);
			}
			else if(n>0){
				if(((A>>>(size-n))&1) == 1)
					C.setBoolVal(true);
				else
					C.setBoolVal(false);
			}
			
			int output = A<<n;
			output = output & outMask;
			out[0].setIntVal(output);
			return; 
		}
		
		
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}
	
	public void setPinX(Pin pin){
		setInPin(0, pin);
	}
	
	public void setPinN(Pin pin){
		setInPin(1, pin);
	}
	
	public void setPinSHR(Pin shr){
		this.shr = shr;
		shr.addChild(this);
	}
	
	public void setPinSHL(Pin shl){
		this.shl = shl;
		shl.addChild(this);
	}
	
	public Pin getPinC_Shift() {
		return C;
	}

	private static int calcOut(int in) {
		int ret = 0;
		while (in > 0) {
			in /= 2;
			if (in <= 0)
				continue;
			ret++;
		}
		return ret;
	}
}
