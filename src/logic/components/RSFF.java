package logic.components;

import logic.Pin;
import util.Log;

public class RSFF extends FF {
	protected boolean err;

	public RSFF(String name) {
		super(2, name);
		err = false;
	}

	public void func() {
		if ((clk != null) && (!clk.getBoolVal())) {
			return;
		}
		if ((reset != null) && (reset.getBoolVal())) {
			state = false;
			return;
		}

		if ((in[0].getBoolVal()) && (in[1].getBoolVal())) {
			Log.log(":Error!!! Unexpected RSFF  state..." + name);
			err = true;
			return;
		}
		err = false;
		state = (in[0].getBoolVal())
				|| ((!in[1].getBoolVal()) && (out[0].getBoolVal()));
	}

	public void setPinR(Pin r) {
		setInPin(1, r);
	}

	public void setPinS(Pin s) {
		setInPin(0, s);
	}
	public void setOutput() {
		out[0].setHighZ(err);
		out[1].setHighZ(err);
		out[0].setBoolVal(state);
		out[1].setBoolVal(!state);
	}
}
