package logic.components;

import logic.Execution;
import logic.Pin;
import util.*;

public class Test {

	public static void main(String[] args) {

		// registar A
		REG a = new REG(1, "A");
		a.getOutPin(0).setIsInt();
		Pin inca = new Pin(false, "incA"); // incA
		a.setPinInc(inca);
		Pin lda = new Pin(false, "ldA"); // ldA
		a.setPinLd(lda);

		// registar B
		REG b = new REG(1, "B");
		b.getOutPin(0).setIsInt();
		Pin incb = new Pin(false, "inB");// incB
		b.setPinInc(incb);
		Pin ldb = new Pin(false, "ldB");// ldB
		b.setPinLd(ldb);

		// ALU jedinica
		ALU alu = new ALU(8);
		alu.setInPin(0, a.getOutPin(0));
		alu.setInPin(1, b.getOutPin(0));
		Pin add = new Pin(false, "add"); // add Pin
		alu.setPinAdd(add);

		// trostaticki bafer na izlazu ALU
		TSB aluout = new TSB("");
		Pin ctrlaluout = new Pin(false, "ctrlaluout");// aluout signal na TSB
		aluout.setE(ctrlaluout);
		aluout.setInPin(0, alu.getOutPin(0));

		// Magistrala
		BUS bus = new BUS("BUS");
		bus.setInPin(0, aluout.getOutPin(0));

		a.setInPin(0, bus.getOutPin(0));
		b.setInPin(0, bus.getOutPin(0));

		inca.setBoolVal(true); // inkrementiramo A;
		incb.setBoolVal(true); // inkrementiramo B;
		add.setBoolVal(true); // aktivno sabiranje
		ctrlaluout.setBoolVal(true); // pustamo na magistralu
		Execution.nextCLK(); // posle signala kloka trebalo bi da je A=1,
								// B=1, a na magistrali 2
		Log.log("A = " + a.getOutPin(0).getIntVal());
		Log.log("; B = " + b.getOutPin(0).getIntVal());
		Log.log("; BUS = " + bus.getOutPin(0).getIntVal());
		inca.setBoolVal(false); // ukidamo inkrementiranje
		incb.setBoolVal(false); // ukidamo inkrementiranje
		lda.setBoolVal(true); // ucitavanje sa magistrale u A
		Execution.nextCLK(); // trebalo bi A=2, B=1, BUS=3
		Log.log("A = " + a.getOutPin(0).getIntVal());
		Log.log("; B = " + b.getOutPin(0).getIntVal());
		Log.log("; BUS = " + bus.getOutPin(0).getIntVal());

	}
}
