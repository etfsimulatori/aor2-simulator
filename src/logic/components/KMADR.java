package logic.components;

import logic.LogicalComponent;

public class KMADR extends LogicalComponent { // @Darko

	private int[] stepsDec = { 26, 30, 32, 34, 43, 45, 48, 58 };

	public KMADR(int[] stepsDec) {
		super(stepsDec.length, 1);
		out[0].setIsInt();
		this.stepsDec = stepsDec;
	}

	public void func() {

		int i = 0;
		for (; i < in.length; i++) {
			if (in[i].getBoolVal()) {
				break;
			}
		}
		if (i < in.length)
			out[0].setIntVal(stepsDec[i]);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
