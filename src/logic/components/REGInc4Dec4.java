package logic.components;

import util.Log;

public class REGInc4Dec4 extends REG {

	public REGInc4Dec4(int in, String name) {
		super(in, name);
	}

	public void func() {
		if ((clk != null) && (!clk.getBoolVal())) {
			return;
		}
		
		int val = 0;
		if ((cl != null) && (cl.getBoolVal())) {
			val++;
		}
		if ((ld != null) && (ld.getBoolVal())) {
			val++;
		}
		if ((inc != null) && (inc.getBoolVal())) {
			val++;
		}
		if ((dec != null) && (dec.getBoolVal())) {
			val++;
		}
		if ((shr != null) && (shr.getBoolVal())) {
			val++;
		}
		if ((shl != null) && (shl.getBoolVal())) {
			val++;
		}
		if (val > 1) {
			Log.log(":Error!!! Multiple register action...");
			return;
		}
		if ((cl != null) && (cl.getBoolVal())) {
			for (int i = 0; i < out.length; i++) {
				value = 0;
			}
			return;
		}
		if ((ld != null) && (ld.getBoolVal())) {
			value = getInput() & createMask();
			return;
		}
		if ((shl != null) && (shl.getBoolVal())) {
			value <<= 1;
			if ((il != null) && (il.getBoolVal())) {
				value |= 1;
			}
			value = value & createMask();
			return;
		}

		if ((shr != null) && (shr.getBoolVal())) {
			value >>= 1;
			if ((ir != null) && (ir.getBoolVal())) {
				value |= (int) Math.pow(2.0D, in[0].getNumOfLines() - 1);
			} else {
				value &= ((int) Math.pow(2.0D, in[0].getNumOfLines() - 1) ^ 0xFFFFFFFF);
			}
			value = value & createMask();
			return;
		}

		if ((inc != null) && (inc.getBoolVal())) {
			if (out[0].isBool()) {
				value += 4;
				if (value % (int) Math.pow(2.0D, in.length) == 0) {
					value = 0;
					c = true;
				} else {
					c = false;
				}
			} else {
				value += 4;
				if (value % (int) Math.pow(2.0D, in[0].getNumOfLines()) == 0) {
					value = 0;
					c = true;
				} else {
					c = false;
				}
			}
			value = value & createMask();
			return;
		}
		if ((dec != null) && (dec.getBoolVal())) {
			if (value > 0) {
				value -= 4;
				c = false;
			} else {
				value = (int) Math.pow(2.0D, in.length);
				c = true;
			}
			value = value & createMask();
			return;
		}
	}

	

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
