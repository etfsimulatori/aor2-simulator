package logic.components;

import java.io.*;
import java.util.*;

import logic.Execution;
import logic.LogicalComponent;
import logic.Pin;
import util.Log;

public class MEM extends LogicalComponent {
	private ArrayList<String> memory;
	private Pin read;
	private Pin write;
	private int size;

	public MEM(int size) {
		super(2, 1);
		out[0].setIsInt();

		this.size = size;
		memory = new ArrayList<String>(size);
		for (int i = 0; i < size; i++) {
			memory.add("0");
		}
		Execution.addSequentialComponent(this);

	}

	public void initFromFile(String fileName) {
		BufferedReader reader = null;
		try {
			int mask = size - 1;
			reader = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = reader.readLine()) != null) {
				try {

					line = removeComments(line);
					String[] linData = line.split(",");
					if (linData.length == 2) {
						String adr = linData[0].trim();
						String data = linData[1].trim();
						int intAdr = Integer.parseInt(adr, 16);
						intAdr = intAdr & mask;
						memory.set(intAdr, data);
					}
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}
		} catch (Exception e) {
			Log.log(e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				Log.log(e);
			}
		}
	}

	public String removeComments(String line) {
		if (line != null) {
			int index = line.indexOf("//");
			if (index > 0) {
				line = line.substring(0, index);
				line = line.trim();
			}
			line = line.toLowerCase().replace("\"", " ").trim();
		}
		return line;
	}

	public void func() {
		if (write.getBoolVal()) {
			memory.set(in[0].getIntVal(),
					Integer.toString(in[1].getIntVal(), 16));
		}

		if (read.getBoolVal()) {
			out[0].setIntVal(
					Integer.parseInt(memory.get(in[0].getIntVal()), 16));
		}
		if (!read.getBoolVal() && !write.getBoolVal()) {
			out[0].setIntVal(0);
		}
	}

	public int getSize() {
		return size;
	}

	public void setRead(Pin read) {
		this.read = read;
		read.addChild(this);
	}

	public void setWrite(Pin write) {
		this.write = write;
		write.addChild(this);
	}

	public void write(int adress, int data) {
		memory.set(adress, Integer.toString(data, 16));
	}

	public int read(int adress) {
		return Integer.parseInt(memory.get(adress), 16);
	}

	public void init(boolean fullInit) {
		if (fullInit) {
			for (int i = 0; i < size; i++) {
				memory.set(i, "0");
			}
		}
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
