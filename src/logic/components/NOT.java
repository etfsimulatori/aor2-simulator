package logic.components;

import logic.LogicalComponent;

public class NOT extends LogicalComponent {

	public NOT() {
		super(1, 1);
	}

	public void func() {
		// makes trouble...
		// need testing...
		if (in[0].isHighZ()) {
			out[0].setHighZ(true);
			return;
		}
		out[0].setBoolVal(!in[0].getBoolVal());
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
