package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;
import logic.Pin;
import logic.components.AND;
import logic.components.BoolsToInt;
import logic.components.CLK;
import logic.components.CMP;
import logic.components.NOT;
import logic.components.OR;
import logic.components.REG;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class MemUprav extends AbstractSchema {
	private OR OR1;
	private NOT NOT1;
	private AND AND1, AND2;
	private REG MEMACC;
	private REG TIME;
	private CMP CMP1;
	private BoolsToInt zeroes;
	
	public MemUprav() {
		componentName = "Mem2";
		displayName = "Upravljacka jedinica memorije";
		NameConnector.addSchema(componentName, this);
	}
	
	public void initComponent() {
		OR1 = new OR();
		NOT1 = new NOT();
		AND1 = new AND();
		AND2 = new AND();
		
		MEMACC = new REG(8, "MEMACC");
		MEMACC.getOutPin(0).setIsInt();
		MEMACC.getOutPin(0).setNumOfLines(8);
		MEMACC.initVal(0);

		TIME = new REG(8, "TIME");
		TIME.getOutPin(0).setIsInt();
		TIME.getOutPin(0).setNumOfLines(8);

		CMP1 = new CMP(1);
		
		zeroes = new BoolsToInt(8, 8);
		
		putPins();
		putComponents();
	}
	
	public void initConections() {
		OR1.setInPin(0, NameConnector.getPin("Mem1.RDF"));
		OR1.setInPin(1, NameConnector.getPin("Mem1.WRF"));
		NOT1.setInPin(0, CMP1.getEQL());
		AND1.setInPin(0, OR1.getOutPin(0));
		AND1.setInPin(1, NOT1.getOutPin(0));
		
		AND2.setInPin(0, OR1.getOutPin(0));
		AND2.setInPin(1, CMP1.getEQL());
		
		MEMACC.setInPin(0, new Pin(0, 8, ""));
		MEMACC.setPinLd(new Pin(false, "0"));
		MEMACC.setPinCL(AND2.getOutPin(0));
		MEMACC.setPinInc(AND1.getOutPin(0));
		MEMACC.setClk((CLK) NameConnector.getComponent("CLK"));
		
		TIME.initVal(Parameters.memdelay);
		TIME.setPinLd(new Pin(false, "0"));
		TIME.setClk((CLK) NameConnector.getComponent("CLK"));
		
		CMP1.setInPin(0, MEMACC.getOutPin(0));
		CMP1.setInPin(1, TIME.getOutPin(0));
		
		for (int i = 0; i < 8; i++) {
			zeroes.setInPin(i, new Pin(false, "0"));
		}
	}
	
	public void initGui() {
		gui = new GuiSchema("src/images/Mem2.png");
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(69, 169));
		points.add(new Point(80, 169));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem1.RDF"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(68, 180));
		points.add(new Point(81, 180));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem1.WRF"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(576, 309));
		points.add(new Point(600, 309));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(587, 310));
		points.add(new Point(587, 362));
		points.add(new Point(131, 362));
		points.add(new Point(131, 186));
		points.add(new Point(141, 186));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(131, 261));
		points.add(new Point(145, 261));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem2.fcMEM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 174));
		points.add(new Point(145, 174));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(117, 175));
		points.add(new Point(117, 249));
		points.add(new Point(145, 249));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(176, 180));
		points.add(new Point(264, 180));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(176, 255));
		points.add(new Point(255, 255));
		points.add(new Point(255, 199));
		points.add(new Point(264, 199));
		sections.add(points);
		line = new GuiPinLine(sections, AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 210));
		points.add(new Point(361, 328));
		points.add(new Point(500, 328));
		sections.add(points);
		line = new GuiPinLine(sections, MEMACC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(594, 212));
		points.add(new Point(594, 249));
		points.add(new Point(457, 249));
		points.add(new Point(457, 290));
		points.add(new Point(500, 290));
		sections.add(points);
		line = new GuiPinLine(sections, TIME.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(459, 180));
		points.add(new Point(468, 180));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(692, 182));
		points.add(new Point(701, 182));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 152));
		points.add(new Point(361, 169));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(594, 153));
		points.add(new Point(594, 172));
		sections.add(points);
		line = new GuiPinLine(sections, zeroes.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(459, 199));
		points.add(new Point(468, 199));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(692, 201));
		points.add(new Point(701, 201));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);


		gui.addLabel(new GuiPinLabel(463, 322, MEMACC.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(465, 282, TIME.getOutPin(0)));
		
		gui.addLabel(new GuiTextLabel(592, 150, "" + Parameters.memdelay, 12));
	}

	private void putComponents() {
		
	}

	private void putPins() {
		NameConnector.addPin(componentName, "fcMEM", CMP1.getOutPin(1));
	}
}
