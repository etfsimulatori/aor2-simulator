package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;

public class Fetch1 extends AbstractSchema {

	private REG REGIR;
	private IntToBools IRbits;
	private BoolsToInt IRIMM;
	private BoolsToInt IRBRNCH;
	private BoolsToInt IRJMP;
	private BoolsToInt IRINT;
	private BoolsToInt IRSH;
	private TSB TSBIMM18out1, TSBIMM18out2, TSBBRNCHout2, TSBJMPout2, TSBINTout3, TSBSHout1;
	private Pin logicalZero = new Pin(false, "0");
	
	public Fetch1(){
		componentName = "Fetch1";
		displayName = "Fetch1";
		NameConnector.addSchema(componentName, this);
	}
	
	@Override
	public void initComponent() {
		
		REGIR = new REG(1, "REGIR");
		REGIR.getOutPin(0).setIsInt();
		REGIR.getOutPin(0).setNumOfLines(32);
	
		IRbits = new IntToBools(32, 32);
		
		IRIMM = new BoolsToInt(32, 32);
		IRBRNCH = new BoolsToInt(32, 32);
		IRJMP = new BoolsToInt(32, 32);
		IRINT = new BoolsToInt(32, 32);
		IRSH = new BoolsToInt(32, 32);
		
		TSBIMM18out1 = new TSB("TSBIMM181");
		TSBIMM18out1.getOutPin(0).setNumOfLines(32);
		
		TSBIMM18out2 = new TSB("TSBIMM182");
		TSBIMM18out2.getOutPin(0).setNumOfLines(32);
		
		TSBBRNCHout2 = new TSB("TSBBRNCH");
		TSBBRNCHout2.getOutPin(0).setNumOfLines(32);
		
		TSBJMPout2 = new TSB("TSBJMP");
		TSBJMPout2.getOutPin(0).setNumOfLines(32);
		
		TSBINTout3 = new TSB("TSBINT");
		TSBINTout3.getOutPin(0).setNumOfLines(32);
		
		TSBSHout1 = new TSB("TSBSH");
		TSBSHout1.getOutPin(0).setNumOfLines(32);
		
		putPins();
		putComponents();
		
	}

	@Override
	public void initConections() {
		
		REGIR.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));
		REGIR.setPinLd(NameConnector.getPin("Oper1.ldIR"));
		REGIR.setClk((CLK) NameConnector.getComponent("CLK"));
		
		REGIR.setVal(0x20);
		
		IRbits.setInPin(0, REGIR.getOutPin(0));
		/*
		for(int i = 0; i < 32; i++) {
			System.out.println("" + i + ": = " + IRbits.getOutPin(i).getBoolVal());
		}
		*/
		for (int i = 0; i < 17; i++) {
			IRIMM.setInPin(i, IRbits.getOutPin(i));
			//System.out.println("i = " + i + ", " + IRbits.getOutPin(i));	
		}
		
		//System.out.println("\n\nIRIMM = " + IRIMM.getOutPin(0).getIntVal() + "\n\n");
		
		for (int i = 17, j = 17; i < 32; i++) {
			IRIMM.setInPin(i, IRbits.getOutPin(j));
		}
		
		for (int i = 0; i < 20; i++) {
			IRBRNCH.setInPin(i, IRbits.getOutPin(i));
		}
		for (int i = 20, j = 20; i < 32; i++) {
			IRBRNCH.setInPin(i, IRbits.getOutPin(j));
		}
		
		for (int i = 0; i < 23; i++) {
			IRJMP.setInPin(i, IRbits.getOutPin(i));
		}
		for (int i = 23, j = 23; i < 32; i++) {
			IRJMP.setInPin(i, IRbits.getOutPin(j));
		}
		
		IRINT.setInPin(0, logicalZero);
		IRINT.setInPin(1, logicalZero);
		for (int i = 2; i < 6; i++) {
			IRINT.setInPin(i, IRbits.getOutPin(i + 20));
		}
		for (int i = 6; i < 32; i++) {
			IRINT.setInPin(i, logicalZero);
		}
		
		for (int i = 0; i < 5; i++) {
			IRSH.setInPin(i, IRbits.getOutPin(i + 13));
		}
		for (int i = 5; i < 32; i++) {
			IRSH.setInPin(i, logicalZero);
		}
		
		// proveriti imena pinova
		// moguce da mOper1 nece da usaglasi neka imena, proveriti
		TSBIMM18out1.setInPin(0, IRIMM.getOutPin(0));
		TSBIMM18out1.setE(NameConnector.getPin("Oper1.IRIMM18out1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBIMM18out1.getOutPin(0));
		
		TSBIMM18out2.setInPin(0, IRIMM.getOutPin(0));
		TSBIMM18out2.setE(NameConnector.getPin("Oper1.IRIMM18out2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBIMM18out2.getOutPin(0));
		
		TSBBRNCHout2.setInPin(0, IRBRNCH.getOutPin(0));
		TSBBRNCHout2.setE(NameConnector.getPin("Oper1.IRBRNCHout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBBRNCHout2.getOutPin(0));
		
		TSBJMPout2.setInPin(0, IRJMP.getOutPin(0));
		TSBJMPout2.setE(NameConnector.getPin("Oper1.IRJMPout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBJMPout2.getOutPin(0));
		
		TSBINTout3.setInPin(0, IRINT.getOutPin(0));
		TSBINTout3.setE(NameConnector.getPin("Oper1.IRINTout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBINTout3.getOutPin(0));
		
		TSBSHout1.setInPin(0, IRSH.getOutPin(0));
		TSBSHout1.setE(NameConnector.getPin("Oper1.IRSHout1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBSHout1.getOutPin(0));
		
	}

	@Override
	public void initGui() {
		gui = new GuiSchema("src/images/Fetch1.png");
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(308, 10));
		points.add(new Point(308, 627));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(308, 99));
		points.add(new Point(118, 99));
		points.add(new Point(118, 136));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(289, 31));
		points.add(new Point(289, 606));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 54));
		points.add(new Point(270, 584));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(119, 176));
		points.add(new Point(119, 193));
		sections.add(points);
		line = new GuiPinLine(sections, REGIR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 231));
		points.add(new Point(171, 231));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(128, 232));
		points.add(new Point(128, 288));
		points.add(new Point(171, 288));
		sections.add(points);
		line = new GuiPinLine(sections, IRIMM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 345));
		points.add(new Point(171, 345));
		sections.add(points);
		line = new GuiPinLine(sections, IRBRNCH.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 401));
		points.add(new Point(171, 401));
		sections.add(points);
		line = new GuiPinLine(sections, IRJMP.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 458));
		points.add(new Point(171, 458));
		sections.add(points);
		line = new GuiPinLine(sections, IRJMP.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 515));
		points.add(new Point(171, 515));
		sections.add(points);
		line = new GuiPinLine(sections, IRSH.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 231));
		points.add(new Point(307, 231));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIMM18out1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 288));
		points.add(new Point(288, 288));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIMM18out2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 345));
		points.add(new Point(288, 345));
		sections.add(points);
		line = new GuiPinLine(sections, TSBBRNCHout2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 401));
		points.add(new Point(288, 401));
		sections.add(points);
		line = new GuiPinLine(sections, TSBJMPout2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 458));
		points.add(new Point(269, 458));
		sections.add(points);
		line = new GuiPinLine(sections, TSBINTout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 515));
		points.add(new Point(307, 515));
		sections.add(points);
		line = new GuiPinLine(sections, TSBSHout1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(183, 237));
		points.add(new Point(183, 243));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IRIMM18out1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(183, 293));
		points.add(new Point(183, 300));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IRIMM18out2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(183, 350));
		points.add(new Point(183, 357));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IRBRNCHout2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(183, 406));
		points.add(new Point(183, 413));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IRJMPout2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(183, 464));
		points.add(new Point(183, 470));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IRINTout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(183, 521));
		points.add(new Point(183, 527));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IRSHout1"));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(35, 165));
		points.add(new Point(42, 165));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(35, 146));
		points.add(new Point(42, 146));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);
		
		gui.addLabel(new GuiPinLabel(93, 208, REGIR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(83, 228, IRIMM.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(83, 340, IRBRNCH.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(83, 397, IRJMP.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(85, 454, IRINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(85, 512, IRSH.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(315, 30, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(294, 50, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(275, 70, NameConnector.getPin("Bus1.IBUS3")));

	}
	
	public void putPins() {
		
		NameConnector.addPin(componentName, "REGIR", REGIR.getOutPin(0));
		
		for(int i = 0; i<32; i++){
			IRbits.getOutPin(i).setName("IR" + i);
			NameConnector.addPin(componentName, "IR" + i,
					IRbits.getOutPin(i));
		}

		NameConnector.addPin(componentName, "IRIMM18out1", TSBIMM18out1.getOutPin(0));
		
		NameConnector.addPin(componentName, "IRIMM18out2", TSBIMM18out2.getOutPin(0));
		
		NameConnector.addPin(componentName, "IRBRNCHout2", TSBBRNCHout2.getOutPin(0));
		
		NameConnector.addPin(componentName, "IRJMPout2", TSBJMPout2.getOutPin(0));
		
		NameConnector.addPin(componentName, "IRINTout3", TSBINTout3.getOutPin(0));
		
		NameConnector.addPin(componentName, "IRSHout1", TSBSHout1.getOutPin(0));

	}
	
	public void putComponents() {
		NameConnector.addComponent(componentName, "REGIR", REGIR);
	}
	
	public REG RegIR() {
		return REGIR;
	}

}
