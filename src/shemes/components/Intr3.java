package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.BoolsToInt;
import logic.components.CD;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.NameConnector;

public class Intr3 extends AbstractSchema {

	private CD CD1, CD2;
	private BoolsToInt UINT, UEXT;
	private TSB TSBUINTout3, TSBUEXTout3;

	public Intr3() {
		componentName = "Intr3";
		displayName = "Intr 3";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent(){
		
		CD1 = new CD(4);
		CD2 = new CD(8);

		UINT = new BoolsToInt(32, 32);
		UEXT = new BoolsToInt(32, 32);

		TSBUINTout3 = new TSB("UINTout3");
		TSBUINTout3.getOutPin(0).setNumOfLines(32);
		TSBUEXTout3 = new TSB("EXTout3");
		TSBUEXTout3.getOutPin(0).setNumOfLines(32);

		putPins();
		putComponents();
	}

	public void initConections() {
		CD1.setE(new Pin(true, "1"));
		CD1.setInPin(0, NameConnector.getPin("Exec2.PSWT"));
		CD1.setInPin(1, NameConnector.getPin("Intr1.PRINM"));
		CD1.setInPin(2, NameConnector.getPin("Intr1.PRADR"));
		CD1.setInPin(3, NameConnector.getPin("Intr1.PRCOD"));

		UINT.setInPin(0, new Pin(false, "0"));
		UINT.setInPin(1, new Pin(false, "0"));
		UINT.setInPin(2, CD1.getOutPin(0));
		UINT.setInPin(3, CD1.getOutPin(1));
		for (int i = 4; i < 32; i++) {
			UINT.setInPin(i, new Pin(false, "0"));
		}

		TSBUINTout3.setInPin(0, UINT.getOutPin(0));
		TSBUINTout3.setE(NameConnector.getPin("Oper1.UINTout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBUINTout3.getOutPin(0));

		CD2.setE(new Pin(true, "1"));
		CD2.setInPin(0, new Pin(false, "0"));
		CD2.setInPin(1, NameConnector.getPin("Intr1.PRINTR1"));
		CD2.setInPin(2, NameConnector.getPin("Intr1.PRINTR2"));
		CD2.setInPin(3, NameConnector.getPin("Intr1.PRINTR3"));
		CD2.setInPin(4, NameConnector.getPin("Intr1.PRINTR4"));
		CD2.setInPin(5, NameConnector.getPin("Intr1.PRINTR5"));
		CD2.setInPin(6, NameConnector.getPin("Intr1.PRINTR6"));
		CD2.setInPin(7, NameConnector.getPin("Intr1.PRINTR7"));

		UEXT.setInPin(0, new Pin(false, "0"));
		UEXT.setInPin(1, new Pin(false, "0"));
		UEXT.setInPin(2, CD2.getOutPin(0));
		UEXT.setInPin(3, CD2.getOutPin(1));
		UEXT.setInPin(4, CD2.getOutPin(2));
		UEXT.setInPin(5, new Pin(true, "1"));
		for (int i = 6; i < 32; i++) {
			UEXT.setInPin(i, new Pin(false, "0"));
		}

		TSBUEXTout3.setInPin(0, UEXT.getOutPin(0));
		TSBUEXTout3.setE(NameConnector.getPin("Oper1.UEXTout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBUEXTout3.getOutPin(0));
	}

	public void initGui() {
		gui = new GuiSchema("src/images/Intr3.png");
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(429, 16));
		points.add(new Point(429, 696));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(410, 35));
		points.add(new Point(410, 677));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(391, 54));
		points.add(new Point(391, 658));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(168, 120));
		points.add(new Point(266, 120));
		sections.add(points);
		line = new GuiPinLine(sections, UINT.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(288, 120));
		points.add(new Point(390, 120));
		sections.add(points);
		line = new GuiPinLine(sections, TSBUINTout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(165, 335));
		points.add(new Point(264, 335));
		sections.add(points);
		line = new GuiPinLine(sections, UEXT.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(288, 335));
		points.add(new Point(390, 335));
		sections.add(points);
		line = new GuiPinLine(sections, TSBUEXTout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(117, 142));
		points.add(new Point(139, 142));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(117, 127));
		points.add(new Point(139, 127));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(129, 169));
		points.add(new Point(138, 169));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(129, 156));
		points.add(new Point(138, 156));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(129, 112));
		points.add(new Point(138, 112));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(129, 99));
		points.add(new Point(138, 99));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(129, 74));
		points.add(new Point(138, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(126, 385));
		points.add(new Point(135, 385));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(126, 371));
		points.add(new Point(135, 371));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(126, 301));
		points.add(new Point(135, 301));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(126, 280));
		points.add(new Point(135, 280));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(56, 395));
		points.add(new Point(66, 395));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(126, 314));
		points.add(new Point(135, 314));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(276, 126));
		points.add(new Point(276, 132));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.UINTout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(276, 341));
		points.add(new Point(276, 347));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.UEXTout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(113, 357));
		points.add(new Point(135, 357));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(113, 342));
		points.add(new Point(135, 342));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(113, 327));
		points.add(new Point(135, 327));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 380));
		points.add(new Point(66, 380));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 365));
		points.add(new Point(66, 365));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 350));
		points.add(new Point(66, 350));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 335));
		points.add(new Point(66, 335));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR4"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 320));
		points.add(new Point(66, 320));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR5"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 305));
		points.add(new Point(66, 305));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR6"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 290));
		points.add(new Point(66, 290));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR7"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(59, 159));
		points.add(new Point(69, 159));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(59, 144));
		points.add(new Point(69, 144));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(59, 129));
		points.add(new Point(69, 129));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRADR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(59, 114));
		points.add(new Point(69, 114));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRCOD"));
		gui.addLine(line);


		gui.addLabel(new GuiPinLabel(164, 145, UINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(160, 359, UEXT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(435, 31,  NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(417, 51,  NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(398, 71,  NameConnector.getPin("Bus1.IBUS3")));
	}

	private void putPins() {
		NameConnector.addPin(componentName, "UINT", UINT.getOutPin(0));

		NameConnector.addPin(componentName, "UEXT", UEXT.getOutPin(0));
	}

	private void putComponents() {

	}
}
