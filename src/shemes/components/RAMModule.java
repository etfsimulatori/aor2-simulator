package shemes.components;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.AND;
import logic.components.BoolsToInt;
import logic.components.INC;
import logic.components.IntToBools;
import logic.components.IntToInt;
import logic.components.MEM;
import logic.components.MP;
import logic.components.OR;
import shemes.AbstractSchema;
import util.Log;
import util.NameConnector;

public class RAMModule extends AbstractSchema {
	public static final int MEM_SIZE = 0x100000;//1MB 
	private static final int NUM_OF_MODULES = 4;

	private ArrayList<MEM> mems;
	private MP MX0, MX1, MX2, MXIN, MXOUT;
	private INC INC1;
	private OR OR1;
	private AND AND1;

	private IntToBools DINbits;
	private BoolsToInt[] inputRotations;
	private IntToInt[] inputParts;
	private IntToBools[] outputParts;
	private BoolsToInt[] outputRotations;

	private IntToInt highAdr;
	private IntToBools adrSelectors;

	public RAMModule() {
		componentName = "MemModule";
		displayName = "RAM Modul";
		NameConnector.addSchema(componentName, this);
	}

	@Override
	public void initComponent() {
		mems = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			mems.add(new MEM(MEM_SIZE));
			mems.get(i).getOutPin(0).setIsInt();
			mems.get(i).getOutPin(0).setNumOfLines(8);
		}

		MX0 = new MP(2);
		MX0.getOutPin(0).setIsInt();
		MX0.getOutPin(0).setNumOfLines(30);

		MX1 = new MP(2);
		MX1.getOutPin(0).setIsInt();
		MX1.getOutPin(0).setNumOfLines(30);

		MX2 = new MP(2);
		MX2.getOutPin(0).setIsInt();
		MX2.getOutPin(0).setNumOfLines(30);

		MXIN = new MP(4);
		MXIN.getOutPin(0).setIsInt();
		MXIN.getOutPin(0).setNumOfLines(32);

		MXOUT = new MP(4);
		MXOUT.getOutPin(0).setIsInt();
		MXOUT.getOutPin(0).setNumOfLines(32);

		highAdr = new IntToInt(32, 30, true, false, 0);
		adrSelectors = new IntToBools(32, 2);

		DINbits = new IntToBools(32, 32);

		inputRotations = new BoolsToInt[4];
		for (int i = 0; i < 4; i++) {
			inputRotations[i] = new BoolsToInt(32, 32);
		}

		inputParts = new IntToInt[4];
		for (int i = 0; i < 4; i++) {
			inputParts[i] = new IntToInt(32, 8, false, false, i * 8);
		}

		outputParts = new IntToBools[4];
		for (int i = 0; i < 4; i++) {
			outputParts[i] = new IntToBools(8, 8);
		}

		outputRotations = new BoolsToInt[4];
		for (int i = 0; i < 4; i++) {
			outputRotations[i] = new BoolsToInt(32, 32);
		}

		INC1 = new INC(30);

		OR1 = new OR();
		AND1 = new AND();
		
		putPins();
		putComponents();
	}

	

	@Override
	public void initConections() {
		// Address
		highAdr.setInPin(0, NameConnector.getPin("Bus1.ABUS"));
		INC1.setInPin(0, highAdr.getOutPin(0));
		adrSelectors.setInPin(0, NameConnector.getPin("Bus1.ABUS"));

		OR1.setInPin(0, adrSelectors.getOutPin(0));
		OR1.setInPin(1, adrSelectors.getOutPin(1));

		MX0.setInPin(0, highAdr.getOutPin(0));
		MX0.setInPin(1, INC1.getOutPin(0));
		MX0.setCtrl(0, OR1.getOutPin(0));

		MX1.setInPin(0, highAdr.getOutPin(0));
		MX1.setInPin(1, INC1.getOutPin(0));
		MX1.setCtrl(0, adrSelectors.getOutPin(1));

		AND1.setInPin(0, adrSelectors.getOutPin(0));
		AND1.setInPin(1, adrSelectors.getOutPin(1));

		MX2.setInPin(0, highAdr.getOutPin(0));
		MX2.setInPin(1, INC1.getOutPin(0));
		MX2.setCtrl(0, AND1.getOutPin(0));

		// Data input
		DINbits.setInPin(0, NameConnector.getPin("Bus1.DBUS"));
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 32; j++) {
				inputRotations[i].setInPin(j, DINbits.getOutPin((j + i * 8) % 32));
			}
			MXIN.setInPin(i, inputRotations[i].getOutPin(0));
		}

		MXIN.setCtrl(0, adrSelectors.getOutPin(0));
		MXIN.setCtrl(1, adrSelectors.getOutPin(1));

		for (int i = 0; i < 4; i++) {
			inputParts[i].setInPin(0, MXIN.getOutPin(0));
		}
		// Modules ain, din, rd and wr pins
		mems.get(0).setInPin(0, MX0.getOutPin(0));
		mems.get(1).setInPin(0, MX1.getOutPin(0));
		mems.get(2).setInPin(0, MX2.getOutPin(0));
		mems.get(3).setInPin(0, highAdr.getOutPin(0));
		for (int i = 0; i < 4; i++) {
			mems.get(i).setInPin(1, inputParts[i].getOutPin(0));
			mems.get(i).setRead(NameConnector.getPin("Mem1.RDF"));
			mems.get(i).setWrite(NameConnector.getPin("Mem1.WRF"));
		}
		
		
		//Data output
		for(int i = 0; i<4; i++){
			outputParts[i].setInPin(0, mems.get(i).getOutPin(0));
		}
		
		for(int i = 0; i<4; i++){
			for(int j = 0; j<32; j++){
				outputRotations[i].setInPin(j, outputParts[(j/8 + i)%4].getOutPin(j%8));
			}
			MXOUT.setInPin(i, outputRotations[i].getOutPin(0));
		}
		
		MXOUT.setCtrl(0, adrSelectors.getOutPin(0));
		MXOUT.setCtrl(1, adrSelectors.getOutPin(1));
	}
	
	@Override
	public void initGui() {
		gui = new GuiSchema("src/images/RamModule.png");
		
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(553, 49));
		points.add(new Point(553, 23));
		points.add(new Point(661, 23));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(274, 189));
		points.add(new Point(274, 203));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(431, 189));
		points.add(new Point(431, 202));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(579, 189));
		points.add(new Point(579, 202));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(116, 247));
		points.add(new Point(116, 343));
		points.add(new Point(140, 343));
		sections.add(points);
		line = new GuiPinLine(sections,  highAdr.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(553, 109));
		points.add(new Point(553, 87));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(236, 189));
		points.add(new Point(236, 203));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(393, 203));
		points.add(new Point(393, 188));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(541, 202));
		points.add(new Point(541, 188));
		sections.add(points);
		line = new GuiPinLine(sections,  INC1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(598, 222));
		points.add(new Point(605, 222));
		sections.add(points);
		line = new GuiPinLine(sections,  OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(636, 227));
		points.add(new Point(626, 227));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(331, 227));
		points.add(new Point(325, 227));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(395, 723));
		points.add(new Point(395, 715));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(254, 98));
		points.add(new Point(254, 107));
		sections.add(points);
		line = new GuiPinLine(sections,  adrSelectors.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(627, 216));
		points.add(new Point(636, 216));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(450, 222));
		points.add(new Point(457, 222));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(323, 216));
		points.add(new Point(331, 216));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(235, 99));
		points.add(new Point(235, 107));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(376, 724));
		points.add(new Point(376, 715));
		sections.add(points);
		line = new GuiPinLine(sections,  adrSelectors.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(294, 222));
		points.add(new Point(300, 222));
		sections.add(points);
		line = new GuiPinLine(sections,  AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 78));
		points.add(new Point(216, 78));
		sections.add(points);
		line = new GuiPinLine(sections,  inputRotations[0].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 59));
		points.add(new Point(216, 59));
		sections.add(points);
		line = new GuiPinLine(sections,  inputRotations[1].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 40));
		points.add(new Point(216, 40));
		sections.add(points);
		line = new GuiPinLine(sections,  inputRotations[2].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(201, 22));
		points.add(new Point(216, 22));
		sections.add(points);
		line = new GuiPinLine(sections,  inputRotations[3].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(178, 301));
		points.add(new Point(178, 278));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(314, 19));
		points.add(new Point(330, 19));
		sections.add(points);
		line = new GuiPinLine(sections,  inputParts[3].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(326, 301));
		points.add(new Point(326, 279));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(314, 38));
		points.add(new Point(330, 38));
		sections.add(points);
		line = new GuiPinLine(sections,  inputParts[2].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(474, 301));
		points.add(new Point(474, 278));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(314, 57));
		points.add(new Point(330, 57));
		sections.add(points);
		line = new GuiPinLine(sections,  inputParts[1].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(622, 301));
		points.add(new Point(622, 276));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(314, 76));
		points.add(new Point(330, 76));
		sections.add(points);
		line = new GuiPinLine(sections,  inputParts[0].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(560, 242));
		points.add(new Point(560, 343));
		points.add(new Point(584, 343));
		sections.add(points);
		line = new GuiPinLine(sections,  MX0.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(412, 241));
		points.add(new Point(412, 343));
		points.add(new Point(436, 343));
		sections.add(points);
		line = new GuiPinLine(sections,  MX1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(255, 242));
		points.add(new Point(255, 343));
		points.add(new Point(288, 343));
		sections.add(points);
		line = new GuiPinLine(sections,  MX2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(38, 572));
		points.add(new Point(751, 572));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(230, 573));
		points.add(new Point(230, 342));
		points.add(new Point(217, 342));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(378, 573));
		points.add(new Point(378, 342));
		points.add(new Point(365, 342));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(526, 572));
		points.add(new Point(526, 342));
		points.add(new Point(513, 342));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(674, 573));
		points.add(new Point(674, 342));
		points.add(new Point(661, 342));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Mem1.RDF"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(37, 535));
		points.add(new Point(751, 535));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(235, 535));
		points.add(new Point(235, 389));
		points.add(new Point(217, 389));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(383, 535));
		points.add(new Point(383, 389));
		points.add(new Point(365, 389));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(531, 535));
		points.add(new Point(531, 389));
		points.add(new Point(513, 389));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(679, 535));
		points.add(new Point(679, 389));
		points.add(new Point(661, 389));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Mem1.WRF"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(38, 497));
		points.add(new Point(752, 497));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(117, 497));
		points.add(new Point(117, 390));
		points.add(new Point(140, 390));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(265, 497));
		points.add(new Point(265, 390));
		points.add(new Point(287, 390));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(413, 497));
		points.add(new Point(413, 390));
		points.add(new Point(435, 390));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(561, 498));
		points.add(new Point(561, 390));
		points.add(new Point(583, 390));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(622, 447));
		points.add(new Point(622, 470));
		sections.add(points);
		line = new GuiPinLine(sections,  mems.get(0).getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(473, 447));
		points.add(new Point(473, 470));
		sections.add(points);
		line = new GuiPinLine(sections,  mems.get(1).getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(326, 448));
		points.add(new Point(326, 471));
		sections.add(points);
		line = new GuiPinLine(sections,  mems.get(2).getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(178, 471));
		points.add(new Point(178, 447));
		sections.add(points);
		line = new GuiPinLine(sections,  mems.get(3).getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 695));
		points.add(new Point(358, 695));
		sections.add(points);
		line = new GuiPinLine(sections,  outputRotations[0].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 676));
		points.add(new Point(357, 676));
		sections.add(points);
		line = new GuiPinLine(sections,  outputRotations[1].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 658));
		points.add(new Point(357, 658));
		sections.add(points);
		line = new GuiPinLine(sections,  outputRotations[2].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 639));
		points.add(new Point(357, 639));
		sections.add(points);
		line = new GuiPinLine(sections,  outputRotations[3].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(415, 667));
		points.add(new Point(460, 667));
		sections.add(points);
		line = new GuiPinLine(sections,  MXOUT.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(273, 48));
		points.add(new Point(300, 48));
		sections.add(points);
		line = new GuiPinLine(sections, MXIN.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(661, 139));
		points.add(new Point(661, 10));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.ABUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(730, 139));
		points.add(new Point(730, 11));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.DBUS"));
		gui.addLine(line);


		gui.addLabel(new GuiPinLabel(433, 683,  MXOUT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(631, 462,  mems.get(0).getOutPin(0)));
		gui.addLabel(new GuiPinLabel(484, 462,  mems.get(1).getOutPin(0)));
		gui.addLabel(new GuiPinLabel(337, 462,  mems.get(2).getOutPin(0)));
		gui.addLabel(new GuiPinLabel(187, 462,  mems.get(3).getOutPin(0)));
		gui.addLabel(new GuiPinLabel(191, 293,  inputParts[3].getOutPin(0)));
		gui.addLabel(new GuiPinLabel(340, 293,  inputParts[2].getOutPin(0)));
		gui.addLabel(new GuiPinLabel(488, 293,  inputParts[1].getOutPin(0)));
		gui.addLabel(new GuiPinLabel(639, 293,  inputParts[0].getOutPin(0)));
		gui.addLabel(new GuiPinLabel(736, 81, NameConnector.getPin("Bus1.DBUS")));
		gui.addLabel(new GuiPinLabel(667, 81, NameConnector.getPin("Bus1.ABUS")));
		gui.addLabel(new GuiPinLabel(558, 19,  highAdr.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(558, 116,  INC1.getOutPin(0)));



	}
	
	private void putComponents() {
		// TODO Auto-generated method stub
		
	}

	private void putPins() {
		NameConnector.addPin(componentName, "DOUT", MXOUT.getOutPin(0));
		
	}
	
	
	public void initFromFile(String fileName) {
		BufferedReader reader = null;
		try {
			int mask = NUM_OF_MODULES*MEM_SIZE - 1;
			reader = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = reader.readLine()) != null) {
				try {

					line = removeComments(line);
					String[] linData = line.split(",");
					if (linData.length == 2) {
						String adr = linData[0].trim();
						String data = linData[1].trim();
						int intAdr = Integer.parseInt(adr, 16);
						intAdr = intAdr & mask;
						int intData = Integer.parseInt(data, 16);
						intData = intData & 0xFF;
						write(intAdr, intData);
					}
				} catch (Exception e) {
					 //e.printStackTrace();
				}
			}
		} catch (Exception e) {
			Log.log(e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				Log.log(e);
			}
		}
	}

	private String removeComments(String line) {
		if (line != null) {
			int index = line.indexOf("//");
			if (index > 0) {
				line = line.substring(0, index);
				line = line.trim();
			}
			line = line.toLowerCase().replace("\"", " ").trim();
		}
		return line;
	}
	
	public void write(int adress, int data) {
		mems.get(adress%NUM_OF_MODULES).write(adress/4, data);
	}

	public int read(int adress) {
		return mems.get(adress%NUM_OF_MODULES).read(adress/4);
	}
}
