package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.CLK;
import logic.components.IntToBools;
import logic.components.KMADR;
import logic.components.KMINTR;
import logic.components.KMOPR;
import logic.components.MP;
import logic.components.MicroMemory;
import logic.components.NOT;
import logic.components.OR;
import logic.components.REG;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class Counter extends AbstractSchema {

	private KMINTR KMINTR1;
	private KMOPR KMOPR1;
	private KMADR KMADR1;
	private MP MP1;
	private REG mPC;
	private OR ORmPC;
	private OR OR1, OR2;
	private NOT NOTmPC;
	private MicroMemory mMEM;

	private IntToBools mCWbit0, mCWbits1_8, mCWbits9_16, mCWbits17_36, mCWbits37_56, mCWbits57_76;

	public Counter() {
		componentName = "Counter";
		displayName = "Upravljacka jedinica procesora";
		NameConnector.addSchema(componentName, this);
		this.addSubScheme(new Oper1());
		this.addSubScheme(new Uprav1());
	}

	@Override
	public void initComponent() {
		super.initComponent();

		int[] kmintrInterruptAddresses = new int[Parameters.counterInterruptAddresses.length];
		for (int i = 0; i < kmintrInterruptAddresses.length; i++) {
			kmintrInterruptAddresses[i] = Integer.parseInt(Parameters.counterInterruptAddresses[i][1]);
		}
		KMINTR1 = new KMINTR(kmintrInterruptAddresses);
		KMINTR1.getOutPin(0).setNumOfLines(8);

		int[] kmoprCodeAddresses = new int[Parameters.counterCodeAddresses.length];
		for (int i = 0; i < kmoprCodeAddresses.length; i++) {
			kmoprCodeAddresses[i] = Integer.parseInt(Parameters.counterCodeAddresses[i][1]);
		}
		KMOPR1 = new KMOPR(kmoprCodeAddresses);
		KMOPR1.getOutPin(0).setNumOfLines(8);

		int[] kmoprAdrModesAddresses = new int[Parameters.counterAdrModesAddresses.length];
		for (int i = 0; i < kmoprAdrModesAddresses.length; i++) {
			kmoprAdrModesAddresses[i] = Integer.parseInt(Parameters.counterAdrModesAddresses[i][1]);
		}
		KMADR1 = new KMADR(kmoprAdrModesAddresses);
		KMADR1.getOutPin(0).setNumOfLines(8);

		MP1 = new MP(4);
		MP1.getOutPin(0).setNumOfLines(8);
		MP1.getOutPin(0).setIsInt();

		mPC = new REG(1, "mPC");
		mPC.getOutPin(0).setNumOfLines(8);
		mPC.getOutPin(0).setIsInt();

		ORmPC = new OR(5);
		OR1 = new OR();
		OR2 = new OR();
		NOTmPC = new NOT();

		mMEM = new MicroMemory(256);

		mCWbit0 = new IntToBools(1, 1);
		mCWbits1_8 = new IntToBools(1, 8);
		mCWbits9_16 = new IntToBools(1, 8);
		mCWbits17_36 = new IntToBools(1, 20);
		mCWbits37_56 = new IntToBools(1, 20);
		mCWbits57_76 = new IntToBools(1, 20);

		putPins();
		putComponents();
	}

	@Override
	public void initConections() {
		super.initConections();

		for (int i = 0; i < Parameters.counterInterruptAddresses.length; i++) {
			String name = Parameters.counterInterruptAddresses[i][0];
			Pin pin = NameConnector.getPin(name);
			KMINTR1.setInPin(i, pin);
		}

		for (int i = 0; i < Parameters.counterCodeAddresses.length; i++) {
			String name = Parameters.counterCodeAddresses[i][0];
			Pin pin = NameConnector.getPin(name);
			KMOPR1.setInPin(i, pin);
		}

		for (int i = 0; i < Parameters.counterAdrModesAddresses.length; i++) {
			String name = Parameters.counterAdrModesAddresses[i][0];
			Pin pin = NameConnector.getPin(name);
			KMADR1.setInPin(i, pin);
		}

		MP1.setInPin(0, mMEM.getOutPin(2));
		MP1.setInPin(1, KMADR1.getOutPin(0));
		MP1.setInPin(2, KMOPR1.getOutPin(0));
		MP1.setInPin(3, KMINTR1.getOutPin(0));

		OR1.setInPin(0, NameConnector.getPin("Uprav1.bradr"));
		OR1.setInPin(1, NameConnector.getPin("Uprav1.brintr"));

		OR2.setInPin(0, NameConnector.getPin("Uprav1.brintr"));
		OR2.setInPin(1, NameConnector.getPin("Uprav1.bropr"));

		MP1.setCtrl(0, OR1.getOutPin(0));
		MP1.setCtrl(1, OR2.getOutPin(0));

		ORmPC.setInPin(0, NameConnector.getPin("Uprav1.brintr"));
		ORmPC.setInPin(1, NameConnector.getPin("Uprav1.bradr"));
		ORmPC.setInPin(2, NameConnector.getPin("Uprav1.bropr"));
		ORmPC.setInPin(3, NameConnector.getPin("Uprav1.branch"));
		ORmPC.setInPin(4, NameConnector.getPin("Uprav1.br"));
		NOTmPC.setInPin(0, ORmPC.getOutPin(0));

		mPC.setInPin(0, MP1.getOutPin(0));
		mPC.setPinLd(ORmPC.getOutPin(0));
		mPC.setPinInc(NOTmPC.getOutPin(0));
		mPC.setClk((CLK) NameConnector.getComponent("CLK"));

		mMEM.setInPin(0, mPC.getOutPin(0));

		mCWbit0.setInPin(0, mMEM.getOutPin(0));
		mCWbits1_8.setInPin(0, mMEM.getOutPin(1));
		mCWbits9_16.setInPin(0, mMEM.getOutPin(2));
		mCWbits17_36.setInPin(0, mMEM.getOutPin(3));
		mCWbits37_56.setInPin(0, mMEM.getOutPin(4));
		mCWbits57_76.setInPin(0, mMEM.getOutPin(5));

	}

	@Override
	public void initGui() {
		super.initGui();

		gui = new GuiSchema("./src/images/Counter.png");

		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(98, 111));
		points.add(new Point(98, 87));
		sections.add(points);
		line = new GuiPinLine(sections, KMINTR1.getInPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(117, 112));
		points.add(new Point(117, 86));
		sections.add(points);
		line = new GuiPinLine(sections, KMINTR1.getInPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(212, 112));
		points.add(new Point(212, 87));
		sections.add(points);
		line = new GuiPinLine(sections, KMINTR1.getInPin(KMINTR1.getInPins().length - 1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(292, 112));
		points.add(new Point(292, 87));
		sections.add(points);
		line = new GuiPinLine(sections, KMOPR1.getInPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(311, 111));
		points.add(new Point(311, 88));
		sections.add(points);
		line = new GuiPinLine(sections, KMOPR1.getInPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(405, 111));
		points.add(new Point(405, 87));
		sections.add(points);
		line = new GuiPinLine(sections, KMOPR1.getInPin(KMOPR1.getInPins().length - 1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(484, 111));
		points.add(new Point(484, 87));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getInPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(520, 112));
		points.add(new Point(520, 88));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getInPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(560, 111));
		points.add(new Point(560, 87));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getInPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(598, 110));
		points.add(new Point(598, 87));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getInPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(232, 266));
		points.add(new Point(232, 247));
		points.add(new Point(480, 247));
		points.add(new Point(480, 645));
		points.add(new Point(176, 645));
		points.add(new Point(176, 626));
		sections.add(points);
		line = new GuiPinLine(sections, mMEM.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 266));
		points.add(new Point(195, 225));
		points.add(new Point(541, 225));
		points.add(new Point(541, 189));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(157, 267));
		points.add(new Point(157, 213));
		points.add(new Point(349, 213));
		points.add(new Point(349, 189));
		sections.add(points);
		line = new GuiPinLine(sections, KMOPR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(119, 265));
		points.add(new Point(119, 203));
		points.add(new Point(160, 203));
		points.add(new Point(160, 189));
		sections.add(points);
		line = new GuiPinLine(sections, KMINTR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(303, 267));
		points.add(new Point(309, 267));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(316, 368));
		points.add(new Point(324, 368));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(395, 937));
		points.add(new Point(395, 927));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bradr"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(303, 315));
		points.add(new Point(309, 315));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(312, 380));
		points.add(new Point(324, 380));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(419, 938));
		points.add(new Point(419, 926));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bropr"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(335, 291));
		points.add(new Point(328, 291));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(303, 277));
		points.add(new Point(328, 277));
		points.add(new Point(328, 305));
		points.add(new Point(303, 305));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(316, 356));
		points.add(new Point(325, 356));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(444, 938));
		points.add(new Point(444, 928));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.brintr"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(251, 283));
		points.add(new Point(270, 283));
		points.add(new Point(270, 272));
		points.add(new Point(283, 272));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(252, 302));
		points.add(new Point(270, 302));
		points.add(new Point(270, 310));
		points.add(new Point(284, 310));
		sections.add(points);
		line = new GuiPinLine(sections, OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(315, 404));
		points.add(new Point(324, 404));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(499, 938));
		points.add(new Point(499, 927));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.branch"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(315, 392));
		points.add(new Point(324, 392));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(471, 938));
		points.add(new Point(471, 927));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.br"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(293, 380));
		points.add(new Point(282, 380));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(252, 369));
		points.add(new Point(281, 369));
		points.add(new Point(281, 390));
		points.add(new Point(257, 390));
		sections.add(points);
		line = new GuiPinLine(sections, ORmPC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(176, 315));
		points.add(new Point(176, 355));
		sections.add(points);
		line = new GuiPinLine(sections, MP1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(94, 377));
		points.add(new Point(100, 377));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(381, 549));
		points.add(new Point(390, 549));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(176, 402));
		points.add(new Point(176, 445));
		sections.add(points);
		line = new GuiPinLine(sections, mPC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(92, 468));
		points.add(new Point(100, 468));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(381, 577));
		points.add(new Point(391, 577));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(54, 600));
		points.add(new Point(54, 593));
		sections.add(points);
		line = new GuiPinLine(sections, mCW0().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(88, 600));
		points.add(new Point(88, 593));
		sections.add(points);
		line = new GuiPinLine(sections, mCW1_8().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(125, 600));
		points.add(new Point(125, 593));
		sections.add(points);
		line = new GuiPinLine(sections, mCW1_8().getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(155, 601));
		points.add(new Point(155, 593));
		sections.add(points);
		line = new GuiPinLine(sections, mCW9_16().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(192, 601));
		points.add(new Point(192, 594));
		sections.add(points);
		line = new GuiPinLine(sections, mCW9_16().getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(232, 601));
		points.add(new Point(232, 594));
		sections.add(points);
		line = new GuiPinLine(sections, mCW17_36().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(340, 600));
		points.add(new Point(340, 593));
		sections.add(points);
		line = new GuiPinLine(sections, mCW57_76().getOutPin(19));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(447, 791));
		points.add(new Point(447, 772));
		points.add(new Point(243, 772));
		points.add(new Point(243, 709));
		points.add(new Point(92, 709));
		points.add(new Point(92, 674));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(92, 668));
		points.add(new Point(92, 630));
		sections.add(points);
		line = new GuiPinLine(sections, mMEM.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(122, 790));
		points.add(new Point(122, 771));
		points.add(new Point(201, 771));
		points.add(new Point(201, 693));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(176, 493));
		points.add(new Point(176, 535));
		sections.add(points);
		line = new GuiPinLine(sections, mMEM.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 798));
		points.add(new Point(373, 798));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.START"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 813));
		points.add(new Point(373, 813));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.OBI"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(360, 858));
		points.add(new Point(373, 858));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch3.pop_psw"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(360, 874));
		points.add(new Point(373, 874));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch3.brnch_regindpom"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 889));
		points.add(new Point(373, 889));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch3.call_regindpom"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 903));
		points.add(new Point(373, 903));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec4.brpom"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 919));
		points.add(new Point(373, 919));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.prekid"));
		gui.addLine(line);

		gui.addLabel(new GuiPinLabel(510, 220, KMADR1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(320, 210, KMOPR1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(122, 201, KMINTR1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(186, 335, MP1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(187, 430, mPC.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(54, 766, mMEM.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(381, 766, mMEM.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(388, 766, mMEM.getOutPin(1)));
		gui.addLabel(new GuiPinLabel(303, 639, mMEM.getOutPin(2)));
		gui.addLabel(new GuiPinLabel(60, 766, mMEM.getOutPin(1)));
		gui.addLabel(new GuiPinLabel(74, 766, mMEM.getOutPin(2)));
		gui.addLabel(new GuiPinLabel(88, 766, mMEM.getOutPin(3)));
		gui.addLabel(new GuiPinLabel(123, 766, mMEM.getOutPin(4)));
		gui.addLabel(new GuiPinLabel(158, 766, mMEM.getOutPin(5)));

	}

	private void putComponents() {
		// TODO Auto-generated method stub

	}

	private void putPins() {
		NameConnector.addPin(componentName, "KMADR", KMADR1.getOutPin(0));

		NameConnector.addPin(componentName, "KMOPR", KMOPR1.getOutPin(0));

		NameConnector.addPin(componentName, "KMINTR", KMINTR1.getOutPin(0));

		NameConnector.addPin(componentName, "mPC", mPC.getOutPin(0));
		
		NameConnector.addPin(componentName, "mCW0", mCWbit0.getOutPin(0));

		NameConnector.addPin(componentName, "mCW1", mCWbits1_8.getOutPin(7));

		NameConnector.addPin(componentName, "mCW2", mCWbits1_8.getOutPin(6));

		NameConnector.addPin(componentName, "mCW3", mCWbits1_8.getOutPin(5));

		NameConnector.addPin(componentName, "mCW4", mCWbits1_8.getOutPin(4));

		NameConnector.addPin(componentName, "mCW5", mCWbits1_8.getOutPin(3));

		NameConnector.addPin(componentName, "mCW6", mCWbits1_8.getOutPin(2));

		NameConnector.addPin(componentName, "mCW7", mCWbits1_8.getOutPin(1));

		NameConnector.addPin(componentName, "mCW8", mCWbits1_8.getOutPin(0));
	}

	public REG mPCREG() {
		return mPC;
	}

	public IntToBools mCW0() {
		return mCWbit0;
	}

	public IntToBools mCW1_8() {
		return mCWbits1_8;
	}

	public IntToBools mCW9_16() {
		return mCWbits9_16;

	}

	public IntToBools mCW17_36() {
		return mCWbits17_36;

	}

	public IntToBools mCW37_56() {
		return mCWbits37_56;
	}

	public IntToBools mCW57_76() {
		return mCWbits57_76;
	}

}
