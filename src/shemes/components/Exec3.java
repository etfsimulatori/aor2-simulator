package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiImageLabel;
import gui.GuiLableLine;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;
import gui.components.OrGui;
import logic.components.AND;
import logic.components.IntToBools;
import logic.components.NOT;
import logic.components.OR;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class Exec3 extends AbstractSchema {
	private IntToBools IBUS1bits;
	private IntToBools IBUS2bits;
	private IntToBools IBUS3bits;
	
	private OR NZOR;
	private AND NAND1;
	private NOT ZNOT[];
	private AND ZAND1, ZAND2;
	
	private OR COR1, COR2, COR3;
	private AND CAND1, CAND2, CAND3;
	private NOT CNOT1;
	
	private OR VOR1, VOR2, VOR3, VOR4;
	private AND VAND1, VAND2, VAND3, VAND4, VAND5, VAND6;
	private NOT VNOT1, VNOT2, VNOT3, VNOT4, VNOT5, VNOT6;
	
	public Exec3() {
		componentName = "Exec3";
		displayName = "Exec 3";
		NameConnector.addSchema(componentName, this);
	}
	
	public void initComponent() {
		IBUS1bits = new IntToBools(32, 32);
		IBUS2bits = new IntToBools(32, 32);
		IBUS3bits = new IntToBools(32, 32);
		
		int nz = Parameters.calcSize(Parameters.exec3Conections, "NZOR");
		NZOR = new OR(nz);
		NAND1 = new AND();

		ZNOT = new NOT[32];
		for (int i = 0; i < ZNOT.length; i++) {
			ZNOT[i] = new NOT();
		}
		ZAND1 = new AND(32);
		ZAND2 = new AND();
		
		COR1 = new OR();
		COR2 = new OR();
		COR3 = new OR(3);
		CAND1 = new AND();
		CAND2 = new AND();
		CAND3 = new AND();
		CNOT1 = new NOT();
		
		VOR1 = new OR();
		VOR2 = new OR();
		VOR3 = new OR();
		VOR4 = new OR();
		VAND1 = new AND(3);
		VAND2 = new AND(3);
		VAND3 = new AND();
		VAND4 = new AND(3);
		VAND5 = new AND(3);
		VAND6 = new AND();
		VNOT1 = new NOT();
		VNOT2 = new NOT();
		VNOT3 = new NOT();
		VNOT4 = new NOT();
		VNOT5 = new NOT();
		VNOT6 = new NOT();
		
		putPins();
		putComponents();
	}
	
	public void initConections() {
		IBUS1bits.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));
		IBUS2bits.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));
		IBUS3bits.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));
		
		String[] line = Parameters.getLine(Parameters.exec3Conections, "NZOR");
		for (int i = 0; i < NZOR.getInPins().length; i++) {
			NZOR.setInPin(i, NameConnector.getPin(line[i + 3]));
		}
		
		NAND1.setInPin(0, IBUS3bits.getOutPin(31));
		NAND1.setInPin(1, NZOR.getOutPin(0));

		for (int i = 0; i < 32; i++) {
			ZNOT[i].setInPin(0, IBUS3bits.getOutPin(i));
		}
		for (int i = 0; i < 32; i++) {
			ZAND1.setInPin(i, ZNOT[i].getOutPin(0));
		}

		ZAND2.setInPin(0, NZOR.getOutPin(0));
		ZAND2.setInPin(1, ZAND1.getOutPin(0));
		
		COR1.setInPin(0, NameConnector.getPin("Fetch2.SUB"));
		COR1.setInPin(1, NameConnector.getPin("Fetch2.CMP"));
		COR2.setInPin(0, NameConnector.getPin("Fetch2.LSR"));
		COR2.setInPin(1, NameConnector.getPin("Fetch2.LSL"));
		
		CAND1.setInPin(0, NameConnector.getPin("Fetch2.ADD"));
		CAND1.setInPin(1, NameConnector.getPin("Exec1.C_Last"));
		
		CNOT1.setInPin(0, NameConnector.getPin("Exec1.C_Last"));
		CAND2.setInPin(0, COR1.getOutPin(0));
		CAND2.setInPin(1, CNOT1.getOutPin(0));
		
		CAND3.setInPin(0, COR2.getOutPin(0));
		CAND3.setInPin(1, NameConnector.getPin("Exec1.C_Shift"));
		
		COR3.setInPin(0, CAND1.getOutPin(0));
		COR3.setInPin(1, CAND2.getOutPin(0));
		COR3.setInPin(2, CAND3.getOutPin(0));
		
		VOR1.setInPin(0, NameConnector.getPin("Fetch2.SUB"));
		VOR1.setInPin(1, NameConnector.getPin("Fetch2.CMP"));
		
		VNOT1.setInPin(0, IBUS2bits.getOutPin(31));
		VNOT2.setInPin(0, IBUS1bits.getOutPin(31));
		VNOT3.setInPin(0, IBUS3bits.getOutPin(31));
		VAND1.setInPin(0, VNOT1.getOutPin(0));
		VAND1.setInPin(1, VNOT2.getOutPin(0));
		VAND1.setInPin(2, IBUS3bits.getOutPin(31));
		VAND2.setInPin(0, IBUS2bits.getOutPin(31));
		VAND2.setInPin(1, IBUS1bits.getOutPin(31));
		VAND2.setInPin(2, VNOT3.getOutPin(0));
		
		VNOT4.setInPin(0, IBUS2bits.getOutPin(31));
		VNOT5.setInPin(0, IBUS1bits.getOutPin(31));
		VNOT6.setInPin(0, IBUS3bits.getOutPin(31));
		VAND4.setInPin(0, VNOT4.getOutPin(0));
		VAND4.setInPin(1, IBUS1bits.getOutPin(31));
		VAND4.setInPin(2, IBUS3bits.getOutPin(31));
		VAND5.setInPin(0, IBUS2bits.getOutPin(31));
		VAND5.setInPin(1, VNOT5.getOutPin(0));
		VAND5.setInPin(2, VNOT6.getOutPin(0));
		
		VOR2.setInPin(0, VAND1.getOutPin(0));
		VOR2.setInPin(1, VAND2.getOutPin(0));
		VOR3.setInPin(0, VAND4.getOutPin(0));
		VOR3.setInPin(1, VAND5.getOutPin(0));
		
		VAND3.setInPin(0, NameConnector.getPin("Fetch2.ADD"));
		VAND3.setInPin(1, VOR2.getOutPin(0));
		VAND4.setInPin(0, VOR1.getOutPin(0));
		VAND4.setInPin(1, VOR3.getOutPin(0));
		
		VOR4.setInPin(0, VAND3.getOutPin(0));
		VOR4.setInPin(1, VAND6.getOutPin(0));
	}
	
	public void initGui() {
		gui = new GuiSchema("src/images/Exec3.png");
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;
		
		OrGui orGui = new OrGui(NZOR, 43, 120, null, true, false);
		gui = orGui.convert(gui);
		
		for (int i = 0; i < 32; i++) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(109, 451 + 15 * i));
			points.add(new Point(120, 451 + 15 * i));
			sections.add(points);
			line = new GuiPinLine(sections, IBUS3bits.getOutPin(i));
			gui.addLine(line);
			GuiImageLabel im = new GuiImageLabel(119, 449 + 15 * i, "src/images/INV.png");
			gui.addLabel(im);

			gui.addLabel(new GuiTextLabel(65, 456 + 15 * i, "IBUS3", 12));
			gui.addLabel(new GuiTextLabel(100, 458 + 15 * i, "" + (32 - i - 1), 10));

		}
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(124, 445));
		points.add(new Point(124, 445 + 15 * 32));
		sections.add(points);
		line = new GuiLableLine(sections);
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(357, 45));
		points.add(new Point(365, 45));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(311, 326));
		points.add(new Point(319, 326));
		sections.add(points);
		line = new GuiPinLine(sections,      NameConnector.getPin("Fetch2.SUB"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(357, 56));
		points.add(new Point(364, 56));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(311, 337));
		points.add(new Point(319, 337));
		sections.add(points);
		line = new GuiPinLine(sections,      NameConnector.getPin("Fetch2.CMP"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 11));
		points.add(new Point(426, 11));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(312, 260));
		points.add(new Point(511, 260));
		sections.add(points);
		line = new GuiPinLine(sections,      NameConnector.getPin("Fetch2.ADD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(358, 88));
		points.add(new Point(366, 88));
		sections.add(points);
		line = new GuiPinLine(sections,      NameConnector.getPin("Fetch2.LSR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(358, 99));
		points.add(new Point(366, 99));
		sections.add(points);
		line = new GuiPinLine(sections,      NameConnector.getPin("Fetch2.LSL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(358, 115));
		points.add(new Point(413, 115));
		points.add(new Point(413, 105));
		points.add(new Point(427, 105));
		sections.add(points);
		line = new GuiPinLine(sections,      NameConnector.getPin("Exec1.C_Shift"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(358, 71));
		points.add(new Point(413, 71));
		points.add(new Point(413, 23));
		points.add(new Point(427, 23));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(414, 62));
		points.add(new Point(422, 62));
		sections.add(points);
		line = new GuiPinLine(sections,      NameConnector.getPin("Exec1.C_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(393, 193));
		points.add(new Point(404, 193));
		points.add(new Point(404, 367));
		points.add(new Point(415, 367));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(404, 271));
		points.add(new Point(411, 271));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(405, 295));
		points.add(new Point(415, 295));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(406, 343));
		points.add(new Point(411, 343));
		sections.add(points);
		line = new GuiPinLine(sections,      IBUS2bits.getOutPin(31));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(375, 215));
		points.add(new Point(383, 215));
		points.add(new Point(383, 373));
		points.add(new Point(411, 373));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(411, 277));
		points.add(new Point(384, 277));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(384, 301));
		points.add(new Point(415, 301));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(383, 349));
		points.add(new Point(415, 349));
		sections.add(points);
		line = new GuiPinLine(sections,      IBUS1bits.getOutPin(31));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(353, 241));
		points.add(new Point(359, 241));
		points.add(new Point(359, 379));
		points.add(new Point(411, 379));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(359, 355));
		points.add(new Point(415, 355));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(359, 307));
		points.add(new Point(411, 307));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(360, 283));
		points.add(new Point(415, 283));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(154, 21));
		points.add(new Point(161, 21));
		sections.add(points);
		line = new GuiPinLine(sections,      IBUS3bits.getOutPin(31));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(388, 50));
		points.add(new Point(426, 50));
		sections.add(points);
		line = new GuiPinLine(sections,      COR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(388, 94));
		points.add(new Point(426, 94));
		sections.add(points);
		line = new GuiPinLine(sections,      COR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(450, 17));
		points.add(new Point(457, 17));
		points.add(new Point(457, 49));
		points.add(new Point(478, 49));
		sections.add(points);
		line = new GuiPinLine(sections,      CAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(450, 56));
		points.add(new Point(480, 56));
		sections.add(points);
		line = new GuiPinLine(sections,      CAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(450, 100));
		points.add(new Point(457, 100));
		points.add(new Point(457, 63));
		points.add(new Point(479, 63));
		sections.add(points);
		line = new GuiPinLine(sections,      CAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(502, 56));
		points.add(new Point(509, 56));
		sections.add(points);
		line = new GuiPinLine(sections,      COR3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(341, 332));
		points.add(new Point(511, 332));
		sections.add(points);
		line = new GuiPinLine(sections,      VOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(440, 277));
		points.add(new Point(446, 277));
		points.add(new Point(446, 283));
		points.add(new Point(465, 283));
		sections.add(points);
		line = new GuiPinLine(sections,      VAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(439, 301));
		points.add(new Point(446, 301));
		points.add(new Point(446, 295));
		points.add(new Point(465, 295));
		sections.add(points);
		line = new GuiPinLine(sections,      VAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(535, 265));
		points.add(new Point(542, 265));
		points.add(new Point(542, 295));
		points.add(new Point(548, 295));
		sections.add(points);
		line = new GuiPinLine(sections,      VAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(439, 349));
		points.add(new Point(446, 349));
		points.add(new Point(446, 355));
		points.add(new Point(465, 355));
		sections.add(points);
		line = new GuiPinLine(sections,      VAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(439, 373));
		points.add(new Point(446, 373));
		points.add(new Point(446, 367));
		points.add(new Point(465, 367));
		sections.add(points);
		line = new GuiPinLine(sections,      VAND5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(534, 337));
		points.add(new Point(542, 337));
		points.add(new Point(542, 306));
		points.add(new Point(549, 306));
		sections.add(points);
		line = new GuiPinLine(sections,      VAND6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(487, 289));
		points.add(new Point(494, 289));
		points.add(new Point(494, 271));
		points.add(new Point(511, 271));
		sections.add(points);
		line = new GuiPinLine(sections,      VOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(486, 361));
		points.add(new Point(494, 361));
		points.add(new Point(494, 343));
		points.add(new Point(511, 343));
		sections.add(points);
		line = new GuiPinLine(sections,      VOR3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(571, 301));
		points.add(new Point(578, 301));
		sections.add(points);
		line = new GuiPinLine(sections,      VOR4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 684));
		points.add(new Point(162, 684));
		sections.add(points);
		line = new GuiPinLine(sections,      ZAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(186, 679));
		points.add(new Point(193, 679));
		sections.add(points);
		line = new GuiPinLine(sections,      ZAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(186, 27));
		points.add(new Point(193, 27));
		sections.add(points);
		line = new GuiPinLine(sections,      NAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(161, 673));
		points.add(new Point(154, 673));
		points.add(new Point(154, 32));
		points.add(new Point(161, 32));
		sections.add(points);
		line = new GuiPinLine(sections,      NZOR.getOutPin(0));
		gui.addLine(line);
	}
	
	private void putPins() {
		NameConnector.addPin(componentName, "N", NAND1.getOutPin(0));

		NameConnector.addPin(componentName, "Z", ZAND2.getOutPin(0));

		NameConnector.addPin(componentName, "C", COR3.getOutPin(0));

		NameConnector.addPin(componentName, "V", VOR4.getOutPin(0));
	}

	private void putComponents() {
		
	}

}
