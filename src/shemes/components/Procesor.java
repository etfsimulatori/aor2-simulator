package shemes.components;

import shemes.AbstractSchema;
import util.NameConnector;

public class Procesor extends AbstractSchema {

	public Procesor() {
		componentName = "Procesor";
		displayName = "Procesor";
		NameConnector.addSchema(componentName, this);
		this.addSubScheme(new ProcesorOperaciona());
		this.addSubScheme(new Counter());
	}

}
