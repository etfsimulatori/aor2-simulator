package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLine;
import gui.GuiSchema;
import logic.components.AND;
import logic.components.NOT;
import shemes.AbstractSchema;
import util.NameConnector;

public class Oper1 extends AbstractSchema {
	private AND ANDS[];
	private NOT NOTS[];
	private static String outPins[] = { "ldMAR", "incMAR", "ldMDR", "MDRout1", "MOST1_3", "MOST2_3", "MOST1_2", "MOST2_1", "MOST3_2", "RD", "WR", "stIO", "ldIR", "IRIMM18out1", "IRIMM18out2", "IRBRNCHout2", "IRJMPout2",
										"IRINTout3", "IRSHout1", "wrGPR", "incGPR", "decGPR", "GPRDSTout1", "GPRSRCout2", "PCout1", "SPout3", "ldPC", "incPC", "decPC", "ldSP", "incSP", "decSP", "add", "sub",
										"inc", "dec", "and", "or", "xor", "not", "shr", "shl", "ldAW", "ALUout3", "BSout3", "AWout1", "stPSWI", "clPSWI", "stPSWT", "clPSWT", "ldN",
										"ldZ", "ldC", "ldV", "ldL", "clSTART", "ldPSW", "PSWout1", "stPRINS", "clPRINS", "stPRCOD", "clPRCOD", "stPRADR", "clPRADR", "clPRINM", "clINTR", "UINTout3", "UEXTout3", "DECSPout3"};
	
	public Oper1() {
		componentName = "Oper1";
		displayName = "Signali operacione jedinice";
		NameConnector.addSchema(componentName, this);
	}
	
	public void initComponent() {
		ANDS = new AND[69];
		NOTS = new NOT[69];
		
		for (int i = 0; i < 69; i++) {
			ANDS[i] = new AND();
			NOTS[i] = new NOT();
		}
		
		putPins();
		putComponents();
	}

	public void initConections() {
		for (int i = 0; i < 69; i++) {
			NOTS[i].setInPin(0, ((Counter) NameConnector.getSchema("Counter")).mCW0().getOutPin(0));
			ANDS[i].setInPin(0, NOTS[i].getOutPin(0));
		}
		
		for (int i = 0; i < 8; i++)
			ANDS[i].setInPin(1, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(7 - i));
		
		for (int i = 0; i < 8; i++)
			ANDS[i + 8].setInPin(1, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(7 - i));
		
		for (int i = 0; i < 20; i++)
			ANDS[i + 16].setInPin(1, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(19 - i));
		
		for (int i = 0; i < 20; i++)
			ANDS[i + 36].setInPin(1, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(19 - i));
		
		for (int i = 0; i < 13; i++)
			ANDS[i + 56].setInPin(1, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(19 - i));
	}
	
	public void initGui() {
		gui = new GuiSchema("src/images/Oper1.png");
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 11));
		points.add(new Point(625, 11));
		points.add(new Point(625, 421));
		points.add(new Point(628, 421));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 12));
		points.add(new Point(51, 445));
		points.add(new Point(53, 445));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(243, 13));
		points.add(new Point(243, 421));
		points.add(new Point(245, 421));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(434, 12));
		points.add(new Point(434, 421));
		points.add(new Point(436, 421));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(53, 38));
		points.add(new Point(53, 38));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 62));
		points.add(new Point(53, 62));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 86));
		points.add(new Point(53, 86));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 110));
		points.add(new Point(53, 110));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(52, 134));
		points.add(new Point(53, 134));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 158));
		points.add(new Point(53, 158));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(52, 182));
		points.add(new Point(53, 182));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(52, 206));
		points.add(new Point(53, 206));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 230));
		points.add(new Point(53, 230));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 254));
		points.add(new Point(53, 254));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 278));
		points.add(new Point(53, 278));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 302));
		points.add(new Point(53, 302));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 325));
		points.add(new Point(53, 325));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 349));
		points.add(new Point(53, 349));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 373));
		points.add(new Point(53, 373));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(51, 397));
		points.add(new Point(53, 397));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 38));
		points.add(new Point(245, 38));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 62));
		points.add(new Point(245, 62));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 86));
		points.add(new Point(245, 86));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 110));
		points.add(new Point(245, 110));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(243, 134));
		points.add(new Point(245, 134));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 158));
		points.add(new Point(245, 158));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 182));
		points.add(new Point(245, 182));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 206));
		points.add(new Point(245, 206));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 230));
		points.add(new Point(245, 230));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 254));
		points.add(new Point(245, 254));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 278));
		points.add(new Point(245, 278));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 302));
		points.add(new Point(245, 302));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(243, 325));
		points.add(new Point(245, 325));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 349));
		points.add(new Point(245, 349));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 373));
		points.add(new Point(245, 373));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(244, 397));
		points.add(new Point(245, 397));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 38));
		points.add(new Point(436, 38));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 62));
		points.add(new Point(436, 62));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 86));
		points.add(new Point(436, 86));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 110));
		points.add(new Point(436, 110));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 134));
		points.add(new Point(436, 134));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 158));
		points.add(new Point(436, 158));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(434, 182));
		points.add(new Point(436, 182));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 206));
		points.add(new Point(436, 206));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(434, 230));
		points.add(new Point(436, 230));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(434, 254));
		points.add(new Point(436, 254));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 278));
		points.add(new Point(436, 278));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(434, 302));
		points.add(new Point(436, 302));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(436, 325));
		points.add(new Point(435, 325));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 349));
		points.add(new Point(436, 349));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(434, 373));
		points.add(new Point(436, 373));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(435, 397));
		points.add(new Point(436, 397));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 38));
		points.add(new Point(628, 38));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 62));
		points.add(new Point(628, 62));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 86));
		points.add(new Point(628, 86));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 110));
		points.add(new Point(628, 110));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(625, 134));
		points.add(new Point(628, 134));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(625, 158));
		points.add(new Point(628, 158));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(625, 182));
		points.add(new Point(628, 182));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 206));
		points.add(new Point(628, 206));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(627, 230));
		points.add(new Point(628, 230));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 254));
		points.add(new Point(628, 254));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 278));
		points.add(new Point(628, 278));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(625, 302));
		points.add(new Point(628, 302));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 325));
		points.add(new Point(628, 325));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 349));
		points.add(new Point(628, 349));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 373));
		points.add(new Point(628, 373));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(626, 397));
		points.add(new Point(628, 397));
		sections.add(points);
		line = new GuiPinLine(sections,   ((Counter) NameConnector.getSchema("Counter")).mCW0().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 43));
		points.add(new Point(86, 43));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[0].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 67));
		points.add(new Point(84, 67));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[1].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 91));
		points.add(new Point(84, 91));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[2].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 115));
		points.add(new Point(84, 115));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[3].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 139));
		points.add(new Point(84, 139));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[4].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 163));
		points.add(new Point(84, 163));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[5].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 187));
		points.add(new Point(84, 187));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[6].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 211));
		points.add(new Point(84, 211));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[7].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 235));
		points.add(new Point(84, 235));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[8].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 258));
		points.add(new Point(84, 258));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[9].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 282));
		points.add(new Point(84, 282));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[10].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 306));
		points.add(new Point(84, 306));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[11].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 330));
		points.add(new Point(84, 330));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[12].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 354));
		points.add(new Point(84, 354));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[13].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 378));
		points.add(new Point(84, 378));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[14].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 402));
		points.add(new Point(84, 402));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[15].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 426));
		points.add(new Point(84, 426));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[16].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 43));
		points.add(new Point(276, 43));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[17].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 67));
		points.add(new Point(276, 67));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[18].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 91));
		points.add(new Point(276, 91));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[19].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 115));
		points.add(new Point(277, 115));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[20].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 139));
		points.add(new Point(276, 139));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[21].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 163));
		points.add(new Point(276, 163));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[22].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 187));
		points.add(new Point(276, 187));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[23].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 211));
		points.add(new Point(276, 211));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[24].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 235));
		points.add(new Point(276, 235));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[25].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 258));
		points.add(new Point(276, 258));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[26].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 282));
		points.add(new Point(276, 282));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[27].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 306));
		points.add(new Point(276, 306));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[28].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 330));
		points.add(new Point(276, 330));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[29].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 354));
		points.add(new Point(276, 354));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[30].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 378));
		points.add(new Point(276, 378));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[31].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 402));
		points.add(new Point(276, 402));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[32].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(270, 426));
		points.add(new Point(276, 426));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[33].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 43));
		points.add(new Point(467, 43));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[34].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 67));
		points.add(new Point(467, 67));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[35].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 91));
		points.add(new Point(467, 91));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[36].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 115));
		points.add(new Point(467, 115));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[37].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 139));
		points.add(new Point(467, 139));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[38].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 163));
		points.add(new Point(467, 163));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[39].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 187));
		points.add(new Point(467, 187));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[40].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 211));
		points.add(new Point(467, 211));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[41].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 235));
		points.add(new Point(467, 235));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[42].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 258));
		points.add(new Point(467, 258));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[43].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 282));
		points.add(new Point(467, 282));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[44].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 306));
		points.add(new Point(467, 306));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[45].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 330));
		points.add(new Point(467, 330));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[46].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 354));
		points.add(new Point(467, 354));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[47].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 378));
		points.add(new Point(467, 378));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[48].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 402));
		points.add(new Point(467, 402));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[49].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(461, 426));
		points.add(new Point(467, 426));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[50].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 43));
		points.add(new Point(660, 43));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[51].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 67));
		points.add(new Point(660, 67));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[52].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 91));
		points.add(new Point(660, 91));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[53].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 115));
		points.add(new Point(660, 115));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[54].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 139));
		points.add(new Point(660, 139));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[55].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 163));
		points.add(new Point(660, 163));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[56].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 187));
		points.add(new Point(660, 187));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[57].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 211));
		points.add(new Point(660, 211));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[58].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 235));
		points.add(new Point(660, 235));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[59].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 258));
		points.add(new Point(660, 258));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[60].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 282));
		points.add(new Point(660, 282));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[61].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 306));
		points.add(new Point(660, 306));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[62].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 330));
		points.add(new Point(660, 330));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[63].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 354));
		points.add(new Point(660, 354));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[64].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 378));
		points.add(new Point(660, 378));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[65].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 402));
		points.add(new Point(660, 402));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[66].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(654, 426));
		points.add(new Point(660, 426));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[67].getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 450));
		points.add(new Point(86, 450));
		sections.add(points);
		line = new GuiPinLine(sections, ANDS[68].getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 48));
		points.add(new Point(57, 48));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 72));
		points.add(new Point(57, 72));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 96));
		points.add(new Point(57, 96));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 120));
		points.add(new Point(57, 120));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 144));
		points.add(new Point(57, 144));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 168));
		points.add(new Point(57, 168));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 191));
		points.add(new Point(57, 191));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 215));
		points.add(new Point(57, 215));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW1_8().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 239));
		points.add(new Point(57, 239));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 263));
		points.add(new Point(57, 263));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 287));
		points.add(new Point(57, 287));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 311));
		points.add(new Point(57, 311));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 335));
		points.add(new Point(57, 335));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 359));
		points.add(new Point(57, 359));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 383));
		points.add(new Point(57, 383));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 407));
		points.add(new Point(57, 407));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW9_16().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 431));
		points.add(new Point(57, 431));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(19));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(237, 48));
		points.add(new Point(249, 48));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(18));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(237, 72));
		points.add(new Point(249, 72));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(17));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 96));
		points.add(new Point(249, 96));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(16));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 120));
		points.add(new Point(249, 120));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(15));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 144));
		points.add(new Point(249, 144));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(14));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 168));
		points.add(new Point(249, 168));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(13));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 191));
		points.add(new Point(249, 191));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(12));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 215));
		points.add(new Point(249, 215));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(11));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 239));
		points.add(new Point(249, 239));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(10));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 263));
		points.add(new Point(249, 263));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(9));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 287));
		points.add(new Point(249, 287));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(8));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 311));
		points.add(new Point(249, 311));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 335));
		points.add(new Point(249, 335));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 359));
		points.add(new Point(249, 359));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 383));
		points.add(new Point(249, 383));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 407));
		points.add(new Point(249, 407));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(238, 431));
		points.add(new Point(249, 431));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 48));
		points.add(new Point(440, 48));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 72));
		points.add(new Point(440, 72));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW17_36().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 96));
		points.add(new Point(440, 96));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(19));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 120));
		points.add(new Point(440, 120));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(18));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 144));
		points.add(new Point(440, 144));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(17));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 168));
		points.add(new Point(440, 168));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(16));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 191));
		points.add(new Point(440, 191));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(15));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 215));
		points.add(new Point(440, 215));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(14));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(427, 239));
		points.add(new Point(440, 239));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(13));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 263));
		points.add(new Point(440, 263));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(12));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 287));
		points.add(new Point(440, 287));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(11));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 311));
		points.add(new Point(440, 311));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(10));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 335));
		points.add(new Point(440, 335));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(9));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 359));
		points.add(new Point(440, 359));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(8));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 383));
		points.add(new Point(440, 383));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(428, 407));
		points.add(new Point(440, 407));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(426, 431));
		points.add(new Point(440, 431));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 48));
		points.add(new Point(633, 48));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 72));
		points.add(new Point(633, 72));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 96));
		points.add(new Point(633, 96));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 120));
		points.add(new Point(633, 120));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 144));
		points.add(new Point(633, 144));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW37_56().getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 168));
		points.add(new Point(633, 168));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(19));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 191));
		points.add(new Point(633, 191));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(18));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 215));
		points.add(new Point(633, 215));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(17));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 239));
		points.add(new Point(633, 239));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(16));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 263));
		points.add(new Point(633, 263));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(15));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 287));
		points.add(new Point(633, 287));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(14));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 311));
		points.add(new Point(633, 311));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(13));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 335));
		points.add(new Point(633, 335));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(12));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 359));
		points.add(new Point(633, 359));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(11));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 383));
		points.add(new Point(633, 383));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(10));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 407));
		points.add(new Point(633, 407));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(9));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(620, 431));
		points.add(new Point(634, 431));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(8));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(45, 455));
		points.add(new Point(57, 455));
		sections.add(points);
		line = new GuiPinLine(sections, ((Counter) NameConnector.getSchema("Counter")).mCW57_76().getOutPin(7));
		gui.addLine(line);
	}
	
	private void putPins() {
		for (int i = 0; i < 69; i++)
			NameConnector.addPin(componentName, outPins[i], ANDS[i].getOutPin(0));
	}

	private void putComponents() {
		
	}
}
