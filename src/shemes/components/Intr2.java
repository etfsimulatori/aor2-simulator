package shemes.components;

import gui.GuiPinLine;
import gui.GuiSchema;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;


public class Intr2 extends AbstractSchema {
	
	private CD CD1;
	private CMP CMP1;
	private AND AND1;
	private OR OR1;
	private Pin logicalZero = new Pin(false, "0");
	
	public Intr2(){
		componentName = "Intr2";
		displayName = "Intr 2";
		NameConnector.addSchema(componentName, this);
	}

	@Override
	public void initComponent() {
		
		CD1 = new CD(8);
		
		CMP1 = new CMP(3);
		/*
		CMP1.getInPin(0).setIsBool();
		CMP1.getInPin(1).setIsBool();
		CMP1.getInPin(2).setIsBool();
		CMP1.getInPin(3).setIsBool();
		CMP1.getInPin(4).setIsBool();
		CMP1.getInPin(5).setIsBool();
		*/
		
		AND1 = new AND(3);
		
		OR1 = new OR(7);
		
		putPins();
		// putComponents(); - ?
	}

	@Override
	public void initConections() {
		
		OR1.setInPin(0, NameConnector.getPin("Intr1.PRINTR1"));
		OR1.setInPin(1, NameConnector.getPin("Intr1.PRINTR2"));
		OR1.setInPin(2, NameConnector.getPin("Intr1.PRINTR3"));
		OR1.setInPin(3, NameConnector.getPin("Intr1.PRINTR4"));
		OR1.setInPin(4, NameConnector.getPin("Intr1.PRINTR5"));
		OR1.setInPin(5, NameConnector.getPin("Intr1.PRINTR6"));
		OR1.setInPin(6, NameConnector.getPin("Intr1.PRINTR7"));
		
		AND1.setInPin(0, OR1.getOutPin(0));
		AND1.setInPin(1, CMP1.getGRT());
		AND1.setInPin(2, NameConnector.getPin("Exec2.PSWI"));

		CD1.setInPin(0, logicalZero);
		CD1.setInPin(1, NameConnector.getPin("Intr1.PRINTR1"));
		CD1.setInPin(2, NameConnector.getPin("Intr1.PRINTR2"));
		CD1.setInPin(3, NameConnector.getPin("Intr1.PRINTR3"));
		CD1.setInPin(4, NameConnector.getPin("Intr1.PRINTR4"));
		CD1.setInPin(5, NameConnector.getPin("Intr1.PRINTR5"));
		CD1.setInPin(6, NameConnector.getPin("Intr1.PRINTR6"));
		CD1.setInPin(7, NameConnector.getPin("Intr1.PRINTR7"));
		
		CMP1.setInPin(0, CD1.getOutPin(0));
		CMP1.setInPin(1, CD1.getOutPin(1));
		CMP1.setInPin(2, CD1.getOutPin(2));
		CMP1.setInPin(3, NameConnector.getPin("Exec2.PSWL0"));
		CMP1.setInPin(4, NameConnector.getPin("Exec2.PSWL1"));
		CMP1.setInPin(5, NameConnector.getPin("Exec2.PSWL2"));
		
	}

	@Override
	public void initGui() {
		
		gui = new GuiSchema("src/images/Intr2.png");
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 12));
		points.add(new Point(194, 12));
		points.add(new Point(194, 341));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(194, 246));
		points.add(new Point(132, 246));
		points.add(new Point(132, 217));
		points.add(new Point(118, 217));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR7"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 27));
		points.add(new Point(210, 27));
		points.add(new Point(210, 341));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(210, 231));
		points.add(new Point(138, 231));
		points.add(new Point(138, 214));
		points.add(new Point(117, 214));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR6"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 42));
		points.add(new Point(225, 42));
		points.add(new Point(225, 342));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(226, 215));
		points.add(new Point(144, 215));
		points.add(new Point(144, 210));
		points.add(new Point(115, 210));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR5"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 57));
		points.add(new Point(239, 57));
		points.add(new Point(239, 342));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(240, 200));
		points.add(new Point(144, 200));
		points.add(new Point(144, 206));
		points.add(new Point(116, 206));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR4"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 72));
		points.add(new Point(252, 72));
		points.add(new Point(252, 342));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(253, 186));
		points.add(new Point(138, 186));
		points.add(new Point(138, 202));
		points.add(new Point(116, 202));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 88));
		points.add(new Point(268, 88));
		points.add(new Point(268, 341));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(269, 170));
		points.add(new Point(133, 170));
		points.add(new Point(133, 199));
		points.add(new Point(118, 199));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(339, 103));
		points.add(new Point(282, 103));
		points.add(new Point(282, 342));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(283, 155));
		points.add(new Point(127, 155));
		points.add(new Point(127, 195));
		points.add(new Point(118, 195));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(81, 276));
		points.add(new Point(87, 276));
		points.add(new Point(87, 285));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWI"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 331));
		points.add(new Point(298, 341));
		sections.add(points);
		line = new GuiPinLine(sections, logicalZero);
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(95, 208));
		points.add(new Point(87, 208));
		points.add(new Point(87, 265));
		points.add(new Point(80, 265));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(80, 270));
		points.add(new Point(119, 270));
		points.add(new Point(119, 390));
		sections.add(points);
		line = new GuiPinLine(sections, CMP1.getGRT());
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(58, 270));
		points.add(new Point(49, 270));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(261, 388));
		points.add(new Point(261, 446));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(338, 445));
		points.add(new Point(149, 445));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(246, 388));
		points.add(new Point(246, 429));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(149, 428));
		points.add(new Point(338, 428));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(231, 388));
		points.add(new Point(231, 414));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(339, 413));
		points.add(new Point(149, 413));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(73, 445));
		points.add(new Point(89, 445));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWL0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(89, 428));
		points.add(new Point(73, 428));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWL1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(89, 413));
		points.add(new Point(73, 413));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWL2"));
		gui.addLine(line);

	}

	private void putPins() {
		
		NameConnector.addPin(componentName, "mprintr", OR1.getOutPin(0));
		
		NameConnector.addPin(componentName, "printr", AND1.getOutPin(0));
		
		NameConnector.addPin(componentName, "prl0", CD1.getOutPin(0));
		
		NameConnector.addPin(componentName, "prl1", CD1.getOutPin(1));
		
		NameConnector.addPin(componentName, "prl2", CD1.getOutPin(2));
	}
}
