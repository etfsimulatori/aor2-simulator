package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.CD;
import logic.components.DC;
import shemes.AbstractSchema;
import util.NameConnector;

public class Arb extends AbstractSchema {

	private CD CD1;
	private DC DC1;

	public Arb() {
		componentName = "Arb";
		displayName = "Arbitrator";
		NameConnector.addSchema(componentName, this);
	}
	
	public void initComponent() {

		CD1 = new CD(8);
		CD1.setE(new Pin(true, "1"));

		DC1 = new DC(3);
		putPins();

	}

	public void initConections() {

		DC1.setE(CD1.getW());
		DC1.setInPin(0, CD1.getOutPin(0));
		DC1.setInPin(1, CD1.getOutPin(1));
		DC1.setInPin(2, CD1.getOutPin(2));

		for (int i = 0; i < 7; i++) {
			CD1.setInPin(i, new Pin(false, "0"));
		}
		CD1.setInPin(7, NameConnector.getPin("Bus2.BR"));

	}
	
	public void initGui() {

		gui = new GuiSchema("src/images/Arb.png");

		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(242, 166));
		points.add(new Point(242, 196));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 166));
		points.add(new Point(202, 196));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(162, 166));
		points.add(new Point(162, 195));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 129));
		points.add(new Point(22, 129));
		points.add(new Point(22, 254));
		points.add(new Point(38, 254));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getW());
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 16));
		points.add(new Point(338, 46));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(295, 16));
		points.add(new Point(295, 45));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(254, 16));
		points.add(new Point(254, 46));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(215, 17));
		points.add(new Point(215, 46));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(179, 45));
		points.add(new Point(179, 17));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(141, 17));
		points.add(new Point(141, 45));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(101, 16));
		points.add(new Point(101, 46));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(22, 88));
		points.add(new Point(39, 88));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(60, 45));
		points.add(new Point(60, 16));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.BR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 317));
		points.add(new Point(338, 346));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(296, 317));
		points.add(new Point(296, 347));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(254, 317));
		points.add(new Point(254, 347));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(216, 317));
		points.add(new Point(216, 347));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 317));
		points.add(new Point(180, 346));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(141, 317));
		points.add(new Point(141, 346));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(101, 317));
		points.add(new Point(101, 346));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(61, 316));
		points.add(new Point(61, 345));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(7));
		gui.addLine(line);

	}


	public void putPins() {
		for (int i = 0; i < 8; i++)
			NameConnector.addPin(componentName, "BG_IN_" + i, DC1.getOutPin(i));

	}
}
