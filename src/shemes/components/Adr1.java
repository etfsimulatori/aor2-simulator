package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;

public class Adr1 extends AbstractSchema {
	
	private MP MX1;
	private TSB TSBGPRDST, TSBGPRSRC, TSBPC, TSBSP, TSBDECSP;
	private IntToBools IRbits;
	private BoolsToInt IRMX0;
	private BoolsToInt IRMX1;
	private BoolsToInt IRASRC;
	private ADD ADD1;
	
	public Adr1(){
		componentName = "Addr";
		displayName = "Addr";
		NameConnector.addSchema(componentName, this);
		
		this.addSubScheme(new GPRFile());
	}
	
	@Override
	public void initComponent() {
		super.initComponent();
		
		IRbits = new IntToBools(32, 32);
		
		IRMX0 = new BoolsToInt(4, 4);
		IRMX1 = new BoolsToInt(4, 4);
		IRASRC = new BoolsToInt(4, 4);
		
		MX1 = new MP(2);
		MX1.getOutPin(0).setIsInt();
		MX1.getOutPin(0).setNumOfLines(4);
	
		TSBGPRDST = new TSB("TSBGPRDST");
		TSBGPRDST.getOutPin(0).setNumOfLines(32);
		
		TSBGPRSRC = new TSB("TSBGPRSRC");
		TSBGPRSRC.getOutPin(0).setNumOfLines(32);
		
		TSBPC = new TSB("TSBPC");
		TSBPC.getOutPin(0).setNumOfLines(32);
		
		TSBSP = new TSB("TSBSP");
		TSBSP.getOutPin(0).setNumOfLines(32);
		
		TSBDECSP = new TSB("TSBDECSP");
		TSBDECSP.getOutPin(0).setNumOfLines(32);
		
		ADD1 = new ADD();
		ADD1.getOutPin(0).setIsInt();
		ADD1.getOutPin(0).setNumOfLines(32);
		
		putPins();
		putComponents();
		
	}

	@Override
	public void initConections() {
		super.initConections();

		IRbits.setInPin(0, NameConnector.getPin("Fetch1.REGIR"));
		
		for (int i = 0; i < 4; i++) {
			IRMX0.setInPin(i, IRbits.getOutPin(i+22));	
		}
		
		for (int i = 0; i < 4; i++) {
			IRMX1.setInPin(i, IRbits.getOutPin(i+21));	
		}
		
		for (int i = 0; i < 4; i++) {
			IRASRC.setInPin(i, IRbits.getOutPin(i+18));	
		}
		
		MX1.setInPin(0, IRMX0.getOutPin(0));
		MX1.setInPin(1, IRMX1.getOutPin(0));
		MX1.setCtrl(0, NameConnector.getPin("Fetch3.sdst"));
		
		TSBGPRDST.setInPin(0, NameConnector.getPin("Gpr.DOdst"));
		TSBGPRDST.setE(NameConnector.getPin("Oper1.GPRDSTout1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBGPRDST.getOutPin(0));
		
		TSBGPRSRC.setInPin(0, NameConnector.getPin("Gpr.DOsrc"));
		TSBGPRSRC.setE(NameConnector.getPin("Oper1.GPRSRCout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBGPRSRC.getOutPin(0));
		
		TSBPC.setInPin(0, NameConnector.getPin("Gpr.PC"));
		TSBPC.setE(NameConnector.getPin("Oper1.PCout1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBPC.getOutPin(0));
		
		TSBSP.setInPin(0, NameConnector.getPin("Gpr.SP"));
		TSBSP.setE(NameConnector.getPin("Oper1.SPout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBSP.getOutPin(0));
		
		ADD1.setPinA(NameConnector.getPin("Gpr.SP"));
		ADD1.setPinB(new Pin(-4, 32, "DECSP"));
		
		TSBDECSP.setInPin(0, ADD1.getOutPin(0));
		TSBDECSP.setE(NameConnector.getPin("Oper1.DECSPout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBDECSP.getOutPin(0));
		
		
	}

	@Override
	public void initGui() {
		super.initGui();
		
		gui = new GuiSchema("src/images/Addr1.png");
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(564, 10));
		points.add(new Point(564, 723));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(545, 33));
		points.add(new Point(545, 691));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(526, 56));
		points.add(new Point(526, 659));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(526, 143));
		points.add(new Point(281, 143));
		points.add(new Point(281, 162));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(169, 122));
		points.add(new Point(169, 190));
		points.add(new Point(195, 190));
		sections.add(points);
		line = new GuiPinLine(sections, IRASRC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(76, 179));
		points.add(new Point(76, 228));
		points.add(new Point(195, 228));
		sections.add(points);
		line = new GuiPinLine(sections, MX1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(375, 181));
		points.add(new Point(368, 181));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.wrGPR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(375, 200));
		points.add(new Point(368, 200));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.incGPR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(375, 219));
		points.add(new Point(368, 219));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.decGPR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(57, 122));
		points.add(new Point(57, 139));
		sections.add(points);
		line = new GuiPinLine(sections, IRMX0.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(95, 122));
		points.add(new Point(95, 139));
		sections.add(points);
		line = new GuiPinLine(sections, IRMX1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(27, 159));
		points.add(new Point(37, 159));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch3.sdst"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(350, 257));
		points.add(new Point(350, 294));
		points.add(new Point(431, 294));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Gpr.DOdst"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(443, 308));
		points.add(new Point(443, 300));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.GPRDSTout1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(454, 294));
		points.add(new Point(563, 294));
		sections.add(points);
		line = new GuiPinLine(sections, TSBGPRDST.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(302, 256));
		points.add(new Point(302, 360));
		points.add(new Point(431, 360));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Gpr.DOsrc"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(443, 366));
		points.add(new Point(443, 374));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.GPRSRCout2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(454, 360));
		points.add(new Point(545, 360));
		sections.add(points);
		line = new GuiPinLine(sections, TSBGPRSRC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(261, 257));
		points.add(new Point(261, 426));
		points.add(new Point(431, 426));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Gpr.PC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(443, 432));
		points.add(new Point(443, 440));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.PCout1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(454, 427));
		points.add(new Point(563, 427));
		sections.add(points);
		line = new GuiPinLine(sections, TSBPC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(226, 257));
		points.add(new Point(226, 493));
		points.add(new Point(431, 493));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(226, 493));
		points.add(new Point(226, 681));
		points.add(new Point(300, 681));
		points.add(new Point(300, 649));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Gpr.SP"));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(376, 665));
		points.add(new Point(376, 649));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(-4, 32, "DECSP"));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(337, 609));
		points.add(new Point(337, 567));
		points.add(new Point(430, 567));
		sections.add(points);
		line = new GuiPinLine(sections, ADD1.getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(453, 567));
		points.add(new Point(526, 567));
		sections.add(points);
		line = new GuiPinLine(sections, TSBDECSP.getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(443, 581));
		points.add(new Point(443, 572));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.DECSPout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(443, 506));
		points.add(new Point(443, 498));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.SPout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(454, 493));
		points.add(new Point(525, 493));
		sections.add(points);
		line = new GuiPinLine(sections, TSBSP.getOutPin(0));
		gui.addLine(line);
		
		
		gui.addLabel(new GuiPinLabel(355, 290, NameConnector.getPin("Gpr.DOdst")));
		gui.addLabel(new GuiPinLabel(355, 355, NameConnector.getPin("Gpr.DOsrc")));
		gui.addLabel(new GuiPinLabel(355, 420, NameConnector.getPin("Gpr.PC")));
		gui.addLabel(new GuiPinLabel(355, 490, NameConnector.getPin("Gpr.SP")));
		gui.addLabel(new GuiPinLabel(575, 30, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(554, 50, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(535, 70, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(280, 140, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(180, 225, MX1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(180, 185, IRASRC.getOutPin(0)));

	}

	public void putPins() {

		//NameConnector.addPin(componentName, "DI", NameConnector.getPin("Bus1.IBUS1"));

		NameConnector.addPin(componentName, "Asrc", IRASRC.getOutPin(0));
		
		NameConnector.addPin(componentName, "Adst", MX1.getOutPin(0));
		
		//NameConnector.addPin(componentName, "wrGPR", NameConnector.getPin("Oper1.wrGPR"));
		
		//NameConnector.addPin(componentName, "incGPR", NameConnector.getPin("Oper1.incGPR"));
		
		//NameConnector.addPin(componentName, "decGPR", NameConnector.getPin("Oper1.decGPR"));
		
	}
	
	private void putComponents() {
		
	}

}
