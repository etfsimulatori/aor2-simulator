package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.IntToBools;
import logic.components.MP;
import logic.components.NOT;
import logic.components.OR;
import logic.components.XOR;
import shemes.AbstractSchema;
import util.NameConnector;

public class Exec4 extends AbstractSchema {

	private NOT NOT1, NOT2, NOT3, NOT4, NOT5, NOT6, NOT7, NOT8;//, NOT9, NOT10;
	private XOR XOR1;
	private OR OR1;//, OR2;
	//private AND AND1, AND2, AND3;
	private MP MX7, MX8, MX9;
	private IntToBools REGIRbits;
	

	public Exec4() {
		componentName = "Exec4";
		displayName = "Exec 4";
		NameConnector.addSchema(componentName, this);
	}
	
	public void initComponent() {
		//AND1 = new AND();
		//AND2 = new AND();
		//AND3 = new AND();
		NOT1 = new NOT();
		NOT2 = new NOT();
		NOT3 = new NOT();
		NOT4 = new NOT();
		NOT5 = new NOT();
		NOT6 = new NOT();
		NOT7 = new NOT();
		NOT8 = new NOT();
		//NOT9 = new NOT();
		//NOT10 = new NOT();
		XOR1 = new XOR();
		OR1 = new OR();
		//OR2 = new OR();
		MX7 = new MP(8);
		MX8 = new MP(8);
		MX9 = new MP(2);
		REGIRbits= new IntToBools(32, 32);

		putPins();
		putComponents();
	}
	
	public void initConections() {

		NOT1.setInPin(0, NameConnector.getPin("Exec2.PSWZ"));
		XOR1.setInPin(0, NameConnector.getPin("Exec2.PSWN"));
		XOR1.setInPin(1, NameConnector.getPin("Exec2.PSWV"));
		NOT2.setInPin(0, XOR1.getOutPin(0));
		OR1.setInPin(0, XOR1.getOutPin(0));
		OR1.setInPin(1, NameConnector.getPin("Exec2.PSWZ"));
		NOT3.setInPin(0, OR1.getOutPin(0));
		NOT4.setInPin(0, NameConnector.getPin("Exec2.PSWV"));
		NOT5.setInPin(0, NameConnector.getPin("Exec2.PSWN"));
		NOT6.setInPin(0,REGIRbits.getOutPin(28) );
		NOT7.setInPin(0, REGIRbits.getOutPin(27) );
		NOT8.setInPin(0, REGIRbits.getOutPin(26) );
		//NOT9.setInPin(0, REGIRbits.getOutPin(31) );
		//NOT10.setInPin(0, REGIRbits.getOutPin(30) );
		//AND2.setInPin(0, NOT9.getOutPin(0));
		//AND2.setInPin(1, NOT10.getOutPin(0));
		//AND3.setInPin(0, NOT9.getOutPin(0));
		//AND3.setInPin(1, REGIRbits.getOutPin(30));
		//OR2.setInPin(0, AND2.getOutPin(0));
		//OR2.setInPin(1, AND3.getOutPin(0));
		//AND1.setInPin(0, MX9.getOutPin(0));
		//AND1.setInPin(1, OR2.getOutPin(0));
		
		MX7.setInPin(0, OR1.getOutPin(0));// jle
		MX7.setInPin(1, NOT3.getOutPin(0));// jg
		MX7.setInPin(2, NOT2.getOutPin(0));// jge
		MX7.setInPin(3, NOT1.getOutPin(0));//jne
		MX7.setInPin(4, NameConnector.getPin("Exec2.PSWZ"));// je
		MX7.setInPin(5, new Pin(false,"0"));//0
		MX7.setInPin(6, new Pin(false,"0"));// 0
		MX7.setInPin(7, new Pin(false,"0"));//0
		MX7.setCtrl(0, NOT8.getOutPin(0));
		MX7.setCtrl(1, NOT7.getOutPin(0));
		MX7.setCtrl(2, NOT6.getOutPin(0));

		MX8.setInPin(0, XOR1.getOutPin(0));//jl
		MX8.setInPin(1, NameConnector.getPin("Exec2.PSWN"));//jp
		MX8.setInPin(2, NOT5.getOutPin(0));//jnp
		MX8.setInPin(3, NameConnector.getPin("Exec2.PSWN"));//jo
		MX8.setInPin(4, NOT4.getOutPin(0));//jno
		MX8.setInPin(5, new Pin(false,"0"));//0
		MX8.setInPin(6, new Pin(false,"0"));//0
		MX8.setInPin(7, new Pin(false,"0"));//0
		MX8.setCtrl(0, REGIRbits.getOutPin(26));
		MX8.setCtrl(1, REGIRbits.getOutPin(27));
		MX8.setCtrl(2, REGIRbits.getOutPin(28));

		MX9.setInPin(0, MX8.getOutPin(0));
		MX9.setInPin(1, MX7.getOutPin(0));
		MX9.setCtrl(0,REGIRbits.getOutPin(29));

		REGIRbits.setInPin(0, NameConnector.getPin("Fetch1.REGIR"));
		
	}
	
	public void initGui() {
		
		gui = new GuiSchema("src/images/Exec4.png");
		
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(44, 12));
		points.add(new Point(161, 12));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(119, 12));
		points.add(new Point(119, 29));
		points.add(new Point(126, 29));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(45, 108));
		points.add(new Point(90, 108));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(94, 283));
		points.add(new Point(102, 283));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWZ"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(162, 29));
		points.add(new Point(156, 29));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(96, 302));
		points.add(new Point(102, 302));
		sections.add(points);
		line = new GuiPinLine(sections, NOT1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(53, 52));
		points.add(new Point(44, 52));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Exec2.PSWN"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(297, 57));
		points.add(new Point(409, 57));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(377, 74));
		points.add(new Point(368, 74));
		points.add(new Point(368, 58));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(246, 340));
		points.add(new Point(253, 340));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Exec2.PSWN"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(53, 63));
		points.add(new Point(45, 63));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(246, 302));
		points.add(new Point(253, 302));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(377, 29));
		points.add(new Point(368, 29));
		points.add(new Point(368, 12));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(411, 12));
		points.add(new Point(296, 12));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Exec2.PSWV"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(155, 74));
		points.add(new Point(162, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(94, 321));
		points.add(new Point(103, 321));
		sections.add(points);
		line = new GuiPinLine(sections, NOT2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(155, 120));
		points.add(new Point(162, 120));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(94, 340));
		points.add(new Point(102, 340));
		sections.add(points);
		line = new GuiPinLine(sections, NOT3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(410, 29));
		points.add(new Point(403, 29));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(245, 283));
		points.add(new Point(252, 283));
		sections.add(points);
		line = new GuiPinLine(sections, NOT4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(410, 74));
		points.add(new Point(403, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(245, 321));
		points.add(new Point(253, 321));
		sections.add(points);
		line = new GuiPinLine(sections, NOT5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(75, 57));
		points.add(new Point(161, 57));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(82, 58));
		points.add(new Point(82, 97));
		points.add(new Point(91, 97));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(128, 74));
		points.add(new Point(119, 74));
		points.add(new Point(119, 58));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(246, 358));
		points.add(new Point(253, 358));
		sections.add(points);
		line = new GuiPinLine(sections, XOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(112, 102));
		points.add(new Point(162, 102));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(95, 358));
		points.add(new Point(103, 358));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(127, 119));
		points.add(new Point(119, 119));
		points.add(new Point(119, 102));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

//		sections = new ArrayList<List<Point>>();
//		points = new ArrayList<Point>();
//		points.add(new Point(424, 314));
//		points.add(new Point(424, 294));
//		points.add(new Point(450, 294));
//		sections.add(points);
//		line = new GuiPinLine(sections, OR2.getOutPin(0));
//		gui.addLine(line);

//		sections = new ArrayList<List<Point>>();
//		points = new ArrayList<Point>();
//		points.add(new Point(473, 289));
//		points.add(new Point(482, 289));
//		sections.add(points);
//		line = new GuiPinLine(sections, AND1.getOutPin(0));
//		gui.addLine(line);

//		sections = new ArrayList<List<Point>>();
//		points = new ArrayList<Point>();
//		points.add(new Point(402, 356));
//		points.add(new Point(402, 349));
//		points.add(new Point(419, 349));
//		points.add(new Point(419, 335));
//		sections.add(points);
//		line = new GuiPinLine(sections, AND2.getOutPin(0));
//		gui.addLine(line);

//		sections = new ArrayList<List<Point>>();
//		points = new ArrayList<Point>();
//		points.add(new Point(447, 357));
//		points.add(new Point(447, 349));
//		points.add(new Point(430, 349));
//		points.add(new Point(430, 335));
//		sections.add(points);
//		line = new GuiPinLine(sections, AND3.getOutPin(0));
//		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(159, 293));
		points.add(new Point(178, 293));
		points.add(new Point(178, 198));
		points.add(new Point(328, 198));
		points.add(new Point(328, 274));
		points.add(new Point(341, 274));
		sections.add(points);
		line = new GuiPinLine(sections, MX7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(309, 293));
		points.add(new Point(341, 293));
		sections.add(points);
		line = new GuiPinLine(sections, MX8.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(388, 283));
		points.add(new Point(415, 283));
		sections.add(points);
		line = new GuiPinLine(sections, MX9.getOutPin(0));
		gui.addLine(line);

//		sections = new ArrayList<List<Point>>();
//		points = new ArrayList<Point>();
//		points.add(new Point(388, 389));
//		points.add(new Point(396, 389));
//		points.add(new Point(396, 385));
//		sections.add(points);
//		points = new ArrayList<Point>();
//		points.add(new Point(433, 389));
//		points.add(new Point(441, 389));
//		points.add(new Point(441, 386));
//		sections.add(points);
//		line = new GuiPinLine(sections, REGIRbits.getOutPin(31));
//		gui.addLine(line);

//		sections = new ArrayList<List<Point>>();
//		points = new ArrayList<Point>();
//		points.add(new Point(460, 389));
//		points.add(new Point(452, 389));
//		points.add(new Point(452, 380));
//		sections.add(points);
//		points = new ArrayList<Point>();
//		points.add(new Point(416, 389));
//		points.add(new Point(407, 389));
//		points.add(new Point(407, 384));
//		sections.add(points);
//		line = new GuiPinLine(sections, REGIRbits.getOutPin(30));
//		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(364, 321));
		points.add(new Point(364, 312));
		sections.add(points);
		line = new GuiPinLine(sections, REGIRbits.getOutPin(29));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(102, 394));
		points.add(new Point(102, 385));
		points.add(new Point(115, 385));
		points.add(new Point(115, 382));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(253, 393));
		points.add(new Point(253, 385));
		points.add(new Point(266, 385));
		points.add(new Point(266, 378));
		sections.add(points);
		line = new GuiPinLine(sections, REGIRbits.getOutPin(28));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 393));
		points.add(new Point(130, 382));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(281, 394));
		points.add(new Point(281, 377));
		sections.add(points);
		line = new GuiPinLine(sections, REGIRbits.getOutPin(27));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(159, 393));
		points.add(new Point(159, 385));
		points.add(new Point(146, 385));
		points.add(new Point(146, 381));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(309, 393));
		points.add(new Point(309, 385));
		points.add(new Point(296, 385));
		points.add(new Point(296, 377));
		sections.add(points);
		line = new GuiPinLine(sections, REGIRbits.getOutPin(26));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(252, 227));
		points.add(new Point(247, 227));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(245, 246));
		points.add(new Point(252, 246));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(245, 264));
		points.add(new Point(253, 264));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(94, 227));
		points.add(new Point(102, 227));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(94, 246));
		points.add(new Point(102, 246));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(95, 264));
		points.add(new Point(103, 264));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false,"0"));
		gui.addLine(line);




	}
	
	public void putPins() {
		NameConnector.addPin(componentName, "brpom", MX9.getOutPin(0));

	}

	public void putComponents() {

	}
	
}
