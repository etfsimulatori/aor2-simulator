package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;


import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.AND;
import logic.components.CLK;
import logic.components.DC;
import logic.components.NOT;
import logic.components.OR;
import logic.components.RSFF;
import shemes.AbstractSchema;
import util.NameConnector;

public class Intr1 extends AbstractSchema {
	
	private RSFF PRINS, PRCOD, PRADR, PRINM;
	private RSFF PRINTR1, PRINTR2, PRINTR3, PRINTR4, PRINTR5, PRINTR6, PRINTR7;
	private DC DC1;
	private OR ORprekid;
	private AND ANDprekid;
	private NOT NOTprekid;
	
	
	
	public Intr1() {
		componentName = "Intr1";
		displayName = "Intr 1";
		NameConnector.addSchema(componentName, this);
	}
	
	@Override
	public void initComponent() {
		PRINS = new RSFF("PRINS");
		
		PRCOD = new RSFF("PRCOD");
		
		PRADR = new RSFF("PRADR");
		
		PRINM = new RSFF("PRINM");
		
		PRINTR1 = new RSFF("PRINTR1");
		
		PRINTR2 = new RSFF("PRINTR2");
		
		PRINTR3 = new RSFF("PRINTR3");
		
		PRINTR4 = new RSFF("PRINTR4");
		
		PRINTR5 = new RSFF("PRINTR5");
		
		PRINTR6 = new RSFF("PRINTR6");
		
		PRINTR7 = new RSFF("PRINTR7");
	
		ORprekid = new OR(6);
		ANDprekid = new AND(2);
		NOTprekid = new NOT();
		
		DC1 = new DC(3);
		
		putPins();
		putComponents();
	}
	
	@Override
	public void initConections() {
		PRINS.setInPin(0, NameConnector.getPin("Oper1.stPRINS"));
		PRINS.setInPin(1, NameConnector.getPin("Oper1.clPRINS"));
		PRINS.setClk((CLK)NameConnector.getComponent("CLK"));
		PRINS.setReset(new Pin(false, "0"));
		
		PRCOD.setInPin(0, NameConnector.getPin("Oper1.stPRCOD"));
		PRCOD.setInPin(1, NameConnector.getPin("Oper1.clPRCOD"));
		PRCOD.setClk((CLK) NameConnector.getComponent("CLK"));
		PRCOD.setReset(new Pin(false, "0"));

		PRADR.setInPin(0, NameConnector.getPin("Oper1.stPRADR"));
		PRADR.setInPin(1, NameConnector.getPin("Oper1.clPRADR"));
		PRADR.setClk((CLK) NameConnector.getComponent("CLK"));
		PRADR.setReset(new Pin(false, "0"));

		PRINM.setInPin(0, new Pin(false, "0"));//TODO OVDE ZAKACITI PIN SA FAULTA
		PRINM.setInPin(1, NameConnector.getPin("Oper1.clPRINM"));
		PRINM.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINM.setReset(new Pin(false, "0"));

		PRINTR1.setInPin(0, new Pin(false, "0"));
		PRINTR1.setInPin(1, DC1.getOutPin(1));
		PRINTR1.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINTR1.setReset(new Pin(false, "0"));

		PRINTR2.setInPin(0, new Pin(false, "0"));
		PRINTR2.setInPin(1, DC1.getOutPin(2));
		PRINTR2.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINTR2.setReset(new Pin(false, "0"));

		PRINTR3.setInPin(0, new Pin(false, "0"));
		PRINTR3.setInPin(1, DC1.getOutPin(3));
		PRINTR3.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINTR3.setReset(new Pin(false, "0"));

		PRINTR4.setInPin(0, new Pin(false, "0"));
		PRINTR4.setInPin(1, DC1.getOutPin(4));
		PRINTR4.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINTR4.setReset(new Pin(false, "0"));

		PRINTR5.setInPin(0, new Pin(false, "0"));
		PRINTR5.setInPin(1, DC1.getOutPin(5));
		PRINTR5.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINTR5.setReset(new Pin(false, "0"));

		PRINTR6.setInPin(0, new Pin(false, "0"));
		PRINTR6.setInPin(1, DC1.getOutPin(6));
		PRINTR6.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINTR6.setReset(new Pin(false, "0"));

		PRINTR7.setInPin(0, new Pin(false, "0"));
		PRINTR7.setInPin(1, DC1.getOutPin(7));
		PRINTR7.setClk((CLK) NameConnector.getComponent("CLK"));
		PRINTR7.setReset(new Pin(false, "0"));
		
		NOTprekid.setInPin(0, NameConnector.getPin("Fetch2.IRET"));
		ANDprekid.setInPin(0, NameConnector.getPin("Exec2.PSWT"));
		ANDprekid.setInPin(1, NOTprekid.getOutPin(0));
		
		ORprekid.setInPin(0, PRINS.getOutPin(0));
		ORprekid.setInPin(1, PRCOD.getOutPin(0));
		ORprekid.setInPin(2, PRADR.getOutPin(0));
		ORprekid.setInPin(3, PRINM.getOutPin(0));
		ORprekid.setInPin(4, NameConnector.getPin("Intr2.printr"));
		ORprekid.setInPin(5, ANDprekid.getOutPin(0));
		
		DC1.setE(NameConnector.getPin("Oper1.clINTR"));
		DC1.setInPin(0, NameConnector.getPin("Intr2.prl0"));
		DC1.setInPin(1, NameConnector.getPin("Intr2.prl1"));
		DC1.setInPin(2, NameConnector.getPin("Intr2.prl2"));
		
		
	}
	
	@Override
	public void initGui() {
		gui = new GuiSchema("src/images/Intr1.png");
		
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 42));
		points.add(new Point(82, 42));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.stPRINS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 72));
		points.add(new Point(82, 72));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 102));
		points.add(new Point(82, 102));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.clPRINS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 42));
		points.add(new Point(156, 42));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 102));
		points.add(new Point(156, 102));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINS.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 42));
		points.add(new Point(306, 42));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.stPRCOD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 72));
		points.add(new Point(306, 72));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 102));
		points.add(new Point(306, 102));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.clPRCOD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 42));
		points.add(new Point(380, 42));
		sections.add(points);
		line = new GuiPinLine(sections,  PRCOD.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 102));
		points.add(new Point(380, 102));
		sections.add(points);
		line = new GuiPinLine(sections,  PRCOD.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 42));
		points.add(new Point(531, 42));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.stPRADR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 72));
		points.add(new Point(531, 72));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 102));
		points.add(new Point(531, 102));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.clPRADR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 42));
		points.add(new Point(604, 42));
		sections.add(points);
		line = new GuiPinLine(sections,  PRADR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 102));
		points.add(new Point(604, 102));
		sections.add(points);
		line = new GuiPinLine(sections,  PRADR.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 170));
		points.add(new Point(82, 170));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 200));
		points.add(new Point(82, 200));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 230));
		points.add(new Point(82, 230));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.clPRINM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 170));
		points.add(new Point(156, 170));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 230));
		points.add(new Point(156, 230));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINM.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 298));
		points.add(new Point(82, 298));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 328));
		points.add(new Point(82, 328));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 358));
		points.add(new Point(82, 358));
		sections.add(points);
		line = new GuiPinLine(sections,  DC1.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 298));
		points.add(new Point(156, 298));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 358));
		points.add(new Point(156, 358));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR3.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 298));
		points.add(new Point(306, 298));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 328));
		points.add(new Point(306, 328));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 358));
		points.add(new Point(306, 358));
		sections.add(points);
		line = new GuiPinLine(sections,  DC1.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 298));
		points.add(new Point(380, 298));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 358));
		points.add(new Point(380, 358));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR2.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 298));
		points.add(new Point(531, 298));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 328));
		points.add(new Point(531, 328));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 358));
		points.add(new Point(531, 358));
		sections.add(points);
		line = new GuiPinLine(sections,  DC1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 298));
		points.add(new Point(604, 298));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 358));
		points.add(new Point(604, 358));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 426));
		points.add(new Point(82, 426));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 456));
		points.add(new Point(82, 456));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(74, 486));
		points.add(new Point(82, 486));
		sections.add(points);
		line = new GuiPinLine(sections,  DC1.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 426));
		points.add(new Point(156, 426));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 486));
		points.add(new Point(156, 486));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR6.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 426));
		points.add(new Point(306, 426));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 456));
		points.add(new Point(306, 456));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(298, 486));
		points.add(new Point(306, 486));
		sections.add(points);
		line = new GuiPinLine(sections,  DC1.getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 426));
		points.add(new Point(380, 426));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 486));
		points.add(new Point(380, 486));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR5.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 426));
		points.add(new Point(531, 426));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 456));
		points.add(new Point(531, 456));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 486));
		points.add(new Point(531, 486));
		sections.add(points);
		line = new GuiPinLine(sections,  DC1.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 426));
		points.add(new Point(604, 426));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 486));
		points.add(new Point(604, 486));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR4.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 554));
		points.add(new Point(531, 554));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 584));
		points.add(new Point(531, 584));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(523, 614));
		points.add(new Point(531, 614));
		sections.add(points);
		line = new GuiPinLine(sections,  DC1.getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 554));
		points.add(new Point(604, 554));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(596, 614));
		points.add(new Point(604, 614));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINTR7.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 548));
		points.add(new Point(142, 548));
		points.add(new Point(142, 570));
		points.add(new Point(145, 570));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 565));
		points.add(new Point(138, 565));
		points.add(new Point(138, 578));
		points.add(new Point(145, 578));
		sections.add(points);
		line = new GuiPinLine(sections,  PRCOD.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 582));
		points.add(new Point(133, 582));
		points.add(new Point(133, 587));
		points.add(new Point(150, 587));
		sections.add(points);
		line = new GuiPinLine(sections,  PRADR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 599));
		points.add(new Point(133, 599));
		points.add(new Point(133, 595));
		points.add(new Point(150, 595));
		sections.add(points);
		line = new GuiPinLine(sections,  PRINM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 616));
		points.add(new Point(138, 616));
		points.add(new Point(138, 604));
		points.add(new Point(145, 604));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Intr2.printr"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 657));
		points.add(new Point(142, 657));
		points.add(new Point(142, 612));
		points.add(new Point(145, 612));
		sections.add(points);
		line = new GuiPinLine(sections,  ANDprekid.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(95, 651));
		points.add(new Point(104, 651));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Exec2.PSWT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(95, 664));
		points.add(new Point(100, 664));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Fetch2.IRET"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(303, 629));
		points.add(new Point(311, 629));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(303, 612));
		points.add(new Point(311, 612));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(302, 595));
		points.add(new Point(311, 595));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(341, 690));
		points.add(new Point(341, 698));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clINTR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 672));
		points.add(new Point(380, 672));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 655));
		points.add(new Point(380, 655));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 638));
		points.add(new Point(380, 638));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 620));
		points.add(new Point(380, 620));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 603));
		points.add(new Point(380, 603));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 586));
		points.add(new Point(380, 586));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 570));
		points.add(new Point(380, 570));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 552));
		points.add(new Point(380, 552));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(7));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(173, 591));
		points.add(new Point(182, 591));
		sections.add(points);
		line = new GuiPinLine(sections, ORprekid.getOutPin(0));
		gui.addLine(line);
	}

	private void putPins() {
		NameConnector.addPin(componentName, "prekid", ORprekid.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINS", PRINS.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRCOD", PRCOD.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRADR", PRADR.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINM", PRINM.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINTR1", PRINTR1.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINTR2", PRINTR2.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINTR3", PRINTR3.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINTR4", PRINTR4.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINTR5", PRINTR5.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINTR6", PRINTR6.getOutPin(0));
		
		NameConnector.addPin(componentName, "PRINTR7", PRINTR7.getOutPin(0));
	}

	private void putComponents() {
		
	}

}
