package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;



import java.awt.Point;
import java.util.ArrayList;
import java.util.List;



import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;


public class Exec1 extends AbstractSchema {
	
	private TSB TSBALUout3,TSBBSout3,TSBAWout1;
	private ALU ALUexec;
	private REG AW;
	private BarrelShifter BS;
	private IntToInt  IBUS1ToN;

	public Exec1() {
		componentName = "Exec1";
		displayName = "Exec 1";
		NameConnector.addSchema(componentName, this);
	}
	public void initComponent() {
		
		IBUS1ToN= new IntToInt(32,5);
		
		TSBALUout3 = new TSB("ALUout3");
		TSBALUout3.getOutPin(0).setNumOfLines(32);

		TSBBSout3 = new TSB("BSout3");
		TSBBSout3.getOutPin(0).setNumOfLines(32);

		ALUexec = new ALU(32);
		TSBAWout1 = new TSB("AWout1");
		TSBAWout1.getOutPin(0).setNumOfLines(32);
	

		BS=new  BarrelShifter(32);
		
		
		AW = new REG(1, "AW");
		AW.getOutPin(0).setIsInt();
		AW.getOutPin(0).setNumOfLines(32);
		
		putPins();
		putComponents();
	}
	
	@Override
	public void initGui() {
		gui = new GuiSchema("src/images/Exec1.png");
		
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(175, 196));
		points.add(new Point(175, 134));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(175, 133));
		points.add(new Point(381, 133));
		sections.add(points);
		line = new GuiPinLine(sections,   ALUexec.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(392, 148));
		points.add(new Point(392, 138));
		sections.add(points);
		line = new GuiPinLine(sections,   NameConnector.getPin("Oper1.ALUout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(403, 133));
		points.add(new Point(652, 133));
		sections.add(points);
		line = new GuiPinLine(sections,  TSBALUout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(126, 295));
		points.add(new Point(126, 357));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(127, 358));
		points.add(new Point(690, 358));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(225, 296));
		points.add(new Point(225, 322));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(226, 322));
		points.add(new Point(672, 322));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(252, 195));
		points.add(new Point(252, 180));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(253, 180));
		points.add(new Point(288, 180));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Exec1.C_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(60, 221));
		points.add(new Point(76, 221));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.not"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(59, 237));
		points.add(new Point(75, 237));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.xor"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(60, 254));
		points.add(new Point(77, 254));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.or"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(59, 271));
		points.add(new Point(77, 271));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.and"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(275, 221));
		points.add(new Point(291, 221));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.dec"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(275, 237));
		points.add(new Point(291, 237));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.inc"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(275, 254));
		points.add(new Point(292, 254));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.sub"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(275, 270));
		points.add(new Point(292, 270));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.add"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(92, 783));
		points.add(new Point(98, 783));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.ldAW"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(56, 312));
		points.add(new Point(101, 312));
		points.add(new Point(101, 297));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(250, 784));
		points.add(new Point(258, 784));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(174, 803));
		points.add(new Point(174, 822));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(175, 822));
		points.add(new Point(653, 822));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(374, 729));
		points.add(new Point(374, 722));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.AWout1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(385, 717));
		points.add(new Point(691, 717));
		sections.add(points);
		line = new GuiPinLine(sections,  TSBAWout1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(174, 765));
		points.add(new Point(174, 717));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(174, 717));
		points.add(new Point(362, 717));
		sections.add(points);
		line = new GuiPinLine(sections,  AW.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(175, 547));
		points.add(new Point(175, 615));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(176, 615));
		points.add(new Point(671, 615));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(404, 416));
		points.add(new Point(653, 416));
		sections.add(points);
		line = new GuiPinLine(sections,  TSBBSout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(392, 430));
		points.add(new Point(392, 421));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Oper1.BSout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(176, 478));
		points.add(new Point(176, 416));
		points.add(new Point(381, 416));
		sections.add(points);
		line = new GuiPinLine(sections, BS.getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(252, 478));
		points.add(new Point(252, 462));
		points.add(new Point(289, 462));
		sections.add(points);
		line = new GuiPinLine(sections, BS.getPinC_Shift());
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(275, 514));
		points.add(new Point(690, 514));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1ToN.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(690, 12));
		points.add(new Point(690, 980));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(672, 947));
		points.add(new Point(672, 48));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(653, 80));
		points.add(new Point(653, 913));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(59, 501));
		points.add(new Point(77, 501));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.shr"));
		gui.addLine(line);
		

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(60, 524));
		points.add(new Point(76, 524));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.shl"));
		gui.addLine(line);


		gui.addLabel(new GuiPinLabel(178, 405, BS.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(175, 707, AW.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(177, 124, ALUexec.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(700, 40, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(680, 75, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(660, 110, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(140, 352, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(240, 320, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(290, 510, IBUS1ToN.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(185, 610, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(185, 820, NameConnector.getPin("Bus1.IBUS3")));

	}
	
	private void putComponents() {
	
			NameConnector.addComponent(componentName, "AW", AW);
	}
		private void putPins() {
		
			NameConnector.addPin(componentName, "C_Last", ALUexec.getPinC_Last());
			
			NameConnector.addPin(componentName, "C_Shift", BS.getPinC_Shift());
		
	}
		public void initConections() {

			TSBALUout3.setInPin(0, ALUexec.getOutPin(0));
			TSBALUout3.setE(NameConnector.getPin("Oper1.ALUout3"));
			((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBALUout3.getOutPin(0));

			ALUexec.setPinX(NameConnector.getPin("Bus1.IBUS1"));
			ALUexec.setPinY(NameConnector.getPin("Bus1.IBUS2"));

			ALUexec.setPinAdd(NameConnector.getPin("Oper1.add"));
			ALUexec.setPinSub(NameConnector.getPin("Oper1.sub"));
			ALUexec.setPinInc(NameConnector.getPin("Oper1.inc"));
			ALUexec.setPinDec(NameConnector.getPin("Oper1.dec"));
			ALUexec.setPinAnd(NameConnector.getPin("Oper1.and"));
			ALUexec.setPinOr(NameConnector.getPin("Oper1.or"));
			ALUexec.setPinXor(NameConnector.getPin("Oper1.xor"));
			ALUexec.setPinNot(NameConnector.getPin("Oper1.not"));
			

			AW.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));
			AW.setPinLd(NameConnector.getPin("Oper1.ldAW"));
			AW.setClk((CLK) NameConnector.getComponent("CLK"));

			BS.setPinX(NameConnector.getPin("Bus1.IBUS2"));
			BS.setPinSHL(NameConnector.getPin("Oper1.shl"));
			BS.setPinSHR(NameConnector.getPin("Oper1.shr"));
			
			IBUS1ToN.setInPin(0,NameConnector.getPin("Bus1.IBUS1"));
			
			BS.setPinN(IBUS1ToN.getOutPin(0));// TODO  mozda ne radi
			
			
			TSBAWout1.setInPin(0, AW.getOutPin(0));
			TSBAWout1.setE(NameConnector.getPin("Oper1.AWout1"));
			((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBAWout1.getOutPin(0));

			TSBBSout3.setInPin(0, BS.getOutPin(0));
			TSBBSout3.setE(NameConnector.getPin("Oper1.BSout3"));
			((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBBSout3.getOutPin(0));

		}
		
		public REG RegAW() {
			return AW;
		}

}
