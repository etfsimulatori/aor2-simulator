package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.AND;
import logic.components.BoolsToInt;
import logic.components.CLK;
import logic.components.IntToBools;
import logic.components.NOT;
import logic.components.OR;
import logic.components.RSFF;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.NameConnector;

public class Exec2 extends AbstractSchema {

	private IntToBools IBUS1bits;
	private AND IAND1;
	private OR IOR1;
	private AND IAND2;
	private OR IOR2;
	private NOT INOT;
	private RSFF IRSFF;
	private AND TAND1;
	private OR TOR1;
	private AND TAND2;
	private OR TOR2;
	private NOT TNOT;
	private RSFF TRSFF;
	private AND NAND1;
	private AND NAND2;
	private OR NOR1;
	private NOT NNOT1;
	private AND NAND3;
	private NOT NNOT2;
	private AND NAND4;
	private OR NOR2;
	private RSFF NRSFF;
	private AND ZAND1;
	private AND ZAND2;
	private OR ZOR1;
	private NOT ZNOT1;
	private AND ZAND3;
	private NOT ZNOT2;
	private AND ZAND4;
	private OR ZOR2;
	private RSFF ZRSFF;
	private AND CAND1;
	private AND CAND2;
	private OR COR1;
	private NOT CNOT1;
	private AND CAND3;
	private NOT CNOT2;
	private AND CAND4;
	private OR COR2;
	private RSFF CRSFF;
	private AND VAND1;
	private AND VAND2;
	private OR VOR1;
	private NOT VNOT1;
	private AND VAND3;
	private NOT VNOT2;
	private AND VAND4;
	private OR VOR2;
	private RSFF VRSFF;
	private AND L0AND1;
	private AND L0AND2;
	private OR L0OR1;
	private NOT L0NOT1;
	private AND L0AND3;
	private NOT L0NOT2;
	private AND L0AND4;
	private OR L0OR2;
	private RSFF L0RSFF;
	private AND L1AND1;
	private AND L1AND2;
	private OR L1OR1;
	private NOT L1NOT1;
	private AND L1AND3;
	private NOT L1NOT2;
	private AND L1AND4;
	private OR L1OR2;
	private RSFF L1RSFF;
	private AND L2AND1;
	private AND L2AND2;
	private OR L2OR1;
	private NOT L2NOT1;
	private AND L2AND3;
	private NOT L2NOT2;
	private AND L2AND4;
	private OR L2OR2;
	private RSFF L2RSFF;
	private BoolsToInt PSW;
	private TSB TSBPSWout1;
	public RSFF STARTRSFF;

	public Exec2() {
		componentName = "Exec2";
		displayName = "Exec 2";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		IBUS1bits = new IntToBools(32, 32);

		IAND1 = new AND();
		IOR1 = new OR();
		INOT = new NOT();
		IAND2 = new AND();
		IOR2 = new OR();
		IRSFF = new RSFF("I");

		TAND1 = new AND();
		TOR1 = new OR();
		TNOT = new NOT();
		TAND2 = new AND();
		TOR2 = new OR();
		TRSFF = new RSFF("T");

		NAND1 = new AND();
		NAND2 = new AND();
		NOR1 = new OR();
		NNOT1 = new NOT();
		NAND3 = new AND();
		NNOT2 = new NOT();
		NAND4 = new AND();
		NOR2 = new OR();
		NRSFF = new RSFF("N");

		ZAND1 = new AND();
		ZAND2 = new AND();
		ZOR1 = new OR();
		ZNOT1 = new NOT();
		ZAND3 = new AND();
		ZNOT2 = new NOT();
		ZAND4 = new AND();
		ZOR2 = new OR();
		ZRSFF = new RSFF("Z");

		CAND1 = new AND();
		CAND2 = new AND();
		COR1 = new OR();
		CNOT1 = new NOT();
		CAND3 = new AND();
		CNOT2 = new NOT();
		CAND4 = new AND();
		COR2 = new OR();
		CRSFF = new RSFF("C");

		VAND1 = new AND();
		VAND2 = new AND();
		VOR1 = new OR();
		VNOT1 = new NOT();
		VAND3 = new AND();
		VNOT2 = new NOT();
		VAND4 = new AND();
		VOR2 = new OR();
		VRSFF = new RSFF("V");

		L0AND1 = new AND();
		L0AND2 = new AND();
		L0OR1 = new OR();
		L0NOT1 = new NOT();
		L0AND3 = new AND();
		L0NOT2 = new NOT();
		L0AND4 = new AND();
		L0OR2 = new OR();
		L0RSFF = new RSFF("L0");

		L1AND1 = new AND();
		L1AND2 = new AND();
		L1OR1 = new OR();
		L1NOT1 = new NOT();
		L1AND3 = new AND();
		L1NOT2 = new NOT();
		L1AND4 = new AND();
		L1OR2 = new OR();
		L1RSFF = new RSFF("L1");

		L2AND1 = new AND();
		L2AND2 = new AND();
		L2OR1 = new OR();
		L2NOT1 = new NOT();
		L2AND3 = new AND();
		L2NOT2 = new NOT();
		L2AND4 = new AND();
		L2OR2 = new OR();
		L2RSFF = new RSFF("L2");

		PSW = new BoolsToInt(32, 32);
		TSBPSWout1 = new TSB("PSWHout3");
		TSBPSWout1.getOutPin(0).setNumOfLines(32);

		STARTRSFF = new RSFF("START");

		putPins();
		putComponents();
	}

	public void initConections() {
		IBUS1bits.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));

		IAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		IAND1.setInPin(1, IBUS1bits.getOutPin(31));
		IOR1.setInPin(0, NameConnector.getPin("Oper1.stPSWI"));
		IOR1.setInPin(1, IAND1.getOutPin(0));
		INOT.setInPin(0, IBUS1bits.getOutPin(31));
		IAND2.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		IAND2.setInPin(1, INOT.getOutPin(0));
		IOR2.setInPin(0, IAND2.getOutPin(0));
		IOR2.setInPin(1, NameConnector.getPin("Oper1.clPSWI"));
		IRSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		IRSFF.setReset(new Pin(false, "0"));
		IRSFF.setPinS(IOR1.getOutPin(0));
		IRSFF.setPinR(IOR2.getOutPin(0));

		TAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		TAND1.setInPin(1, IBUS1bits.getOutPin(30));
		TOR1.setInPin(0, NameConnector.getPin("Oper1.stPSWT"));
		TOR1.setInPin(1, TAND1.getOutPin(0));
		TNOT.setInPin(0, IBUS1bits.getOutPin(30));
		TAND2.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		TAND2.setInPin(1, TNOT.getOutPin(0));
		TOR2.setInPin(0, TAND2.getOutPin(0));
		TOR2.setInPin(1, NameConnector.getPin("Oper1.clPSWT"));
		TRSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		TRSFF.setReset(new Pin(false, "0"));
		TRSFF.setPinS(TOR1.getOutPin(0));
		TRSFF.setPinR(TOR2.getOutPin(0));

		NAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		NAND1.setInPin(1, IBUS1bits.getOutPin(0));
		NAND2.setInPin(0, NameConnector.getPin("Oper1.ldN"));
		NAND2.setInPin(1, NameConnector.getPin("Exec3.N"));
		NOR1.setInPin(0, NAND2.getOutPin(0));
		NOR1.setInPin(1, NAND1.getOutPin(0));
		NNOT1.setInPin(0, IBUS1bits.getOutPin(0));
		NAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		NAND3.setInPin(1, NNOT1.getOutPin(0));
		NNOT2.setInPin(0, NameConnector.getPin("Exec3.N"));
		NAND4.setInPin(0, NameConnector.getPin("Oper1.ldN"));
		NAND4.setInPin(1, NNOT2.getOutPin(0));
		NOR2.setInPin(0, NAND3.getOutPin(0));
		NOR2.setInPin(1, NAND4.getOutPin(0));
		NRSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		NRSFF.setReset(new Pin(false, "0"));
		NRSFF.setPinS(NOR1.getOutPin(0));
		NRSFF.setPinR(NOR2.getOutPin(0));

		ZAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		ZAND1.setInPin(1, IBUS1bits.getOutPin(1));
		ZAND2.setInPin(0, NameConnector.getPin("Oper1.ldZ"));
		ZAND2.setInPin(1, NameConnector.getPin("Exec3.Z"));
		ZOR1.setInPin(0, ZAND2.getOutPin(0));
		ZOR1.setInPin(1, ZAND1.getOutPin(0));
		ZNOT1.setInPin(0, IBUS1bits.getOutPin(1));
		ZAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		ZAND3.setInPin(1, ZNOT1.getOutPin(0));
		ZNOT2.setInPin(0, NameConnector.getPin("Exec3.Z"));
		ZAND4.setInPin(0, NameConnector.getPin("Oper1.ldZ"));
		ZAND4.setInPin(1, ZNOT2.getOutPin(0));
		ZOR2.setInPin(0, ZAND3.getOutPin(0));
		ZOR2.setInPin(1, ZAND4.getOutPin(0));
		ZRSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		ZRSFF.setReset(new Pin(false, "0"));
		ZRSFF.setPinS(ZOR1.getOutPin(0));
		ZRSFF.setPinR(ZOR2.getOutPin(0));

		CAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		CAND1.setInPin(1, IBUS1bits.getOutPin(2));
		CAND2.setInPin(0, NameConnector.getPin("Oper1.ldC"));
		CAND2.setInPin(1, NameConnector.getPin("Exec3.C"));
		COR1.setInPin(0, CAND2.getOutPin(0));
		COR1.setInPin(1, CAND1.getOutPin(0));
		CNOT1.setInPin(0, IBUS1bits.getOutPin(2));
		CAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		CAND3.setInPin(1, CNOT1.getOutPin(0));
		CNOT2.setInPin(0, NameConnector.getPin("Exec3.C"));
		CAND4.setInPin(0, NameConnector.getPin("Oper1.ldC"));
		CAND4.setInPin(1, CNOT2.getOutPin(0));
		COR2.setInPin(0, CAND3.getOutPin(0));
		COR2.setInPin(1, CAND4.getOutPin(0));
		CRSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		CRSFF.setReset(new Pin(false, "0"));
		CRSFF.setPinS(COR1.getOutPin(0));
		CRSFF.setPinR(COR2.getOutPin(0));

		VAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		VAND1.setInPin(1, IBUS1bits.getOutPin(3));
		VAND2.setInPin(0, NameConnector.getPin("Oper1.ldV"));
		VAND2.setInPin(1, NameConnector.getPin("Exec3.V"));
		VOR1.setInPin(0, VAND2.getOutPin(0));
		VOR1.setInPin(1, VAND1.getOutPin(0));
		VNOT1.setInPin(0, IBUS1bits.getOutPin(3));
		VAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		VAND3.setInPin(1, VNOT1.getOutPin(0));
		VNOT2.setInPin(0, NameConnector.getPin("Exec3.V"));
		VAND4.setInPin(0, NameConnector.getPin("Oper1.ldV"));
		VAND4.setInPin(1, VNOT2.getOutPin(0));
		VOR2.setInPin(0, VAND3.getOutPin(0));
		VOR2.setInPin(1, VAND4.getOutPin(0));
		VRSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		VRSFF.setReset(new Pin(false, "0"));
		VRSFF.setPinS(VOR1.getOutPin(0));
		VRSFF.setPinR(VOR2.getOutPin(0));

		L0AND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		L0AND1.setInPin(1, IBUS1bits.getOutPin(4));
		L0AND2.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L0AND2.setInPin(1, NameConnector.getPin("Intr2.prl0"));
		L0OR1.setInPin(0, L0AND2.getOutPin(0));
		L0OR1.setInPin(1, L0AND1.getOutPin(0));
		L0NOT1.setInPin(0, IBUS1bits.getOutPin(4));
		L0AND3.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		L0AND3.setInPin(1, L0NOT1.getOutPin(0));
		L0NOT2.setInPin(0, NameConnector.getPin("Intr2.prl0"));
		L0AND4.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L0AND4.setInPin(1, L0NOT2.getOutPin(0));
		L0OR2.setInPin(0, L0AND3.getOutPin(0));
		L0OR2.setInPin(1, L0AND4.getOutPin(0));
		L0RSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		L0RSFF.setReset(new Pin(false, "0"));
		L0RSFF.setPinS(L0OR1.getOutPin(0));
		L0RSFF.setPinR(L0OR2.getOutPin(0));

		L1AND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		L1AND1.setInPin(1, IBUS1bits.getOutPin(5));
		L1AND2.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L1AND2.setInPin(1, NameConnector.getPin("Intr2.prl1"));
		L1OR1.setInPin(0, L1AND2.getOutPin(0));
		L1OR1.setInPin(1, L1AND1.getOutPin(0));
		L1NOT1.setInPin(0, IBUS1bits.getOutPin(5));
		L1AND3.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		L1AND3.setInPin(1, L1NOT1.getOutPin(0));
		L1NOT2.setInPin(0, NameConnector.getPin("Intr2.prl1"));
		L1AND4.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L1AND4.setInPin(1, L1NOT2.getOutPin(0));
		L1OR2.setInPin(0, L1AND3.getOutPin(0));
		L1OR2.setInPin(1, L1AND4.getOutPin(0));
		L1RSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		L1RSFF.setReset(new Pin(false, "0"));
		L1RSFF.setPinS(L1OR1.getOutPin(0));
		L1RSFF.setPinR(L1OR2.getOutPin(0));

		L2AND1.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		L2AND1.setInPin(1, IBUS1bits.getOutPin(6));
		L2AND2.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L2AND2.setInPin(1, NameConnector.getPin("Intr2.prl2"));
		L2OR1.setInPin(0, L2AND2.getOutPin(0));
		L2OR1.setInPin(1, L2AND1.getOutPin(0));
		L2NOT1.setInPin(0, IBUS1bits.getOutPin(6));
		L2AND3.setInPin(0, NameConnector.getPin("Oper1.ldPSW"));
		L2AND3.setInPin(1, L2NOT1.getOutPin(0));
		L2NOT2.setInPin(0, NameConnector.getPin("Intr2.prl2"));
		L2AND4.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L2AND4.setInPin(1, L2NOT2.getOutPin(0));
		L2OR2.setInPin(0, L2AND3.getOutPin(0));
		L2OR2.setInPin(1, L2AND4.getOutPin(0));
		L2RSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		L2RSFF.setReset(new Pin(false, "0"));
		L2RSFF.setPinS(L2OR1.getOutPin(0));
		L2RSFF.setPinR(L2OR2.getOutPin(0));

		for (int i = 7; i < 30; i++) {
			PSW.setInPin(i, new Pin(false, "0"));
		}
		PSW.setInPin(31, IRSFF.getOutPin(0));
		PSW.setInPin(30, TRSFF.getOutPin(0));
		TSBPSWout1.setInPin(0, PSW.getOutPin(0));
		TSBPSWout1.setE(NameConnector.getPin("Oper1.PSWout1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBPSWout1
				.getOutPin(0));

		PSW.setInPin(0, NRSFF.getOutPin(0));
		PSW.setInPin(1, ZRSFF.getOutPin(0));
		PSW.setInPin(2, CRSFF.getOutPin(0));
		PSW.setInPin(3, VRSFF.getOutPin(0));
		PSW.setInPin(4, L0RSFF.getOutPin(0));
		PSW.setInPin(5, L1RSFF.getOutPin(0));
		PSW.setInPin(6, L2RSFF.getOutPin(0));

		STARTRSFF.setClk((CLK) NameConnector.getComponent("CLK"));
		STARTRSFF.setPinS(new Pin(false, "0"));
		STARTRSFF.setPinR(NameConnector.getPin("Oper1.clSTART"));
		STARTRSFF.setInit(true);

	}

	public void initGui() {

		gui = new GuiSchema("src/images/Exec2.png");

		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(294, 74));
		points.add(new Point(303, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(72, 530));
		points.add(new Point(84, 530));
		sections.add(points);
		line = new GuiPinLine(sections, IRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 127));
		points.add(new Point(302, 127));
		sections.add(points);
		line = new GuiPinLine(sections, IRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(294, 218));
		points.add(new Point(302, 218));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(73, 549));
		points.add(new Point(84, 549));
		sections.add(points);
		line = new GuiPinLine(sections, TRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 271));
		points.add(new Point(302, 271));
		sections.add(points);
		line = new GuiPinLine(sections, TRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 74));
		points.add(new Point(703, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(72, 727));
		points.add(new Point(84, 727));
		sections.add(points);
		line = new GuiPinLine(sections, NRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 127));
		points.add(new Point(702, 127));
		sections.add(points);
		line = new GuiPinLine(sections, NRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(694, 218));
		points.add(new Point(702, 218));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(73, 710));
		points.add(new Point(85, 710));
		sections.add(points);
		line = new GuiPinLine(sections, ZRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(694, 271));
		points.add(new Point(702, 271));
		sections.add(points);
		line = new GuiPinLine(sections, ZRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 365));
		points.add(new Point(702, 365));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(72, 692));
		points.add(new Point(83, 692));
		sections.add(points);
		line = new GuiPinLine(sections, CRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 418));
		points.add(new Point(703, 418));
		sections.add(points);
		line = new GuiPinLine(sections, CRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 510));
		points.add(new Point(703, 510));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(72, 674));
		points.add(new Point(84, 674));
		sections.add(points);
		line = new GuiPinLine(sections, VRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 563));
		points.add(new Point(702, 563));
		sections.add(points);
		line = new GuiPinLine(sections, VRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 658));
		points.add(new Point(703, 658));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(73, 656));
		points.add(new Point(84, 656));
		sections.add(points);
		line = new GuiPinLine(sections, L0RSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(694, 710));
		points.add(new Point(703, 710));
		sections.add(points);
		line = new GuiPinLine(sections, L0RSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(694, 803));
		points.add(new Point(702, 803));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(73, 638));
		points.add(new Point(83, 638));
		sections.add(points);
		line = new GuiPinLine(sections, L1RSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(695, 856));
		points.add(new Point(701, 856));
		sections.add(points);
		line = new GuiPinLine(sections, L1RSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(696, 944));
		points.add(new Point(704, 944));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(73, 620));
		points.add(new Point(83, 620));
		sections.add(points);
		line = new GuiPinLine(sections, L2RSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(696, 996));
		points.add(new Point(704, 996));
		sections.add(points);
		line = new GuiPinLine(sections, L2RSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(76, 903));
		points.add(new Point(88, 903));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(73, 566));
		points.add(new Point(85, 566));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(73, 602));
		points.add(new Point(84, 602));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(76, 941));
		points.add(new Point(87, 941));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(632, 971));
		points.add(new Point(640, 971));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(630, 829));
		points.add(new Point(639, 829));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(629, 684));
		points.add(new Point(638, 684));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(631, 537));
		points.add(new Point(638, 537));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(638, 391));
		points.add(new Point(629, 391));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(630, 244));
		points.add(new Point(638, 244));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(630, 101));
		points.add(new Point(639, 101));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(229, 101));
		points.add(new Point(238, 101));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(231, 244));
		points.add(new Point(237, 244));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(631, 944));
		points.add(new Point(639, 944));
		sections.add(points);
		line = new GuiPinLine(sections, L2OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 997));
		points.add(new Point(639, 997));
		sections.add(points);
		line = new GuiPinLine(sections, L2OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 802));
		points.add(new Point(638, 802));
		sections.add(points);
		line = new GuiPinLine(sections, L1OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(631, 855));
		points.add(new Point(639, 855));
		sections.add(points);
		line = new GuiPinLine(sections, L1OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 657));
		points.add(new Point(639, 657));
		sections.add(points);
		line = new GuiPinLine(sections, L0OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 510));
		points.add(new Point(638, 510));
		sections.add(points);
		line = new GuiPinLine(sections, VOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(631, 563));
		points.add(new Point(637, 563));
		sections.add(points);
		line = new GuiPinLine(sections, VOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 710));
		points.add(new Point(638, 710));
		sections.add(points);
		line = new GuiPinLine(sections, L0OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(631, 365));
		points.add(new Point(638, 365));
		sections.add(points);
		line = new GuiPinLine(sections, COR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 417));
		points.add(new Point(638, 417));
		sections.add(points);
		line = new GuiPinLine(sections, COR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(631, 218));
		points.add(new Point(638, 218));
		sections.add(points);
		line = new GuiPinLine(sections, ZOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 271));
		points.add(new Point(638, 271));
		sections.add(points);
		line = new GuiPinLine(sections, ZOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(630, 74));
		points.add(new Point(638, 74));
		sections.add(points);
		line = new GuiPinLine(sections, NOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(631, 127));
		points.add(new Point(637, 127));
		sections.add(points);
		line = new GuiPinLine(sections, NOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(230, 218));
		points.add(new Point(239, 218));
		sections.add(points);
		line = new GuiPinLine(sections, TOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(231, 270));
		points.add(new Point(238, 270));
		sections.add(points);
		line = new GuiPinLine(sections, TOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(230, 74));
		points.add(new Point(238, 74));
		sections.add(points);
		line = new GuiPinLine(sections, IOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(231, 127));
		points.add(new Point(237, 127));
		sections.add(points);
		line = new GuiPinLine(sections, IOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 69));
		points.add(new Point(211, 69));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stPSWI"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 212));
		points.add(new Point(209, 212));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stPSWT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 133));
		points.add(new Point(209, 133));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPSWI"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(201, 276));
		points.add(new Point(210, 276));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPSWT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(147, 80));
		points.add(new Point(209, 80));
		sections.add(points);
		line = new GuiPinLine(sections, IAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(147, 121));
		points.add(new Point(210, 121));
		sections.add(points);
		line = new GuiPinLine(sections, IAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(147, 223));
		points.add(new Point(208, 223));
		sections.add(points);
		line = new GuiPinLine(sections, TAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(209, 265));
		points.add(new Point(147, 265));
		sections.add(points);
		line = new GuiPinLine(sections, TAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 64));
		points.add(new Point(600, 64));
		points.add(new Point(600, 68));
		points.add(new Point(609, 68));
		sections.add(points);
		line = new GuiPinLine(sections, NAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 80));
		points.add(new Point(608, 80));
		sections.add(points);
		line = new GuiPinLine(sections, NAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 121));
		points.add(new Point(609, 121));
		sections.add(points);
		line = new GuiPinLine(sections, NAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 137));
		points.add(new Point(600, 137));
		points.add(new Point(600, 133));
		points.add(new Point(609, 133));
		sections.add(points);
		line = new GuiPinLine(sections, NAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 355));
		points.add(new Point(600, 355));
		points.add(new Point(600, 359));
		points.add(new Point(609, 359));
		sections.add(points);
		line = new GuiPinLine(sections, CAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 370));
		points.add(new Point(609, 370));
		sections.add(points);
		line = new GuiPinLine(sections, CAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 412));
		points.add(new Point(609, 412));
		sections.add(points);
		line = new GuiPinLine(sections, CAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 427));
		points.add(new Point(600, 427));
		points.add(new Point(600, 423));
		points.add(new Point(609, 423));
		sections.add(points);
		line = new GuiPinLine(sections, CAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 208));
		points.add(new Point(600, 208));
		points.add(new Point(600, 212));
		points.add(new Point(609, 212));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(539, 223));
		points.add(new Point(609, 223));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 265));
		points.add(new Point(610, 265));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 280));
		points.add(new Point(600, 280));
		points.add(new Point(600, 276));
		points.add(new Point(610, 276));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 500));
		points.add(new Point(600, 500));
		points.add(new Point(600, 504));
		points.add(new Point(609, 504));
		sections.add(points);
		line = new GuiPinLine(sections, VAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 516));
		points.add(new Point(609, 516));
		sections.add(points);
		line = new GuiPinLine(sections, VAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(539, 557));
		points.add(new Point(610, 557));
		sections.add(points);
		line = new GuiPinLine(sections, VAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 572));
		points.add(new Point(600, 572));
		points.add(new Point(600, 568));
		points.add(new Point(609, 568));
		sections.add(points);
		line = new GuiPinLine(sections, VAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 647));
		points.add(new Point(600, 647));
		points.add(new Point(600, 651));
		points.add(new Point(608, 651));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 662));
		points.add(new Point(609, 662));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 704));
		points.add(new Point(609, 704));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 719));
		points.add(new Point(600, 719));
		points.add(new Point(600, 715));
		points.add(new Point(610, 715));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 793));
		points.add(new Point(600, 793));
		points.add(new Point(600, 796));
		points.add(new Point(609, 796));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 808));
		points.add(new Point(609, 808));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(538, 850));
		points.add(new Point(610, 850));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 865));
		points.add(new Point(600, 865));
		points.add(new Point(600, 861));
		points.add(new Point(609, 861));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(594, 934));
		points.add(new Point(601, 934));
		points.add(new Point(601, 938));
		points.add(new Point(609, 938));
		sections.add(points);
		line = new GuiPinLine(sections, L2AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(540, 950));
		points.add(new Point(609, 950));
		sections.add(points);
		line = new GuiPinLine(sections, L2AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(539, 991));
		points.add(new Point(611, 991));
		sections.add(points);
		points = new ArrayList<Point>();
		sections.add(points);
		line = new GuiPinLine(sections, L2AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 1007));
		points.add(new Point(601, 1007));
		points.add(new Point(601, 1002));
		points.add(new Point(611, 1002));
		sections.add(points);
		line = new GuiPinLine(sections, L2AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(170, 902));
		points.add(new Point(181, 902));
		sections.add(points);
		line = new GuiPinLine(sections, STARTRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(168, 979));
		points.add(new Point(180, 979));
		sections.add(points);
		line = new GuiPinLine(sections, STARTRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 970));
		points.add(new Point(554, 970));
		points.add(new Point(554, 940));
		points.add(new Point(570, 940));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(554, 969));
		points.add(new Point(554, 1012));
		points.add(new Point(567, 1012));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 829));
		points.add(new Point(553, 829));
		points.add(new Point(553, 798));
		points.add(new Point(569, 798));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(553, 829));
		points.add(new Point(553, 871));
		points.add(new Point(565, 871));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(480, 683));
		points.add(new Point(553, 683));
		points.add(new Point(553, 653));
		points.add(new Point(553, 682));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(553, 653));
		points.add(new Point(570, 653));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(553, 683));
		points.add(new Point(553, 725));
		points.add(new Point(565, 725));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 536));
		points.add(new Point(553, 536));
		points.add(new Point(553, 506));
		points.add(new Point(570, 506));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(553, 536));
		points.add(new Point(553, 578));
		points.add(new Point(565, 578));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.V"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 391));
		points.add(new Point(553, 391));
		points.add(new Point(553, 361));
		points.add(new Point(569, 361));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(553, 390));
		points.add(new Point(553, 433));
		points.add(new Point(565, 433));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.C"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 244));
		points.add(new Point(553, 244));
		points.add(new Point(553, 214));
		points.add(new Point(570, 214));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(553, 243));
		points.add(new Point(553, 286));
		points.add(new Point(566, 286));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.Z"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(480, 101));
		points.add(new Point(553, 101));
		points.add(new Point(553, 70));
		points.add(new Point(570, 70));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(553, 101));
		points.add(new Point(553, 142));
		points.add(new Point(566, 142));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.N"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 59));
		points.add(new Point(562, 59));
		points.add(new Point(562, 131));
		points.add(new Point(570, 131));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(570, 59));
		points.add(new Point(561, 59));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldN"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 202));
		points.add(new Point(562, 202));
		points.add(new Point(562, 274));
		points.add(new Point(571, 274));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(561, 202));
		points.add(new Point(571, 202));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldZ"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 349));
		points.add(new Point(562, 349));
		points.add(new Point(562, 422));
		points.add(new Point(570, 422));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(562, 349));
		points.add(new Point(570, 349));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 495));
		points.add(new Point(562, 495));
		points.add(new Point(562, 567));
		points.add(new Point(570, 567));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(570, 495));
		points.add(new Point(561, 495));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldV"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 642));
		points.add(new Point(562, 642));
		points.add(new Point(562, 1001));
		points.add(new Point(571, 1001));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(562, 642));
		points.add(new Point(571, 642));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(562, 714));
		points.add(new Point(570, 714));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(563, 787));
		points.add(new Point(571, 787));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(563, 859));
		points.add(new Point(570, 859));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(562, 928));
		points.add(new Point(571, 928));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(115, 35));
		points.add(new Point(115, 259));
		points.add(new Point(125, 259));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(115, 217));
		points.add(new Point(124, 217));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(125, 115));
		points.add(new Point(115, 115));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(116, 74));
		points.add(new Point(125, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 37));
		points.add(new Point(506, 985));
		points.add(new Point(516, 985));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(507, 944));
		points.add(new Point(515, 944));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 844));
		points.add(new Point(515, 844));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(508, 802));
		points.add(new Point(515, 802));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(507, 698));
		points.add(new Point(515, 698));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 657));
		points.add(new Point(515, 657));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 551));
		points.add(new Point(515, 551));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(507, 510));
		points.add(new Point(515, 510));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(507, 406));
		points.add(new Point(515, 406));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 364));
		points.add(new Point(515, 364));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 259));
		points.add(new Point(515, 259));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 218));
		points.add(new Point(515, 218));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(508, 74));
		points.add(new Point(515, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(506, 115));
		points.add(new Point(515, 115));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldPSW"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(28, 22));
		points.add(new Point(28, 332));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(389, 308));
		points.add(new Point(28, 308));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(389, 1068));
		points.add(new Point(389, 20));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(370, 383));
		points.add(new Point(370, 1044));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(351, 1018));
		points.add(new Point(351, 406));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(29, 127));
		points.add(new Point(121, 127));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(104, 125));
		points.add(new Point(104, 85));
		points.add(new Point(125, 85));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(31));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(29, 270));
		points.add(new Point(120, 270));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(104, 269));
		points.add(new Point(104, 229));
		points.add(new Point(125, 229));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(30));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(511, 127));
		points.add(new Point(391, 127));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(495, 126));
		points.add(new Point(495, 85));
		points.add(new Point(516, 85));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(510, 270));
		points.add(new Point(389, 270));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(495, 269));
		points.add(new Point(495, 229));
		points.add(new Point(515, 229));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(511, 417));
		points.add(new Point(391, 417));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(515, 376));
		points.add(new Point(495, 376));
		points.add(new Point(495, 417));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(391, 563));
		points.add(new Point(511, 563));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(495, 562));
		points.add(new Point(495, 521));
		points.add(new Point(515, 521));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(389, 710));
		points.add(new Point(510, 710));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(495, 708));
		points.add(new Point(495, 668));
		points.add(new Point(515, 668));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(390, 855));
		points.add(new Point(510, 855));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(495, 855));
		points.add(new Point(495, 814));
		points.add(new Point(515, 814));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(390, 997));
		points.add(new Point(512, 997));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(495, 996));
		points.add(new Point(495, 955));
		points.add(new Point(515, 955));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(133, 629));
		points.add(new Point(197, 629));
		sections.add(points);
		line = new GuiPinLine(sections, PSW.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(208, 642));
		points.add(new Point(208, 634));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.PSWout1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(218, 629));
		points.add(new Point(389, 629));
		sections.add(points);
		line = new GuiPinLine(sections, TSBPSWout1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(75, 979));
		points.add(new Point(87, 979));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clSTART"));
		gui.addLine(line);
		
		gui.addLabel(new GuiPinLabel(38, 40, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(397, 39, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(35, 321, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(398, 382, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(380, 400, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(360, 430, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(121, 660, PSW.getOutPin(0)));

	}

	public void putPins() {
		NameConnector.addPin(componentName, "PSWI", IRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWT", TRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWN", NRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWZ", ZRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWC", CRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWV", VRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWL0", L0RSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWL1", L1RSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWL2", L2RSFF.getOutPin(0));

		NameConnector.addPin(componentName, "START", STARTRSFF.getOutPin(0));

	}

	public void putComponents() {
	}

}
