package shemes.components;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.AND;
import logic.components.BUS;
import logic.components.CLK;
import logic.components.MP;
import logic.components.NOT;
import logic.components.OR;
import logic.components.REG;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.NameConnector;

public class Bus1 extends AbstractSchema {
	
	private BUS IBUS1, IBUS2, IBUS3;
	private BUS ABUS, DBUS;
	private BUS NOTFCBUS;
	private BUS MNOTIOBUS;
	private TSB TSBMDRout1, TSBEA, TSBED, TSBNOTFC, TSBMNOTIO;
	private MP MX1;
	private AND AND1;
	private OR OROBI, OREA, ORMDR, ORMNOTIO;
	private REG MAR, MDR;
	private NOT NOTFC;
	private TSB TSBMOST1_2, TSBMOST2_1, TSBMOST1_3, TSBMOST3_2, TSBMOST2_3;
	
	
	public Bus1(){
		componentName = "Bus1";
		displayName = "Bus 1";
		NameConnector.addSchema(componentName, this);
	}
	
	@Override
	public void initComponent() {
		IBUS1 = new BUS("IBUS1");
		IBUS1.getOutPin(0).setNumOfLines(32);
		IBUS2 = new BUS("IBUS2");
		IBUS2.getOutPin(0).setNumOfLines(32);
		IBUS3 = new BUS("IBUS3");
		IBUS3.getOutPin(0).setNumOfLines(32);
		
		ABUS = new BUS("ABUS");
		ABUS.getOutPin(0).setNumOfLines(32);
		
		DBUS = new BUS("DBUS");
		DBUS.getOutPin(0).setNumOfLines(32);
		
		NOTFCBUS = new BUS("NOTFCBUS");
		NOTFCBUS.getOutPin(0).setIsBool();
		
		MNOTIOBUS = new BUS("MNOTIOBUS");
		MNOTIOBUS.getOutPin(0).setIsBool();
		
		TSBMOST1_2 = new TSB("MOST1_2");
		TSBMOST1_2.getOutPin(0).setNumOfLines(16);

		TSBMOST2_1 = new TSB("MOST2_1");
		TSBMOST2_1.getOutPin(0).setNumOfLines(16);

		TSBMOST1_3 = new TSB("MOST1_3");
		TSBMOST1_3.getOutPin(0).setNumOfLines(16);

		TSBMOST3_2 = new TSB("MOST3_2");
		TSBMOST3_2.getOutPin(0).setNumOfLines(16);
		
		TSBMOST2_3 = new TSB("MOST2_3");
		TSBMOST2_3.getOutPin(0).setNumOfLines(32);
		
		TSBMDRout1 = new TSB("MDRout1");
		TSBMDRout1.getOutPin(0).setNumOfLines(32);
		
		TSBEA = new TSB("TSBEA");
		TSBEA.getOutPin(0).setNumOfLines(32);
		
		TSBED = new TSB("TSBED");
		TSBED.getOutPin(0).setNumOfLines(32);
		
		MX1 = new MP(2);
		MX1.getOutPin(0).setIsInt();
		MX1.getOutPin(0).setNumOfLines(32);
		
		AND1 = new AND();
		
		OROBI = new OR(2, "OR_OBI");
		OREA = new OR(2, "OR_EA");
		ORMDR = new OR();
		ORMNOTIO = new OR();
		
		MAR = new REG(1, "MAR");
		MAR.getOutPin(0).setIsInt();
		MAR.getOutPin(0).setNumOfLines(32);
		
		MDR = new REG(1, "MDR");
		MDR.getOutPin(0).setIsInt();
		MDR.getOutPin(0).setNumOfLines(32);
		
		TSBNOTFC = new TSB("NOT_FCBUS");
		TSBNOTFC.getOutPin(0).setNumOfLines(1);
		TSBNOTFC.getOutPin(0).setIsBool();
		
		NOTFC = new NOT();
		
		TSBMNOTIO = new TSB("MNOTIO");
		TSBMNOTIO.getOutPin(0).setNumOfLines(1);
		TSBMNOTIO.getOutPin(0).setIsBool();
		
		
		putPins();
		putComponents();
		
	}
	
	@Override
	public void initConections() {
		TSBMOST1_2.setInPin(0, IBUS1.getOutPin(0));
		TSBMOST1_2.setE(NameConnector.getPin("Oper1.MOST1_2"));
		this.addOnIBUS2(TSBMOST1_2.getOutPin(0));

		TSBMOST2_1.setInPin(0, IBUS2.getOutPin(0));
		TSBMOST2_1.setE(NameConnector.getPin("Oper1.MOST2_1"));
		this.addOnIBUS1(TSBMOST2_1.getOutPin(0));

		TSBMOST1_3.setInPin(0, IBUS1.getOutPin(0));
		TSBMOST1_3.setE(NameConnector.getPin("Oper1.MOST1_3"));
		this.addOnIBUS3(TSBMOST1_3.getOutPin(0));

		TSBMOST3_2.setInPin(0, IBUS3.getOutPin(0));
		TSBMOST3_2.setE(NameConnector.getPin("Oper1.MOST3_2"));
		this.addOnIBUS2(TSBMOST3_2.getOutPin(0));
		
		TSBMOST2_3.setInPin(0, IBUS2.getOutPin(0));
		TSBMOST2_3.setE(NameConnector.getPin("Oper1.MOST2_3"));
		this.addOnIBUS3(TSBMOST2_3.getOutPin(0));
		
		
		MX1.setInPin(0, DBUS.getOutPin(0));
		MX1.setInPin(1, IBUS1.getOutPin(0));
		MX1.setCtrl(0, NameConnector.getPin("Oper1.ldMDR"));
		
		TSBNOTFC.setInPin(0, NOTFCBUS.getOutPin(0));
		NOTFC.setInPin(0, TSBNOTFC.getOutPin(0));
		
		OROBI.setInPin(0, NameConnector.getPin("Bus2.RDF"));	//OBI OR
		OROBI.setInPin(1, NameConnector.getPin("Bus2.WRF"));
		
		OREA.setInPin(0, NameConnector.getPin("Bus2.ERDA"));	//EA OR
		OREA.setInPin(1, NameConnector.getPin("Bus2.EWRAD"));
		
		AND1.setInPin(0, NameConnector.getPin("Bus2.RDF"));		//MDR AND
		AND1.setInPin(1, NOTFC.getOutPin(0));
		
		ORMDR.setInPin(0, NameConnector.getPin("Oper1.ldMDR"));	//MDR OR
		ORMDR.setInPin(1, AND1.getOutPin(0));
		
		
		MDR.setInPin(0, MX1.getOutPin(0));	//MDR
		MDR.setPinLd(ORMDR.getOutPin(0));
		MDR.setClk((CLK)NameConnector.getComponent("CLK"));
		
		TSBMDRout1.setInPin(0, MDR.getOutPin(0));	//MDRout3
		TSBMDRout1.setE(NameConnector.getPin("Oper1.MDRout1"));
		this.addOnIBUS1(TSBMDRout1.getOutPin(0));
		
		TSBED.setInPin(0, MDR.getOutPin(0));	//ED
		TSBED.setE(NameConnector.getPin("Bus2.EWRAD"));
		this.addOnDBUS(TSBED.getOutPin(0));
		
		MAR.setInPin(0, IBUS3.getOutPin(0));	//MAR
		MAR.setPinLd(NameConnector.getPin("Oper1.ldMAR"));
		MAR.setPinInc(NameConnector.getPin("Oper1.incMAR"));
		MAR.setClk((CLK)NameConnector.getComponent("CLK"));
		
		TSBEA.setInPin(0, MAR.getOutPin(0));	//EA
		TSBEA.setE(OREA.getOutPin(0));
		this.addOnABUS(TSBEA.getOutPin(0));
		
		
		
		ORMNOTIO.setInPin(0, NameConnector.getPin("Bus2.ERDA"));
		ORMNOTIO.setInPin(1, NameConnector.getPin("Bus2.EWRAD"));
		
		TSBMNOTIO.setInPin(0, NameConnector.getPin("Bus2.MNOTIO"));
		TSBMNOTIO.setE(ORMNOTIO.getOutPin(0));
		this.addOnMNOTIOBUS(TSBMNOTIO.getOutPin(0));
		
		
	}
	
	
	
	@Override
	public void initGui() {
		gui = new GuiSchema("src/images/Bus1.png");
		
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(677, 14));
		points.add(new Point(677, 802));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(677, 182));
		points.add(new Point(503, 182));
		points.add(new Point(503, 200));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(677, 624));
		points.add(new Point(509, 624));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(676, 681));
		points.add(new Point(629, 681));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(660, 33));
		points.add(new Point(660, 663));
		points.add(new Point(561, 663));
		points.add(new Point(561, 802));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(561, 672));
		points.add(new Point(509, 672));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(562, 733));
		points.add(new Point(606, 733));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(643, 51));
		points.add(new Point(643, 604));
		points.add(new Point(438, 604));
		points.add(new Point(438, 803));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(643, 103));
		points.add(new Point(161, 103));
		points.add(new Point(161, 268));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(439, 724));
		points.add(new Point(484, 724));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(486, 624));
		points.add(new Point(438, 624));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST1_3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(486, 672));
		points.add(new Point(439, 672));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST2_3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(507, 724));
		points.add(new Point(561, 724));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST3_2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(608, 681));
		points.add(new Point(563, 681));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST1_2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(628, 733));
		points.add(new Point(676, 733));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST2_1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(466, 182));
		points.add(new Point(466, 200));
		sections.add(points);
		line = new GuiPinLine(sections, DBUS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 219));
		points.add(new Point(446, 219));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(372, 220));
		points.add(new Point(372, 283));
		points.add(new Point(378, 283));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldMDR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(366, 294));
		points.add(new Point(378, 294));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(398, 288));
		points.add(new Point(409, 288));
		sections.add(points);
		line = new GuiPinLine(sections, ORMDR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(484, 239));
		points.add(new Point(484, 269));
		sections.add(points);
		line = new GuiPinLine(sections, MX1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(340, 289));
		points.add(new Point(347, 289));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(331, 525));
		points.add(new Point(339, 525));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.RDF"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 538));
		points.add(new Point(339, 538));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.WRF"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 531));
		points.add(new Point(368, 531));
		sections.add(points);
		line = new GuiPinLine(sections, OROBI.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 534));
		points.add(new Point(185, 534));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(340, 298));
		points.add(new Point(346, 298));
		sections.add(points);
		line = new GuiPinLine(sections, NOTFC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(484, 308));
		points.add(new Point(484, 368));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(485, 354));
		points.add(new Point(523, 354));
		sections.add(points);
		line = new GuiPinLine(sections, MDR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(546, 354));
		points.add(new Point(676, 354));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMDRout1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(484, 392));
		points.add(new Point(484, 417));
		sections.add(points);
		line = new GuiPinLine(sections, TSBED.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(454, 378));
		points.add(new Point(478, 378));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.EWRAD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(533, 371));
		points.add(new Point(533, 361));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.MDRout1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(140, 534));
		points.add(new Point(151, 534));
		sections.add(points);
		line = new GuiPinLine(sections, NOTFCBUS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(161, 307));
		points.add(new Point(161, 368));
		sections.add(points);
		line = new GuiPinLine(sections, MAR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(161, 391));
		points.add(new Point(161, 412));
		sections.add(points);
		line = new GuiPinLine(sections, TSBEA.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(126, 379));
		points.add(new Point(156, 379));
		sections.add(points);
		line = new GuiPinLine(sections, OREA.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(100, 373));
		points.add(new Point(107, 373));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.ERDA"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(100, 384));
		points.add(new Point(106, 384));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.EWRAD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(560, 288));
		points.add(new Point(567, 288));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(237, 287));
		points.add(new Point(244, 287));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 278));
		points.add(new Point(84, 278));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldMAR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(78, 297));
		points.add(new Point(85, 297));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.incMAR"));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(197, 627));
		points.add(new Point(202, 627));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.ERDA"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(196, 637));
		points.add(new Point(203, 637));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.EWRAD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(164, 611));
		points.add(new Point(164, 632));
		points.add(new Point(177, 632));
		sections.add(points);
		line = new GuiPinLine(sections, ORMNOTIO.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(177, 606));
		points.add(new Point(195, 606));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.MNOTIO"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(141, 605));
		points.add(new Point(153, 605));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMNOTIO.getOutPin(0));
		gui.addLine(line);



		gui.addLabel(new GuiPinLabel(168, 338, MAR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(492, 340, MDR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(494, 253, MX1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(685, 30, IBUS1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(667, 50, IBUS2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(650, 70, IBUS3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(168, 253, IBUS3.getOutPin(0)));

	}
	
	public void addOnIBUS1(Pin pin) {
		IBUS1.setInPin(0, pin);
	}

	public void addOnIBUS2(Pin pin) {
		IBUS2.setInPin(0, pin);
	}

	public void addOnIBUS3(Pin pin) {
		IBUS3.setInPin(0, pin);
	}

	public void addOnDBUS(Pin pin) {
		DBUS.setInPin(0, pin);
	}

	public void addOnABUS(Pin pin) {
		ABUS.setInPin(0, pin);
	}
	
	public void addOnNOTFCBUS(Pin pin) {
		NOTFCBUS.setInPin(0, pin);
	}
	
	public void addOnMNOTIOBUS(Pin pin) {
		MNOTIOBUS.setInPin(0, pin);
	}
	
	private void putPins() {
		NameConnector.addPin(componentName, "IBUS1", IBUS1.getOutPin(0));

		NameConnector.addPin(componentName, "IBUS2", IBUS2.getOutPin(0));

		NameConnector.addPin(componentName, "IBUS3", IBUS3.getOutPin(0));

		NameConnector.addPin(componentName, "ABUS", ABUS.getOutPin(0));

		NameConnector.addPin(componentName, "DBUS", DBUS.getOutPin(0));
		
		NameConnector.addPin(componentName, "NOTFCBUS", NOTFCBUS.getOutPin(0));
		
		NameConnector.addPin(componentName, "MNOTIOBUS", MNOTIOBUS.getOutPin(0));
		
		NameConnector.addPin(componentName, "MDR", MDR.getOutPin(0));

		NameConnector.addPin(componentName, "MAR", MAR.getOutPin(0));

		NameConnector.addPin(componentName, "MX1", MX1.getOutPin(0));
		
		NameConnector.addPin(componentName, "TSBMDRoutToIBUS",
				TSBMDRout1.getOutPin(0));
		
		NameConnector.addPin(componentName, "FC", NOTFC.getOutPin(0));
		
		NameConnector.addPin(componentName, "OBI", OROBI.getOutPin(0));
	}
	
	private void putComponents() {
		NameConnector.addComponent(componentName, "MAR", MAR);
		NameConnector.addComponent(componentName, "MDR", MDR);
	}

	

	

}
