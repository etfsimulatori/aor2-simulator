package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.AND;
import logic.components.BUS;
import logic.components.CLK;
import logic.components.NOT;
import logic.components.OR;
import logic.components.RSFF;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.NameConnector;

public class Bus2 extends AbstractSchema {
	
	private BUS RDBUS, WRBUS, BUSYBUS;
	private RSFF RSFFRD, RSFFWR, RSFFBUSBUSY, RSFFMNOTIO;
	private AND AND1, AND2, AND3, AND4, AND5;
	private OR ORBRRD, ORBRWR, ORBR;
	private TSB TSBRDBUS, TSBWRBUS, TSBBUSBUSY, TSBSETBUSY;
	private NOT NOTRDBUS, NOTWRBUS, NOTBUSBUSY, NOTBR, NOT1, NOT2;
	private Pin logicalOne = new Pin(true, "1");
	
	public Bus2(){
		componentName = "Bus2";
		displayName = "Bus 2";
		NameConnector.addSchema(componentName, this);
	}
	
	@Override
	public void initComponent() {
		
		RDBUS = new BUS("RDBUS");
		RDBUS.getOutPin(0).setIsBool();
		WRBUS = new BUS("WRBUS");
		RDBUS.getOutPin(0).setIsBool();
		BUSYBUS = new BUS("BUSBUSY");
		BUSYBUS.getOutPin(0).setIsBool();
		
		RSFFRD = new RSFF("RSFFRD");
		RSFFWR = new RSFF("RSFFWR");
		RSFFBUSBUSY = new RSFF("RSFFBUSBUSY");
		RSFFMNOTIO = new RSFF("RSFFMNOTIO");
		
		AND1 = new AND();
		AND2 = new AND();
		AND3 = new AND();
		AND4 = new AND();
		AND5 = new AND();
		
		ORBRRD = new OR();
		ORBRWR = new OR();
		ORBR = new OR();
		
		TSBRDBUS = new TSB("TSBRDBUS");
		TSBRDBUS.getOutPin(0).setNumOfLines(1);
		TSBRDBUS.getOutPin(0).setIsBool();
		TSBWRBUS = new TSB("TSBWRBUS");
		TSBWRBUS.getOutPin(0).setNumOfLines(1);
		TSBWRBUS.getOutPin(0).setIsBool();
		TSBBUSBUSY = new TSB("TSBBUSBUSY");
		TSBBUSBUSY.getOutPin(0).setNumOfLines(1);
		TSBBUSBUSY.getOutPin(0).setIsBool();
		TSBSETBUSY = new TSB("TSBSETBUSY");
		TSBSETBUSY.getOutPin(0).setNumOfLines(1);
		TSBSETBUSY.getOutPin(0).setIsBool();
		
		NOTRDBUS = new NOT();
		NOTWRBUS = new NOT();
		NOTBUSBUSY = new NOT();
		NOTBR = new NOT();
		NOT1 = new NOT();
		NOT2 = new NOT();
		
		putPins();
		putComponents();
	}
	
	

	

	@Override
	public void initConections() {
		//RDBUS
		RSFFRD.setPinS(NameConnector.getPin("Oper1.RD"));
		RSFFRD.setPinR(NameConnector.getPin("Bus1.FC"));
		RSFFRD.setClk((CLK)NameConnector.getComponent("CLK"));
		
		ORBRRD.setInPin(0, NameConnector.getPin("Oper1.RD"));
		ORBRRD.setInPin(1, RSFFRD.getOutPin(0));
		
		AND1.setInPin(0, ORBRRD.getOutPin(0));
		AND1.setInPin(1, RSFFBUSBUSY.getOutPin(0));
		
		TSBRDBUS.setInPin(0, AND1.getOutPin(0));
		TSBRDBUS.setE(AND1.getOutPin(0));
		
		NOTRDBUS.setInPin(0, TSBRDBUS.getOutPin(0));
		this.addOnNOTRDBUS(NOTRDBUS.getOutPin(0));
		
		//WRBUS
		
		RSFFWR.setPinS(NameConnector.getPin("Oper1.WR"));
		RSFFWR.setPinR(NameConnector.getPin("Bus1.FC"));
		RSFFWR.setClk((CLK)NameConnector.getComponent("CLK"));
		
		ORBRWR.setInPin(0, NameConnector.getPin("Oper1.WR"));
		ORBRWR.setInPin(1, RSFFWR.getOutPin(0));
		
		AND2.setInPin(0, ORBRWR.getOutPin(0));
		AND2.setInPin(1, RSFFBUSBUSY.getOutPin(0));
		
		TSBWRBUS.setInPin(0, AND2.getOutPin(0));
		TSBWRBUS.setE(AND2.getOutPin(0));
		
		NOTWRBUS.setInPin(0, TSBWRBUS.getOutPin(0));
		this.addOnNOTWRBUS(NOTWRBUS.getOutPin(0));
		
		//BUSYBUS
		ORBR.setInPin(0, ORBRRD.getOutPin(0));
		ORBR.setInPin(1, ORBRWR.getOutPin(0));
		
		NOTBR.setInPin(0, ORBR.getOutPin(0));
		
		AND3.setInPin(0, ORBR.getOutPin(0));
		AND3.setInPin(1, NameConnector.getPin("Arb.BG_IN_7"));
		
		AND4.setInPin(0, NOTBR.getOutPin(0));
		AND4.setInPin(1, NameConnector.getPin("Arb.BG_IN_7"));
		
		AND5.setInPin(0, AND3.getOutPin(0));
		AND5.setInPin(1, NOT2.getOutPin(0));
		
		RSFFBUSBUSY.setPinS(AND5.getOutPin(0));
		RSFFBUSBUSY.setPinR(NameConnector.getPin("Bus1.FC"));
		RSFFBUSBUSY.setClk((CLK)NameConnector.getComponent("CLK"));
		
		TSBBUSBUSY.setInPin(0, logicalOne);
		TSBBUSBUSY.setE(RSFFBUSBUSY.getOutPin(0));
		
		NOTBUSBUSY.setInPin(0, TSBBUSBUSY.getOutPin(0));
		this.addOnNOTBUSYBUS(NOTBUSBUSY.getOutPin(0));
		
		TSBSETBUSY.setInPin(0, BUSYBUS.getOutPin(0));
		NOT1.setInPin(0, TSBSETBUSY.getOutPin(0));
		NOT2.setInPin(0, NOT1.getOutPin(0));
		
		
		//M/IO
		
		RSFFMNOTIO.setPinS(NameConnector.getPin("Oper1.stIO"));
		RSFFMNOTIO.setPinR(NameConnector.getPin("Bus1.FC"));
		RSFFMNOTIO.setClk((CLK)NameConnector.getComponent("CLK"));
	}
	
	@Override
	public void initGui() {
		gui = new GuiSchema("src/images/Bus2.png");
		
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(72, 84));
		points.add(new Point(104, 84));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(88, 85));
		points.add(new Point(88, 46));
		points.add(new Point(225, 46));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.RD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(179, 84));
		points.add(new Point(218, 84));
		points.add(new Point(218, 58));
		points.add(new Point(225, 58));
		sections.add(points);
		line = new GuiPinLine(sections, RSFFRD.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(247, 53));
		points.add(new Point(298, 53));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(45, 523));
		points.add(new Point(53, 523));
		sections.add(points);
		line = new GuiPinLine(sections, ORBRRD.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(323, 58));
		points.add(new Point(452, 58));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(341, 59));
		points.add(new Point(341, 11));
		points.add(new Point(369, 11));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(436, 58));
		points.add(new Point(436, 79));
		points.add(new Point(464, 79));
		points.add(new Point(464, 65));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(480, 59));
		points.add(new Point(482, 59));
		sections.add(points);
		line = new GuiPinLine(sections, NOTRDBUS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 51));
		points.add(new Point(482, 65));
		points.add(new Point(482, 69));
		sections.add(points);
		line = new GuiPinLine(sections, RDBUS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(76, 294));
		points.add(new Point(111, 294));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(94, 293));
		points.add(new Point(94, 257));
		points.add(new Point(231, 257));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.WR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(184, 294));
		points.add(new Point(224, 294));
		points.add(new Point(224, 268));
		points.add(new Point(231, 268));
		sections.add(points);
		line = new GuiPinLine(sections, RSFFWR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(253, 263));
		points.add(new Point(305, 263));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(45, 535));
		points.add(new Point(52, 535));
		sections.add(points);
		line = new GuiPinLine(sections, ORBRWR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(329, 269));
		points.add(new Point(447, 269));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(347, 269));
		points.add(new Point(347, 222));
		points.add(new Point(374, 222));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(430, 270));
		points.add(new Point(430, 289));
		points.add(new Point(458, 289));
		points.add(new Point(458, 275));
		sections.add(points);
		line = new GuiPinLine(sections, AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(473, 269));
		points.add(new Point(476, 269));
		sections.add(points);
		line = new GuiPinLine(sections, NOTWRBUS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 263));
		points.add(new Point(476, 279));
		sections.add(points);
		line = new GuiPinLine(sections, WRBUS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(96, 118));
		points.add(new Point(104, 118));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(102, 328));
		points.add(new Point(110, 328));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(319, 566));
		points.add(new Point(329, 566));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(96, 152));
		points.add(new Point(104, 152));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(102, 362));
		points.add(new Point(110, 362));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(318, 602));
		points.add(new Point(329, 602));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.FC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(75, 529));
		points.add(new Point(172, 529));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(93, 530));
		points.add(new Point(93, 632));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(94, 587));
		points.add(new Point(111, 587));
		sections.add(points);
		line = new GuiPinLine(sections, ORBR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(161, 632));
		points.add(new Point(161, 540));
		points.add(new Point(171, 540));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(162, 599));
		points.add(new Point(172, 599));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Arb.BG_IN_7"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(139, 587));
		points.add(new Point(171, 587));
		sections.add(points);
		line = new GuiPinLine(sections, NOTBR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 593));
		points.add(new Point(225, 593));
		points.add(new Point(225, 632));
		sections.add(points);
		line = new GuiPinLine(sections, AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 534));
		points.add(new Point(240, 534));
		sections.add(points);
		line = new GuiPinLine(sections, AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(239, 523));
		points.add(new Point(225, 523));
		points.add(new Point(225, 454));
		points.add(new Point(319, 454));
		sections.add(points);
		line = new GuiPinLine(sections, NOT2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(263, 529));
		points.add(new Point(329, 529));
		sections.add(points);
		line = new GuiPinLine(sections, AND5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(408, 530));
		points.add(new Point(458, 530));
		points.add(new Point(458, 480));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(298, 275));
		points.add(new Point(304, 275));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(292, 64));
		points.add(new Point(299, 64));
		sections.add(points);
		line = new GuiPinLine(sections, RSFFBUSBUSY.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(408, 602));
		points.add(new Point(418, 602));
		sections.add(points);
		line = new GuiPinLine(sections, RSFFBUSBUSY.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(179, 152));
		points.add(new Point(188, 152));
		sections.add(points);
		line = new GuiPinLine(sections, RSFFRD.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(185, 362));
		points.add(new Point(194, 362));
		sections.add(points);
		line = new GuiPinLine(sections, RSFFWR.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(437, 474));
		points.add(new Point(446, 474));
		sections.add(points);
		line = new GuiPinLine(sections, logicalOne);
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 474));
		points.add(new Point(485, 474));
		points.add(new Point(485, 454));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(398, 454));
		points.add(new Point(485, 454));
		points.add(new Point(485, 437));
		sections.add(points);
		line = new GuiPinLine(sections, NOTBUSBUSY.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 435));
		points.add(new Point(491, 435));
		sections.add(points);
		line = new GuiPinLine(sections, BUSYBUS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(369, 453));
		points.add(new Point(347, 453));
		sections.add(points);
		line = new GuiPinLine(sections, NOT1.getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(625, 294));
		points.add(new Point(633, 294));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stIO"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(625, 328));
		points.add(new Point(633, 328));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("LogicalComponent.CLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(625, 363));
		points.add(new Point(634, 363));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.FC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(708, 362));
		points.add(new Point(732, 362));
		sections.add(points);
		line = new GuiPinLine(sections, RSFFMNOTIO.getOutPin(1));
		gui.addLine(line);



	}
	
	private void putPins() {
		
		
		NameConnector.addPin(componentName, "NOTRDBUS", RDBUS.getOutPin(0));

		NameConnector.addPin(componentName, "NOTWRBUS", WRBUS.getOutPin(0));

		NameConnector.addPin(componentName, "NOTBUSYBUS", BUSYBUS.getOutPin(0));
		
		NameConnector.addPin(componentName, "BUSYBUSOUT", NOTBUSBUSY.getOutPin(0));
		
		NameConnector.addPin(componentName, "ERDA", AND1.getOutPin(0));
		
		NameConnector.addPin(componentName, "EWRAD", AND2.getOutPin(0));
		
		NameConnector.addPin(componentName, "RDF", RSFFRD.getOutPin(0));
		
		NameConnector.addPin(componentName, "WRF", RSFFWR.getOutPin(0));
		
		NameConnector.addPin(componentName, "BR", ORBR.getOutPin(0));
		
		NameConnector.addPin(componentName, "BG_OUT", AND4.getOutPin(0));
		
		NameConnector.addPin(componentName, "MNOTIO", RSFFMNOTIO.getOutPin(1));
	}
	
	private void putComponents() {
		
	}
	
	public void addOnNOTRDBUS(Pin pin) {
		RDBUS.setInPin(0, pin);
	}

	public void addOnNOTWRBUS(Pin pin) {
		WRBUS.setInPin(0, pin);
	}
	
	public void addOnNOTBUSYBUS(Pin pin){
		BUSYBUS.setInPin(0, pin);
	}
	
	

}
