package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import gui.GuiImageLabel;
import gui.GuiLableLine;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;
import gui.components.DCGui;
import logic.Pin;
import logic.components.AND;
import logic.components.DC;
import logic.components.NOT;
import logic.components.OR;
import shemes.AbstractSchema;
import util.DrawUtils;
import util.NameConnector;
import util.Parameters;
import util.Router;

public class Uprav1 extends AbstractSchema {
	private DC DC7;
	private List<NOT> notList;
	private List<AND> andList;

	private OR OR1;

	public Uprav1() {
		componentName = "Uprav1";
		displayName = "Signali upravljacke jedinice";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		DC7 = new DC(5);
		notList = new ArrayList<NOT>(32);
		andList = new ArrayList<AND>(32);
		for (int i = 0; i < 32; i++) {
			notList.add(i, null);// new NOT());
			andList.add(i, null);// new AND());
		}

		Set<Integer> usedExits = new HashSet<Integer>();
		for (int i = 0; i < Parameters.controlUnitDecoder.length; i++) {
			String[] data = Parameters.controlUnitDecoder[i];
			int index = Integer.parseInt(data[0]);
			if (data[1].length() > 0) {
				usedExits.add(index);
			}
			if (data.length > 3) {
				Pin outPin = DC7.getOutPin(index);
				outPin.setName(data[3]);
			}
		}

		OR1 = new OR(usedExits.size());

		putPins();
		putComponents();
	}

	public void initConections() {
		DC7.setE(NameConnector.getPin("Counter.mCW0"));
		DC7.setInPin(0, NameConnector.getPin("Counter.mCW8"));
		DC7.setInPin(1, NameConnector.getPin("Counter.mCW7"));
		DC7.setInPin(2, NameConnector.getPin("Counter.mCW6"));
		DC7.setInPin(3, NameConnector.getPin("Counter.mCW5"));
		DC7.setInPin(4, NameConnector.getPin("Counter.mCW4"));

		Set<Integer> usedExits = new HashSet<Integer>();
		for (int i = 0; i < Parameters.controlUnitDecoder.length; i++) {
			String[] data = Parameters.controlUnitDecoder[i];
			int index = Integer.parseInt(data[0]);
			if (data[1].length() > 0) {
				if (data[2].equals("#")) {
					Pin pin = NameConnector.getPin(data[1]);
					NOT not = new NOT();// notList.get(index);
					notList.set(index, not);
					not.setInPin(0, pin);
					AND and = new AND();// andList.get(index);
					andList.set(index, and);
					and.setInPin(0, DC7.getOutPin(index));
					and.setInPin(1, not.getOutPin(0));
				} else {
					Pin pin = NameConnector.getPin(data[1]);
					AND and = new AND();// andList.get(index);
					andList.set(index, and);
					and.setInPin(0, DC7.getOutPin(index));
					and.setInPin(1, pin);
				}
				usedExits.add(index);
			}
		}

		for (int i = 0, j = 0; i < DC7.getOutPins().length; i++) {
			if (usedExits.contains(i)) {
				OR1.setInPin(j++, andList.get(i).getOutPin(0));
			}
		}
	}

	public void initGui() {
		gui = new GuiSchema("src/images/Uprav1.png");

		String[] dcArgs = { "DC", "DC", "5", "1", "mCW4", "mCW5", "mCW6",
				"mCW7", "mCW8", "..." };
		DCGui dcGui = new DCGui(DC7, 75, 50, dcArgs, false);
		gui = dcGui.convert(gui);

		drawAll(DC7.getOutPins().length, dcGui);

	}

	public void putPins() {
		for (Pin pin : DC7.getOutPins()) {
			String name = pin.getName();
			if (name != null && name.length() > 0) {
				NameConnector.addPin(componentName, name, pin);
			}
		}
		/*
		 * NameConnector.addPin(componentName, "br", DC7.getOutPin(1));
		 * 
		 * NameConnector.addPin(componentName, "bradr", DC7.getOutPin(22));
		 * 
		 * NameConnector.addPin(componentName, "bropr", DC7.getOutPin(23));
		 */
		NameConnector.addPin(componentName, "branch", OR1.getOutPin(0));
	}

	public void putComponents() {

	}


	protected Point drawOne(int i, Point[] coordinates) {
		GuiPinLine line; // Pomocna promenljiva
		List<List<Point>> sections;
		List<Point> points;

		AND and = andList.get(i);
		NOT not = notList.get(i);
		Pin pin = DC7.getOutPin(i);
		String name = pin.getName();

		if (and != null) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(coordinates[0]);
			points.add(coordinates[1]);
			points.add(coordinates[2]);
			points.add(coordinates[3]);
			points.add(coordinates[4]);
			points.add(coordinates[5]);
			sections.add(points);
			line = new GuiPinLine(sections, pin);
			gui.addLine(line);

			GuiImageLabel im = new GuiImageLabel(coordinates[8].x + 16,
					coordinates[8].y - 1, "src/images/AND.png");
			gui.addLabel(im);

			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(coordinates[9]);
			points.add(coordinates[10]);
			sections.add(points);
			line = new GuiPinLine(sections, and.getOutPin(0));
			gui.addLine(line);

			Pin pin2 = null;
			if (not == null) {
				pin2 = and.getInPin(1);
			} else {
				pin2 = not.getInPin(0);
				GuiImageLabel imInv = new GuiImageLabel(coordinates[7].x - 4,
						coordinates[7].y - 3, "src/images/INV.png");
				gui.addLabel(imInv);
			}
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(coordinates[6]);
			points.add(coordinates[7]);
			sections.add(points);
			line = new GuiPinLine(sections, pin2);
			gui.addLine(line);

			String conditionName = getConditionName(i);
			GuiTextLabel label = new GuiTextLabel(coordinates[6].x - 5, coordinates[6].y,
					conditionName, 12, DrawUtils.ALIGNLEFT);
			gui.addLabel(label);
			return coordinates[10];

		} else if (name != null && !name.equals("")) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(coordinates[0]);
			points.add(coordinates[1]);
			points.add(coordinates[2]);
			points.add(coordinates[3]);
			sections.add(points);
			line = new GuiPinLine(sections, pin);
			gui.addLine(line);

			GuiTextLabel label = new GuiTextLabel(coordinates[3].x + 5,
					coordinates[6].y - 10, name, 12);
			gui.addLabel(label);

		}
		return null;
	}

	private String getConditionName(int id) {
		String result = "";
		for (int i = 0; i < Parameters.controlUnitDecoder.length; i++) {
			String data[] = Parameters.controlUnitDecoder[i];
			if (Integer.parseInt(data[0]) == id) {
				result = data[1];
				break;
			}
		}
		return result;
	}

	protected void drawAll(List<Point[]> coordinates) {
		Point start = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
		Point end = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

		for (int i = 0; i < coordinates.size(); i++) {
			Point p = drawOne(i, coordinates.get(i));
			Router.findMinMax(start, end, p);
		}
		drawOr(start, end);

	}


	protected void drawAll(int n, DCGui dcGui) {
		List<Point> a = new LinkedList<Point>();
		for (int i = 0; i < n; i++) {
			a.add(dcGui.getOutPortPosition(i));
		}
		List<Point[]> coordinates = Router.rout(a, 300, 28);
		drawAll(coordinates);
	}

	protected void drawOr(Point start, Point end) {
		GuiPinLine line; // Pomocna promenljiva
		List<List<Point>> sections;
		List<Point> points;

		OR or = OR1;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(end.x + 23, (end.y + start.y) / 2));
		points.add(new Point(end.x + 63, (end.y + start.y) / 2));
		sections.add(points);
		line = new GuiPinLine(sections, or.getOutPin(0));
		gui.addLine(line);

		GuiImageLabel im = new GuiImageLabel(end.x, (end.y + start.y) / 2 - 10,
				"src/images/OR.png");
		gui.addLabel(im);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(end.x, start.y - 10));
		points.add(new Point(end.x, (end.y + start.y) / 2 - 10));
		sections.add(points);
		line = new GuiLableLine(sections);
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(end.x, (end.y + start.y) / 2 + 10));
		points.add(new Point(end.x, end.y + 10));
		sections.add(points);
		line = new GuiLableLine(sections);
		gui.addLine(line);

		GuiTextLabel label = new GuiTextLabel(end.x + 70,
				(end.y + start.y) / 2, "branch", 12);
		gui.addLabel(label);
	}
}
