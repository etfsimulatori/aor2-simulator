
package shemes.components;

import shemes.AbstractSchema;
import util.NameConnector;

public class Memorija extends AbstractSchema {
	
	public Memorija() {
		componentName = "Memorija";
		displayName = "Memorija";
		NameConnector.addSchema(componentName, this);
		this.addSubScheme(new MemOper());
		this.addSubScheme(new MemUprav());
	}

}