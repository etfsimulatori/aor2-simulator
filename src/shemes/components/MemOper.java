package shemes.components;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Pin;
import logic.components.AND;
import logic.components.NOT;
import logic.components.OR;
import logic.components.TSB;
import logic.components.TSBhZtoLow;
import shemes.AbstractSchema;
import util.NameConnector;

public class MemOper extends AbstractSchema {
	
	private NOT NOT1;
	private AND AND1;
	private AND AND2;
	private NOT NOT2;
	private NOT NOT3;
	private AND ANDMEM;
	
	private TSB TSBMEMout;
	private TSB TSBFCB;
	private TSB TSBRDB, TSBWRB;
	private TSBhZtoLow TSBMNOTIO;
	private NOT NOTMNOTIO;
	private NOT NOTFCB;
	
	private AND AND3;
	private OR OR1;

	public MemOper() {
		componentName = "Mem1";
		displayName = "Operaciona jedinica memorija";
		NameConnector.addSchema(componentName, this);
		
		this.addSubScheme(new RAMModule());
	}
	
	public void initComponent() {
		super.initComponent();
		
		TSBRDB = new TSB("RDB");
		TSBRDB.getOutPin(0).setNumOfLines(1);
		TSBRDB.getOutPin(0).setIsBool();
		NOT1 = new NOT();
		AND1 = new AND();
		
		TSBMNOTIO = new TSBhZtoLow("TSBMNOTIO");
		TSBMNOTIO.getOutPin(0).setNumOfLines(1);
		TSBMNOTIO.getOutPin(0).setIsBool();
		
		NOTMNOTIO = new NOT();
		NOT3 = new NOT();

		TSBWRB = new TSB("WRB");
		TSBWRB.getOutPin(0).setNumOfLines(1);
		TSBWRB.getOutPin(0).setIsBool();
		AND2 = new AND();
		NOT2 = new NOT();

		ANDMEM = new AND();

		
		TSBMEMout = new TSB("MEMout");
		TSBMEMout.getOutPin(0).setNumOfLines(32);

		TSBFCB = new TSB("FCB");
		TSBFCB.getOutPin(0).setIsBool();
		NOTFCB = new NOT();
		
		AND3 = new AND();
		OR1 = new OR();

		putPins();
		putComponents();
	}
	
	public void initConections() {
		super.initConections();
		TSBMNOTIO.setInPin(0, NameConnector.getPin("Bus1.MNOTIOBUS"));
		TSBMNOTIO.setE(new Pin(true, "1"));
		
		NOTMNOTIO.setInPin(0, TSBMNOTIO.getOutPin(0));
		NOT3.setInPin(0, NOTMNOTIO.getOutPin(0));
		
		TSBRDB.setInPin(0, NameConnector.getPin("Bus2.NOTRDBUS"));
		TSBRDB.setE(new Pin(true, "1"));
		NOT1.setInPin(0, TSBRDB.getOutPin(0));
		AND1.setInPin(0, NOT1.getOutPin(0));
		AND1.setInPin(1, NOT3.getOutPin(0));

		TSBWRB.setInPin(0, NameConnector.getPin("Bus2.NOTWRBUS"));
		TSBWRB.setE(new Pin(true, "1"));
		NOT2.setInPin(0, TSBWRB.getOutPin(0));
		AND2.setInPin(0, NOT3.getOutPin(0));
		AND2.setInPin(1, NOT2.getOutPin(0));
		

		ANDMEM.setInPin(0, AND1.getOutPin(0));
		ANDMEM.setInPin(1, NameConnector.getPin("Mem2.fcMEM"));
		
		TSBMEMout.setInPin(0, NameConnector.getPin("MemModule.DOUT"));
		TSBMEMout.setE(ANDMEM.getOutPin(0));
		((Bus1) NameConnector.getSchema("Bus1")).addOnDBUS(TSBMEMout
				.getOutPin(0));

		OR1.setInPin(0, AND1.getOutPin(0));
		OR1.setInPin(1, AND2.getOutPin(0));
		AND3.setInPin(0, OR1.getOutPin(0));
		AND3.setInPin(1, NameConnector.getPin("Mem2.fcMEM"));
		TSBFCB.setInPin(0, NameConnector.getPin("Mem2.fcMEM"));
		TSBFCB.setE(AND3.getOutPin(0));
		NOTFCB.setInPin(0, TSBFCB.getOutPin(0));
		((Bus1) NameConnector.getSchema("Bus1")).addOnNOTFCBUS(NOTFCB
				.getOutPin(0));

	}
	
	public void initGui() {
		super.initGui();
		gui = new GuiSchema("src/images/Mem1.png");

		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(90, 111));
		points.add(new Point(116, 111));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.NOTRDBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(91, 235));
		points.add(new Point(116, 235));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus2.NOTWRBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(142, 173));
		points.add(new Point(170, 173));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(179, 223));
		points.add(new Point(170, 223));
		points.add(new Point(170, 122));
		points.add(new Point(177, 122));
		sections.add(points);
		line = new GuiPinLine(sections, NOT3.getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(90, 173));
		points.add(new Point(114, 173));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.MNOTIOBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 110));
		points.add(new Point(179, 110));
		sections.add(points);
		line = new GuiPinLine(sections, NOT1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(148, 235));
		points.add(new Point(178, 235));
		sections.add(points);
		line = new GuiPinLine(sections, NOT2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(418, 279));
		points.add(new Point(426, 279));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(204, 116));
		points.add(new Point(305, 116));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(74, 348));
		points.add(new Point(82, 348));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(204, 229));
		points.add(new Point(286, 229));
		points.add(new Point(286, 154));
		points.add(new Point(306, 154));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(74, 359));
		points.add(new Point(82, 359));
		sections.add(points);
		line = new GuiPinLine(sections, AND2.getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(104, 354));
		points.add(new Point(124, 354));
		points.add(new Point(124, 345));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(426, 291));
		points.add(new Point(418, 291));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(142, 300));
		points.add(new Point(184, 300));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(162, 301));
		points.add(new Point(162, 354));
		points.add(new Point(135, 354));
		points.add(new Point(135, 346));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem2.fcMEM"));
		gui.addLine(line);
		
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(129, 322));
		points.add(new Point(129, 306));
		sections.add(points);
		line = new GuiPinLine(sections, AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(448, 285));
		points.add(new Point(478, 285));
		points.add(new Point(478, 263));
		sections.add(points);
		line = new GuiPinLine(sections, ANDMEM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(111, 300));
		points.add(new Point(90, 300));
		sections.add(points);
		line = new GuiPinLine(sections, NOTFCB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(586, 257));
		points.add(new Point(490, 257));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMEMout.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(413, 22));
		points.add(new Point(413, 88));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.ABUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(585, 23));
		points.add(new Point(585, 291));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(520, 137));
		points.add(new Point(585, 137));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.DBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(413, 182));
		points.add(new Point(413, 257));
		points.add(new Point(465, 257));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("MemModule.DOUT"));
		gui.addLine(line);
		

		gui.addLabel(new GuiPinLabel(421, 82, NameConnector.getPin("Bus1.ABUS")));
		gui.addLabel(new GuiPinLabel(593, 81, NameConnector.getPin("Bus1.DBUS")));
		gui.addLabel(new GuiPinLabel(421, 197, NameConnector.getPin("MemModule.DOUT")));

	}
	public void putPins() {
		NameConnector.addPin(componentName, "RDF", AND1.getOutPin(0));

		NameConnector.addPin(componentName, "WRF", AND2.getOutPin(0));
		
		//NameConnector.addPin(componentName, "ABUS", NameConnector.getPin("Bus1.ABUS"));

		//NameConnector.addPin(componentName, "DBUS", NameConnector.getPin("Bus1.DBUS"));
	}

	public void putComponents() {

	}
	
	public void addOnDOUTMEM(Pin pin) {
		TSBMEMout.setInPin(0, pin);
	}


}
