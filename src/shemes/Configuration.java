package shemes;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import logic.Execution;
import logic.Pin;
import logic.components.CLK;
import shemes.components.Memorija;
import shemes.components.Arb;
import shemes.components.Procesor;
import util.NameConnector;

public class Configuration extends AbstractSchema {
	CLK clk;

	public Configuration() {
		componentName = "System";
		displayName = "System";
		this.addSubScheme(new Procesor());
		this.addSubScheme(new Memorija());
		this.addSubScheme(new Arb());
		
		clk = new CLK("CLK", 1, 0);
		Execution.addSequentialComponent(clk);

		putComponents();
	}
	
	private void putComponents() {
		NameConnector.addComponent(componentName, clk.getName(), clk);
	}

	@Override
	public void initGui() {
		super.initGui();
		
		gui = new GuiSchema("src/images/System.png");
		
		GuiPinLine line;
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(65, 447));
		points.add(new Point(92, 447));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 466));
		points.add(new Point(92, 466));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 484));
		points.add(new Point(92, 484));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 503));
		points.add(new Point(93, 503));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 522));
		points.add(new Point(92, 522));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 541));
		points.add(new Point(92, 541));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 560));
		points.add(new Point(92, 560));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 579));
		points.add(new Point(92, 579));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(65, 635));
		points.add(new Point(91, 635));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(545, 439));
		points.add(new Point(545, 416));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(571, 439));
		points.add(new Point(571, 417));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(593, 439));
		points.add(new Point(593, 417));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(616, 440));
		points.add(new Point(616, 416));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(642, 439));
		points.add(new Point(642, 416));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(665, 440));
		points.add(new Point(665, 415));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(688, 438));
		points.add(new Point(688, 417));
		sections.add(points);
		line = new GuiPinLine(sections,  new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(507, 155));
		points.add(new Point(16, 155));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(102, 155));
		points.add(new Point(102, 40));
		points.add(new Point(130, 40));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(120, 361));
		points.add(new Point(120, 156));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.ABUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(508, 183));
		points.add(new Point(18, 183));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(409, 183));
		points.add(new Point(409, 30));
		points.add(new Point(347, 30));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(375, 184));
		points.add(new Point(375, 49));
		points.add(new Point(347, 49));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 363));
		points.add(new Point(394, 184));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.DBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(508, 268));
		points.add(new Point(17, 268));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(234, 362));
		points.add(new Point(234, 269));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(215, 268));
		points.add(new Point(215, 78));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.MNOTIOBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(507, 239));
		points.add(new Point(17, 239));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(262, 239));
		points.add(new Point(262, 78));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(281, 362));
		points.add(new Point(281, 239));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus2.NOTRDBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(507, 211));
		points.add(new Point(18, 211));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(168, 78));
		points.add(new Point(168, 211));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(186, 361));
		points.add(new Point(186, 211));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus2.NOTWRBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(507, 296));
		points.add(new Point(17, 296));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(309, 296));
		points.add(new Point(309, 78));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(328, 362));
		points.add(new Point(328, 297));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus1.NOTFCBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(507, 324));
		points.add(new Point(17, 324));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus2.NOTBUSYBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(422, 494));
		points.add(new Point(478, 494));
		points.add(new Point(478, 416));
		points.add(new Point(522, 416));
		points.add(new Point(522, 439));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus2.BR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(423, 522));
		points.add(new Point(479, 522));
		points.add(new Point(479, 626));
		points.add(new Point(522, 626));
		points.add(new Point(522, 607));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_7"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(422, 475));
		points.add(new Point(441, 475));
		points.add(new Point(441, 325));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus2.BUSYBUSOUT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(422, 550));
		points.add(new Point(450, 550));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Bus2.BG_OUT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(544, 605));
		points.add(new Point(544, 628));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_6"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(571, 628));
		points.add(new Point(571, 606));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_5"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(593, 627));
		points.add(new Point(593, 606));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_4"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(616, 628));
		points.add(new Point(616, 606));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(643, 628));
		points.add(new Point(643, 605));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(665, 628));
		points.add(new Point(665, 606));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(688, 628));
		points.add(new Point(688, 605));
		sections.add(points);
		line = new GuiPinLine(sections,  NameConnector.getPin("Arb.BG_IN_0"));
		gui.addLine(line);


		gui.addLabel(new GuiPinLabel(434, 150,  NameConnector.getPin("Bus1.ABUS")));
		gui.addLabel(new GuiPinLabel(434, 180,  NameConnector.getPin("Bus1.DBUS")));
		gui.addLabel(new GuiPinLabel(41, 151,  NameConnector.getPin("Bus1.ABUS")));
		gui.addLabel(new GuiPinLabel(41, 179,  NameConnector.getPin("Bus1.DBUS")));
	}

}
