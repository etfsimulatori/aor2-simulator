package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import util.Log;

public class GuiSchema extends JPanel {

	private static final long serialVersionUID = 1L;

	private BufferedImage image;
	private File imagefile;
	private List<GuiPinLine> lines;
	private List<GuiPinLabel> labels;

	public GuiSchema() {
	}

	public GuiSchema(String filename) {
		setBackground(Color.WHITE);
		imagefile = new File(filename);
		try {
			image = ImageIO.read(imagefile);
		} catch (IOException e) {
			Log.log(e);
			Log.log("Missing file: " + imagefile.getAbsolutePath());
		}
		lines = new ArrayList<GuiPinLine>();
		labels = new ArrayList<GuiPinLabel>();

		Dimension size = new Dimension(image.getWidth(), image.getHeight());
		adjustSize(size);
	}


	public void paint(Graphics g) {
		// super.paint(g);
		clearPanel(g);

		g.drawImage(image, 0, 0, null);

		for (GuiPinLine gl : lines) {
			gl.draw(g);
		}
		for (GuiPinLabel gl : labels) {
			gl.draw(g);
		}
		getToolkit().sync();

	}

	public void clearPanel(Graphics g) {
		Color tmp = g.getColor();
		Dimension size = this.getParent().getSize();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, Math.max(this.getWidth(), (int) size.getWidth()),
				Math.max(this.getHeight(), (int) size.getHeight()));
		g.setColor(tmp);
	}

	public void update(Graphics g) {
		paint(g);
	}

	public BufferedImage getImage() {
		return image;
	}

	public void addLine(GuiPinLine line) {
		lines.add(line);
	}

	public void addLabel(GuiPinLabel label) {
		labels.add(label);
	}

	public List<GuiPinLine> getLines() {
		return lines;
	}

	public void setLines(List<GuiPinLine> lines) {
		this.lines = lines;
	}

	public List<GuiPinLabel> getLabels() {
		return labels;
	}

	public void setLabels(List<GuiPinLabel> labels) {
		this.labels = labels;
	}
	
	public void adjustSize(Dimension size) {
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setSize(size);
		validate();
	}

}
