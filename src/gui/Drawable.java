package gui;

import java.awt.*;

public interface Drawable {

	public void draw(Graphics g);

	public void update();
}
