package gui.components;

import gui.GuiImageLabel;
import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.components.TSB;

public class TSBGui extends OneOutGui {

	public TSBGui(TSB tsb, int x, int y, String[] args,
			boolean printInPortName, boolean printOutPortName) {
		super(tsb, x, y, args, printInPortName, printOutPortName,
				"src/images/TRI.png");

	}

	@Override
	public GuiSchema convert(GuiSchema gui) {

		GuiPinLine line;
		GuiTextLabel label;

		GuiImageLabel im = new GuiImageLabel(x + 100, y, imageURL);
		gui.addLabel(im);

		List<List<Point>> sections;
		List<Point> points;

		if (printInPortName) {
			label = getLabel(component.getInPin(0), x, y + 9);
			gui.addLabel(label);
		}
		if (printOutPortName) {
			String outName = component.getName();
			if ((outName != null) && !("".equals(outName))) {
				label = new GuiTextLabel(x + 140, y + 4, outName, 12);
				gui.addLabel(label);
			}
		}

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(x + 66, y + 9));
		points.add(new Point(x + 100, y + 9));
		sections.add(points);
		line = new GuiPinLine(sections, component.getInPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(x + 125, y + 9));
		points.add(new Point(x + 160, y + 9));
		sections.add(points);
		line = new GuiPinLine(sections, component.getOutPin(0));
		gui.addLine(line);

		TSB tsb = (TSB) component;
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(x + 112, y + 10));
		points.add(new Point(x + 112, y + 30));
		sections.add(points);
		line = new GuiPinLine(sections, tsb.getE());
		gui.addLine(line);

		gui.addLabel(new GuiPinLabel(x + 20, y + 12, component.getInPin(0)));

		gui.addLabel(new GuiTextLabel(x + 75, y + 8, ""
				+ component.getInPin(0).getNumOfLines(), 14));
		gui.addLabel(new GuiTextLabel(x + 145, y + 8, ""
				+ component.getOutPin(0).getNumOfLines(), 14));

		String eName = tsb.getE().getName();
		if ((eName != null) && !("".equals(eName))) {
			label = getLabel(tsb.getE(), x + 140, y + 40);
			gui.addLabel(label);
		}

		return gui;
	}

	@Override
	public int getWidth() {
		String outName = component.getName();
		TSB tsb = (TSB) component;
		String eName = tsb.getE().getName();

		return Math.max(112 - 35 + eName.length() * 10,
				112 - 35 + outName.length() * 10);
	}

	@Override
	public int getHeight() {
		return 60;
	}

}
