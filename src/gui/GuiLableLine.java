package gui;

import java.awt.*;
import java.util.List;

// Klasa koja predstavja vise izlomljenih linija
public class GuiLableLine extends GuiPinLine {

	public GuiLableLine(List<List<Point>> sections) {
		super(sections, null);
		color = Color.BLACK;
	}

	public void draw(Graphics g) {
		Color col = g.getColor();
		g.setColor(color);
		for (List<Point> section : sections) {
			Point last = null;
			for (Point p : section) {
				if (last != null) {
					g.drawLine(last.x, last.y, p.x, p.y);
					if (last.x == p.x)
						g.drawLine(last.x + 1, last.y, p.x + 1, p.y);
					if (last.y == p.y)
						g.drawLine(last.x, last.y + 1, p.x, p.y + 1);
				}
				last = p;
			}
		}
		g.setColor(col);
	}

}
