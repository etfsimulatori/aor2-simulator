package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import util.DrawUtils;

// Klasa koja predstavja jednu labelu
public class GuiTextLabel extends GuiPinLabel {

	int fontSize;// podrazumevano 12;

	public GuiTextLabel(int xx, int yy, String label, int fontSize,
			int alignment) {
		super(xx, yy, null);
		this.x = DrawUtils.calcX(xx, label, fontSize, alignment);
		this.label = label;
		this.fontSize = fontSize;
	}

	public GuiTextLabel(int xx, int yy, String label, int fontSize) {
		super(xx, yy, null);
		this.label = label;
		this.fontSize = fontSize;
	}

	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.setFont(new Font("Times New Roman", Font.PLAIN, fontSize));
		g.drawString(label, x, y);
	}

	public void update() {
	}

}
