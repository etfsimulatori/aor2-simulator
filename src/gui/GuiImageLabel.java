package gui;

import java.awt.*;
import javax.swing.*;

import util.*;

public class GuiImageLabel extends GuiPinLabel {

	protected String imageURL;
	protected Image image;
	protected int width, height;

	public GuiImageLabel(int x, int y, String imageURL) {
		super(x, y, null);
		this.imageURL = imageURL;
		try {
			if (imageURL != null) {
				ImageIcon icon = new ImageIcon(imageURL);
				image = icon.getImage();
				width = image.getWidth(null);
				height = image.getHeight(null);
			}
		} catch (Exception e) {
			Log.log(e);
		}

	}

	public void draw(Graphics g) {
		if (image != null) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g.drawImage(image, x, y, width, height, null);

		}
	}

	public void update() {
	}
}
