package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import logic.LogicalComponent;
import logic.components.REG;
import util.NameConnector;

public class RegisterDialogConfigurable extends JDialog {

	private static final long serialVersionUID = 1L;

	CPURegisterPanel panel;

	public RegisterDialogConfigurable() {
		this.setTitle("CPU registri");

		this.panel = new CPURegisterPanel(this, getRegs());
		this.add(panel);

		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				setVisible(false);
				Main.currentScheme.repaint();
			}
		});

		this.setModal(true);

		this.setResizable(false);

		// this.setPreferredSize(new Dimension(600, 600));
		// this.setSize(600, 600);
		this.setLocation(100, 100);
		this.setVisible(false);

	}

	public void init() {
		panel.init();
	}

	class CPURegisterPanel extends JPanel implements ActionListener {
		private static final long serialVersionUID = 1L;
		private List<REG> regs;
		private Map<String, REG> regsNames;

		private List<JTextField> regsFields;

		private JButton izmeni;
		private JButton izadji;

		private JDialog greska;
		private JDialog potvrda;

		private JDialog sadrzalacRegistara;

		public JDialog getSadrzalacRegistara() {
			return sadrzalacRegistara;
		}

		public void setSadrzalacRegistara(JDialog sadrzalacRegistara) {
			this.sadrzalacRegistara = sadrzalacRegistara;
		}

		public CPURegisterPanel(RegisterDialogConfigurable parent,
				List<REG> regs) {
			super();
			sadrzalacRegistara = parent;

			setLayout(new BorderLayout());
			setBackground(Color.white);

			this.regs = regs;
			this.regsNames = new HashMap<String, REG>();
			for (REG reg : regs) {
				regsNames.put(reg.getName(), reg);
			}
			regsFields = new LinkedList<JTextField>();

		}

		public void init() {
			Color bgcolor = Color.white;

			int numRegs = regs.size();
			int numC = Math.max((int) (Math.sqrt(numRegs / 2)), 1);

			int numV = (int) (numRegs / numC);
			if (numC * numV < numRegs) {
				numV = numV + 1;
			}

			GridLayout layout = new GridLayout(1, 2 * numC);
			layout.setHgap(10);
			JPanel registrisvi = new JPanel(layout);
			registrisvi.setBackground(bgcolor);

			int index = 0;
			for (int i = 0; i < numC; i++) {
				GridLayout valuesLayout = new GridLayout(numV, 1);
				valuesLayout.setVgap(10);
				JPanel values = new JPanel(valuesLayout);
				values.setBackground(bgcolor);

				GridLayout labledLayout = new GridLayout(numV, 1);
				labledLayout.setVgap(10);
				JPanel lables = new JPanel(labledLayout);
				lables.setBackground(bgcolor);
				for (int j = 0; j < numV && index < numRegs; j++, index++) {

					REG reg = regs.get(index);

					// JPanel tmpPanel = new JPanel();
					// tmpPanel.setBackground(bgcolor);

					JLabel ime = new JLabel(reg.getName());
					ime.setVerticalAlignment(0);
					ime.setHorizontalAlignment(4);
					lables.add(ime);

					JTextField regPolje = new JTextField(10);// 5 je broj kolona
					regPolje.setColumns(8);
					regPolje.setText(Integer.toHexString(reg.getVal()));
					regPolje.setName(reg.getName());
					regsFields.add(regPolje);
					values.add(regPolje);

					// prvi.add(tmpPanel);

				}
				registrisvi.add(lables);
				// registrisvi.add(Box.createVerticalStrut(0));
				registrisvi.add(values);
			}

			add(registrisvi, "Center");

			JPanel komande = new JPanel();
			komande.setBackground(bgcolor);

			izmeni = new JButton("Change");
			izmeni.addActionListener(this);

			izadji = new JButton("Cancel ");
			izadji.addActionListener(this);

			komande.add(this.izmeni);
			komande.add(this.izadji);
			add(komande, "South");
		}

		public void actionPerformed(ActionEvent arg) {

			if (arg.getActionCommand().equals("Change")) {
				boolean error = false;

				int cnt = regs.size();
				boolean[] nizvrednosti = new boolean[cnt];
				for (int i = 0; i < cnt; i++) {
					nizvrednosti[i] = proveri(regsFields.get(i));
					error = error || nizvrednosti[i];
				}

				if (!error) {
					for (int i = 0; i < cnt; i++) {
						JTextField regsField = regsFields.get(i);
						REG reg = regsNames.get(regsField.getName());

						reg.setVal(dohvatiVrednost(regsFields.get(i)));
					}
					potvrda = new JDialog();
					potvrda.setSize(350, 100);
					potvrda.setLocation(200, 200);
					potvrda.setModal(true);
					potvrda.setTitle("Potvrda");
					JPanel osnovni = new JPanel(new GridLayout(2, 1));
					JLabel lab = new JLabel(
							"Promena sadrzaja registara je uspesno odradjena!");
					lab.setHorizontalAlignment(0);
					osnovni.add(lab);
					JButton ok = new JButton("U redu");
					ok.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							potvrda.setVisible(false);

						}
					});
					JPanel tmp = new JPanel();
					tmp.add(ok);
					tmp.setBackground(Color.white);
					osnovni.add(tmp);
					osnovni.setBackground(Color.white);
					potvrda.add(osnovni);
					potvrda.setVisible(true);

					Main.currentScheme.validate();
					Main.currentScheme.repaint();

				} else {
					error = false;
					greska = new JDialog();
					greska.setSize(550, 150);
					greska.setLocation(200, 200);
					greska.setModal(true);
					greska.setTitle("Greska!");
					JPanel osnovni = new JPanel(new GridLayout(3, 1));
					JLabel lab = new JLabel(
							"Neka od unetih vrednosti nije dobra (ili prelazi velicinu registra ili je unet netacan hex. broj).");
					lab.setHorizontalAlignment(0);
					osnovni.add(lab);
					JLabel lab1 = new JLabel(
							"Zato se brisu sve tekuce izmene registara!");
					lab1.setHorizontalAlignment(0);
					osnovni.add(lab1);
					JButton ok = new JButton("U redu");
					ok.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							greska.setVisible(false);
							sadrzalacRegistara.setVisible(true);
						}

					});
					JPanel tmp = new JPanel();
					tmp.add(ok);
					tmp.setBackground(Color.white);
					osnovni.add(tmp);
					osnovni.setBackground(Color.white);
					greska.add(osnovni);
					greska.setVisible(true);
				}
			} else {
				sadrzalacRegistara.setVisible(false);
			}
			repaint();
		}

		private boolean proveri(JTextField field) {
			boolean error = false;
			REG reg = regsNames.get(field.getName());
			String text = field.getText();
			long val;
			try {
				val = Long.parseLong(text, 16);
			} catch (Exception e) { // ZBOG NUMBER FORMAT EXCEPTION!
				e.printStackTrace();
				error = true;
				return error;
			}

			int validBits;
			if (reg.getOutPin(0).isBool()) {
				validBits = reg.getOutPins().length;
			} else {
				validBits = reg.getOutPin(0).getNumOfLines();
			}
			if ((long) Math.pow(2.0D, validBits) <= val) {
				error = true;
			}

			return error;
		}

		private int dohvatiVrednost(JTextField field) {
			String text = field.getText();
			long val;
			try {
				val = Long.parseLong(text, 16);
			} catch (Exception e) { // ZBOG NUMBER FORMAT EXCEPTION!
				return 0;
			}

			return (int)val;
		}

		public void paint(Graphics g) {
			super.paint(g);

			for (JTextField field : regsFields) {
				field.setText(RegFormater.format(regsNames.get(field.getName())));
			}
		}
	}

	public static class RegFormater {

		public static int getSize(REG reg) {
			int validBits;
			if (reg.getOutPin(0).isBool()) {
				validBits = reg.getOutPins().length;
			} else {
				validBits = reg.getOutPin(0).getNumOfLines();
			}
			return 1 + (validBits + 3) / 4;
		}

		public static String format(REG reg) {
			int validBits;
			if (reg.getOutPin(0).isBool()) {
				validBits = reg.getOutPins().length;
			} else {
				validBits = reg.getOutPin(0).getNumOfLines();
			}
			validBits = (validBits + 3) / 4;
			String result = String.format("%0" + validBits + "x", reg.getVal());
			return result;
		}

		public static int reformat(String val, REG reg) {

			int result = 0;
			try {
				result = Integer.parseInt(val, 16);
			} catch (Exception e) {
			}
			return result;
		}
	}

	private List<REG> getRegs() {
		Set<REG> resultSet = new HashSet<REG>();
		Map<String, LogicalComponent> comps = NameConnector.listComponents();
		for (LogicalComponent comp : comps.values()) {
			if (comp instanceof REG) {
				resultSet.add((REG) comp);
			}
		}
		List<REG> result = new LinkedList<REG>(resultSet);
		Collections.sort(result, new RegNameComparator());
		return result;
	}

	class RegNameComparator implements Comparator<REG> {
		Pattern splitter = Pattern.compile("(\\d+|\\D+)");

		@Override
		public int compare(REG reg1, REG reg2) {
			String s1 = reg1.getName();
			String s2 = reg2.getName();

			// I deliberately use the Java 1.4 syntax,
			// all this can be improved with 1.5's generics

			// We split each string as runs of number/non-number strings
			ArrayList<String> sa1 = split(s1);
			ArrayList<String> sa2 = split(s2);
			// Nothing or different structure
			if (sa1.size() == 0 || sa1.size() != sa2.size()) {
				// Just compare the original strings
				return s1.compareTo(s2);
			}
			int i = 0;
			String si1 = "";
			String si2 = "";
			// Compare beginning of string
			for (; i < sa1.size(); i++) {
				si1 = (String) sa1.get(i);
				si2 = (String) sa2.get(i);
				if (!si1.equals(si2))
					break; // Until we find a difference
			}
			// No difference found?
			if (i == sa1.size())
				return 0; // Same strings!

			// Try to convert the different run of characters to number
			int val1, val2;
			try {
				val1 = Integer.parseInt(si1);
				val2 = Integer.parseInt(si2);
			} catch (NumberFormatException e) {
				return s1.compareTo(s2); // Strings differ on a non-number
			}

			// Compare remainder of string
			for (i++; i < sa1.size(); i++) {
				si1 = (String) sa1.get(i);
				si2 = (String) sa2.get(i);
				if (!si1.equals(si2)) {
					return s1.compareTo(s2); // Strings differ
				}
			}

			// Here, the strings differ only on a number
			return val1 < val2 ? -1 : 1;
		}

		ArrayList<String> split(String s) {
			ArrayList<String> r = new ArrayList<String>();
			Matcher matcher = splitter.matcher(s);
			while (matcher.find()) {
				String m = matcher.group(1);
				r.add(m);
			}
			return r;
		}
	}
}
