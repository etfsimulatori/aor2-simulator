package main;

import gui.GuiSchema;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;

import logic.Execution;
import logic.Netlist;
import shemes.AbstractSchema;
import shemes.Configuration;
import shemes.Schema;
import util.Log;
import util.Parameters;
import util.TimeHistory;

@SuppressWarnings("serial")
public class Main extends JFrame {

	private static final String SIMULATORNAME = "Simulator jednoadresnog procesora sa horizontalnim mikroprogramskim upravljanjem";

	public static final String ICON = "src/images/btnCompile.png";

	public static List<Schema> schemas;

	public static GuiSchema currentScheme;

	public static JScrollPane centralPanel;

	public static GraphicsPanel graphs;

	public static JScrollPane graphPanel;

	public static TimeHistory history;

	public static CommandStatusPanel east;

	public static JTextArea logArea;

	public static JPanel west;

	public static JMenuBar menuBar;

	public static MemoryDialog dialogMem;
	public static RegisterDialogConfigurable dialogRegs;
	public static FlipflopDialog dialogFlipflops;
	public static SignalsDialog dialogSignals;

	public static Main mainWindow;

	public Main(List<Schema> schemas) {
		super(SIMULATORNAME);
		mainWindow = this;

		this.setIconImage(new ImageIcon(ICON).getImage());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPopupMenu.setDefaultLightWeightPopupEnabled(false);
		ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);

		Main.schemas = schemas;

		currentScheme = AbstractSchema.getSchenaWithGui(schemas.get(0))
				.getGui();

		centralPanel = new JScrollPane(currentScheme);
		centralPanel.setBackground(Color.WHITE);
		centralPanel.setMinimumSize(new Dimension(800, 750));
		centralPanel.setSize(new Dimension(800, 750));

		graphs = new GraphicsPanel();

		graphPanel = new JScrollPane(graphs);
		graphPanel.setBackground(Color.WHITE);
		graphPanel.setMinimumSize(new Dimension(800, 175));
		graphPanel.setSize(new Dimension(800, 175));
		graphPanel.setMaximumSize(new Dimension(800, 175));

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
				centralPanel, graphPanel);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(750);

		JPanel middle = new JPanel();
		middle.setLayout(new BorderLayout());
		middle.setBackground(Color.WHITE);

		middle.setMinimumSize(new Dimension(800, 950));
		middle.setSize(new Dimension(800, 950));
		middle.add(splitPane, BorderLayout.CENTER);

		this.add("Center", middle);

		east = new CommandStatusPanel();
		Dimension d = new Dimension(160, 620);
		east.setMaximumSize(d);
		east.setMinimumSize(d);
		east.setPreferredSize(d);
		east.setSize(d);
		this.add("East", east);

		west = new JPanel();
		west.setLayout(new BorderLayout());
		JPanel threePanel = new TreePanel(schemas);
		west.add(threePanel, BorderLayout.CENTER);
		JPanel menuPanel = new MenuPanel();
		west.add(menuPanel, BorderLayout.SOUTH);
		this.add("West", west);

		menuBar = new SimMenu();
		this.setJMenuBar(menuBar);

		logArea.append(Log.buffer.toString());

		dialogMem = new MemoryDialog();
		dialogMem.init();
		dialogRegs = new RegisterDialogConfigurable();
		dialogRegs.init();
		dialogFlipflops = new FlipflopDialog();
		dialogFlipflops.init();
		dialogSignals = new SignalsDialog();
		dialogSignals.init();

		dialogSignals.gp = graphs;

	}
	public void start(){
		this.pack();
		this.validate();
		this.setSize(1200, 1000);
		this.setVisible(true);
	}

	public static void main(String[] args) {
		// setLookAndFeel();

		Parameters.init("Konfiguracija.txt");

		List<Schema> schemas = initSchems();

		for (Schema schema : schemas) {
			schema.initComponent();
		}

		for (Schema schema : schemas) {
			schema.initConections();
		}

		for (Schema schema : schemas) {
			schema.initGui();
		}

		Main main = new Main(schemas);
		Log.logArea = Main.logArea;

		history = new TimeHistory(dialogSignals.listAllSignals(), graphs);
		graphs.setHistory(history);
		Execution.setHistory(history);
		Execution.setComponent(Netlist.extractAllComponent(Execution
				.getSequentialComponent()));
		Execution.init(true);
		
		main.start();
	}

	private static List<Schema> initSchems() {
		List<Schema> result = new LinkedList<Schema>();
		result.add(new Configuration());
		return result;
	}

	public static void setLookAndFeel() {
		String lookAndFeel = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
		// String lookAndFeel = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
		// String lookAndFeel =
		// "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
		// String lookAndFeel = "javax.swing.plaf.metal.MetalLookAndFeel";
		try {
			UIManager.setLookAndFeel(lookAndFeel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
