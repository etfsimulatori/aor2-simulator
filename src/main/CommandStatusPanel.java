package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;

import logic.Execution;
import logic.Pin;
import util.Log;
import util.NameConnector;

public class CommandStatusPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	JPanel northeast = new JPanel();
	JScrollPane scrolltxt = new JScrollPane();
	JPanel southeast = new JPanel();

	JLabel CPUclock = new JLabel("CPU clock = 0");
	JLabel Tcpu = new JLabel("mPC = 0");
	JLabel PC = new JLabel("PC = 100");
	JButton CLK = new JButton("                 CLK+               ");
	JButton INS = new JButton("        INSTRUCTION+      ");
	JButton PRG = new JButton("          PROGRAM+          ");

	JButton GOTO = new JButton("GO TO:");
	JTextField end = new JTextField();

	JScrollPane scrollPane;
	// Labela za oznaku faze
	JLabel phaseName = new JLabel("Faza izvrsavanja:");
	// Labela koja kazuje fazu
	JLabel phase = new JLabel("-");
	// Labela za oznaku nacina adresiranja
	JLabel signsName = new JLabel("Microprogram:", JLabel.LEFT);
	// Labela koja prikazuje aktivnee signale
	JLabel signs = new JLabel("-", JLabel.LEFT);
	// treba dodati opcioju da se resetuje sistem na pocetno stanje
	JLabel logtxt = new JLabel("Log");

	StepsLoader stepLoader;

	JTextArea logArea = new JTextArea();

	public CommandStatusPanel() {
		stepLoader = new StepsLoader("./microCode.txt", null);

		phase.setText(stepLoader.getDescAt(NameConnector.getPin("Counter.mPC")
				.getIntVal()) == null ? phase.getText() : stepLoader
				.getDescAt(NameConnector.getPin("Counter.mPC").getIntVal()));
		String s1 = stepLoader.getStepsAt(NameConnector.getPin("Counter.mPC")
				.getIntVal()) == null ? signs.getText() : stepLoader
				.getStepsAt(NameConnector.getPin("Counter.mPC").getIntVal());
		s1 = "<html>" + s1 + "</html>";

		signs.setText(s1);
		signs.setAlignmentX(CENTER_ALIGNMENT);

		northeast.setLayout(new GridLayout(2, 1));
		JPanel northeastI = new JPanel();
		northeastI.setLayout(new BoxLayout(northeastI, BoxLayout.Y_AXIS));

		CPUclock.setAlignmentX(CENTER_ALIGNMENT);
		northeastI.add(CPUclock);

		Tcpu.setAlignmentX(CENTER_ALIGNMENT);
		northeastI.add(Tcpu);

		PC.setAlignmentX(CENTER_ALIGNMENT);
		northeastI.add(PC);

		CLK.addActionListener(new CLKActionListener());
		CLK.setAlignmentX(CENTER_ALIGNMENT);
		northeastI.add(CLK);

		INS.addActionListener(new INSActionListener());
		INS.setAlignmentX(CENTER_ALIGNMENT);
		northeastI.add(INS);

		PRG.addActionListener(new PRGActionListener());
		PRG.setAlignmentX(CENTER_ALIGNMENT);
		northeastI.add(PRG);

		JPanel GOTOpanel = new JPanel(new GridLayout(1, 2));
		GOTO.addActionListener(new GOTOActionListener());
		GOTO.setAlignmentX(LEFT_ALIGNMENT);
		GOTOpanel.add(GOTO);
		end.setAlignmentX(RIGHT_ALIGNMENT);
		GOTOpanel.add(end);
		northeastI.add(GOTOpanel);

		northeast.add(northeastI);

		JPanel northeastII = new JPanel();
		northeastII.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Control unit info",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0,
						255)));
		northeastII.setLayout(new BoxLayout(northeastII, BoxLayout.Y_AXIS));
		// Dodati labelu koja kazuje fazu
		phaseName.setAlignmentX(CENTER_ALIGNMENT);
		northeastII.add(phaseName);
		phaseName.setFont(new Font("D", 10, 13));
		phase.setAlignmentX(CENTER_ALIGNMENT);
		northeastII.add(phase);

		// Dodati labelu koja kazuje nacin adresiranja
		signsName.setAlignmentX(CENTER_ALIGNMENT);
		northeastII.add(signsName);
		signsName.setFont(new Font("D", 10, 13));
		signs.setMinimumSize(new Dimension(10, 40));
		// signs.setAlignmentX(CENTER_ALIGNMENT);

		northeastII.add(signs);

		northeast.add(northeastII);

		DefaultCaret caret = (DefaultCaret) logArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		logArea.setText("");
		logArea.setAlignmentX(CENTER_ALIGNMENT);
		southeast.setLayout(new BoxLayout(southeast, BoxLayout.Y_AXIS));

		// Dodat scroll bar za asemblerski tekst
		logtxt.setAlignmentX(CENTER_ALIGNMENT);
		// southeast.add(asmtxt);

		scrolltxt = new JScrollPane(logArea);
		scrolltxt.setName("KOD");
		scrolltxt.setAlignmentX(CENTER_ALIGNMENT);
		scrolltxt
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrolltxt
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		southeast.add(scrolltxt);
		southeast.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Log", TitledBorder.CENTER,
				TitledBorder.TOP, null, new Color(0, 0, 255)));

		this.setLayout(new GridLayout(2, 1));
		this.add(northeast);
		this.add(southeast);

		Main.logArea = logArea;
	}

	public class GOTOActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String s = end.getText();
			end.setText("");

			try {
				int n = Integer.parseInt(s);
				if (n < 0) {
					return;
				}
				if (n < Execution.globalTime) {
					// resetuje se sistem
					Execution.init(false);
					updateLabels();
					updateCUContext();
					enableNext();
				}

				while (NameConnector.getPin("Exec2.START").getBoolVal() == true
						&& Execution.globalTime < n) {

					Execution.nextCLK();
					updateLabels();
					updateCUContext();
				}

				validateNext();
				Main.currentScheme.validate();
				Main.currentScheme.repaint();

			} catch (NumberFormatException e) {
				return;// ovde eventualno ubaciti da vrati prozor koji kaze
						// da nije lepo uneto
				// catch postoji samo da ne bi pukla aplikacija zbog unetih
				// slova ili hex brojeva
			}
		}
	}

	public class PRGActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			while (NameConnector.getPin("Exec2.START").getBoolVal() == true) {

				Execution.nextCLK();
				updateLabels();
				updateCUContext();

			}
			validateNext();
			Main.currentScheme.validate();
			Main.currentScheme.repaint();
		}
	}

	public class INSActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			// par kloka da izadje iz T00

			Execution.nextCLK();
			updateLabels();
			updateCUContext();
			validateNext();
			Main.currentScheme.validate();
			Main.currentScheme.repaint();

			Execution.nextCLK();
			updateLabels();
			updateCUContext();
			validateNext();
			Main.currentScheme.validate();
			Main.currentScheme.repaint();

			// vrti se dok ne dodje do T00
			while ((NameConnector.getPin("Counter.mPC").getIntVal() != 0)
					&& (NameConnector.getPin("Exec2.START").getBoolVal() == true)) {

				Execution.nextCLK();
				updateLabels();
				updateCUContext();

			}
			validateNext();
			Main.currentScheme.validate();
			Main.currentScheme.repaint();
		}

	}

	public class CLKActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			Execution.nextCLK();
			updateLabels();
			updateCUContext();
			validateNext();
			Main.currentScheme.validate();
			Main.currentScheme.repaint();

		}
	}

	public void reset() {
		scrolltxt.getVerticalScrollBar().setValue(0);
		end.setText("");

		updateLabels();
		updateCUContext();
		enableNext();
		Main.currentScheme.validate();
		Main.currentScheme.repaint();

		Log.logArea.setText(null);

		// Main.dialogSignals.resetCheckBoxes();

		Main.graphPanel.validate();
		Main.graphPanel.repaint();

	}

	public void updateLabels() {
		CPUclock.setText("CPU clock = " + Execution.globalTime);
		Tcpu.setText("Tcpu = "
				+ Integer.toHexString(NameConnector.getPin("Counter.mPC")
						.getIntVal()));
		PC.setText("PC ="
				+ Integer.toHexString(NameConnector.getPin("Gpr.PC")
						.getIntVal()));
	}

	public void updateCUContext() {

		Pin mPC = NameConnector.getPin("Counter.mPC");
		if (mPC != null) {
			phase.setText(stepLoader.getDescAt(mPC.getIntVal()));
			String s1 = stepLoader.getStepsAt(mPC.getIntVal());
			s1 = "<html>" + s1 + "</html>";
			signs.setText(s1);
		}
	}

	public void validateNext() {
		if (NameConnector.getPin("Exec2.START").getBoolVal() == false) {
			CLK.setEnabled(false);
			INS.setEnabled(false);
			PRG.setEnabled(false);
			GOTO.setEnabled(false);
		}
	}

	public void enableNext() {
		CLK.setEnabled(true);
		INS.setEnabled(true);
		PRG.setEnabled(true);
		GOTO.setEnabled(true);
	}
}
