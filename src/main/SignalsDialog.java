package main;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import util.NameConnector;

public class SignalsDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel signals = new javax.swing.JPanel();
	private JScrollPane leftPanel = new javax.swing.JScrollPane();
	private JTable leftTable = new javax.swing.JTable();
	private JScrollPane rightPanel = new javax.swing.JScrollPane();
	private JTable rightTable = new javax.swing.JTable();
	private JButton drawButton = new javax.swing.JButton();
	private JButton clearButton = new javax.swing.JButton();

	public GraphicsPanel gp;

	@SuppressWarnings("serial")
	public SignalsDialog() {
		// diagram = this;
		this.setIconImage(new ImageIcon(Main.ICON).getImage());

		signals.setBackground(new java.awt.Color(255, 255, 255));
		signals.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Signals",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0,
						255)));

		List<String> names = NameConnector.listPinNames();

		Object[][] leftObjects = new Object[(names.size() + 1) / 2][];
		for (int i = 0; i < leftObjects.length; i++) {
			leftObjects[i] = new Object[] { names.get(i), null };
		}
		Object[][] rightObjects = new Object[names.size() / 2][];
		for (int i = 0; i < rightObjects.length; i++) {
			rightObjects[i] = new Object[] { names.get(leftObjects.length + i),
					null };
		}

		leftTable.setModel(new javax.swing.table.DefaultTableModel(leftObjects,
				new String[] { "Signal", "Check" }) {
			Class<?>[] types = new Class[] { java.lang.String.class,
					java.lang.Boolean.class };

			public Class<?> getColumnClass(int columnIndex) {
				return types[columnIndex];
			}
		});
		leftPanel.setViewportView(leftTable);
		leftTable.getColumnModel().getColumn(0).setResizable(false);
		leftTable.getColumnModel().getColumn(1).setResizable(false);

		rightTable.setModel(new javax.swing.table.DefaultTableModel(
				rightObjects, new String[] { "Signal", "Check" }) {
			Class<?>[] types = new Class[] { java.lang.String.class,
					java.lang.Boolean.class };

			public Class<?> getColumnClass(int columnIndex) {
				return types[columnIndex];
			}
		});
		rightPanel.setViewportView(rightTable);
		rightTable.getColumnModel().getColumn(0).setResizable(false);
		rightTable.getColumnModel().getColumn(1).setResizable(false);

		drawButton.setText("Draw signals");
		drawButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				gp.addSignalNames(listSelectedSignals());
				// gp.drawDiagram(Execution.globalClock);
				gp.validate();
				gp.repaint();
			}
		});

		clearButton.setText("Clear list");
		clearButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				resetCheckBoxes();
			}
		});

		javax.swing.GroupLayout signaliLayout = new javax.swing.GroupLayout(
				signals);
		signals.setLayout(signaliLayout);
		signaliLayout
				.setHorizontalGroup(signaliLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								signaliLayout
										.createParallelGroup(
												javax.swing.GroupLayout.Alignment.CENTER)
										// .addContainerGap()
										.addGroup(
												signaliLayout
														.createSequentialGroup()
														.addComponent(
																leftPanel,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																201,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(
																rightPanel,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																201,
																javax.swing.GroupLayout.PREFERRED_SIZE)

														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										// .addContainerGap()
										)

										.addComponent(
												drawButton,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												201, Short.MAX_VALUE)
										.addComponent(
												clearButton,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												201, Short.MAX_VALUE)));
		signaliLayout
				.setVerticalGroup(signaliLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								signaliLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												signaliLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																leftPanel,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																332,
																Short.MAX_VALUE)

														.addComponent(
																rightPanel,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																332,
																Short.MAX_VALUE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)

										.addComponent(
												drawButton,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												40,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(
												clearButton,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												40,
												javax.swing.GroupLayout.PREFERRED_SIZE)));

		add(signals);
		setSize(450, 500);
		setLocation(200, 100);

	}

	public List<String> listAllSignals() {

		List<String> names = NameConnector.listPinNames();

		return names;
	}

	public List<String> listSelectedSignals() {

		List<String> names = new LinkedList<String>();
		for (int i = 0; (i < leftTable.getRowCount()); i++) {
			try {
				Object o;
				if ((o = leftTable.getValueAt(i, 1)) != null
						&& ((Boolean) o).booleanValue()) {
					String name = leftTable.getValueAt(i, 0).toString();
					names.add(name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; (i < rightTable.getRowCount()); i++) {
			try {
				Object o;
				if ((o = rightTable.getValueAt(i, 1)) != null
						&& ((Boolean) o).booleanValue()) {
					String name = rightTable.getValueAt(i, 0).toString();
					names.add(name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return names;
	}

	public void resetCheckBoxes() {
		for (int i = 0; i < leftTable.getRowCount(); i++) {
			leftTable.getModel().setValueAt((Object) false, i, 1);
		}
		leftTable.clearSelection();
		for (int i = 0; i < rightTable.getRowCount(); i++) {
			rightTable.getModel().setValueAt((Object) false, i, 1);
		}
		rightTable.clearSelection();
	}

	public void init() {

	}

}
