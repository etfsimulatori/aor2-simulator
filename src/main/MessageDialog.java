package main;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MessageDialog {
	public static void sendMessage(String message, String position) {
		JFrame frame = new JFrame();

		JOptionPane.showMessageDialog(frame, message, position,
				JOptionPane.ERROR_MESSAGE);
		frame.dispose();
	}
}
